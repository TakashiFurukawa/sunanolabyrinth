//
//  SelectDoorLogos.cpp
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/15.
//
//

#include "SelectDoorLogos.h"
#include "MultiResolution.h"

#include "SelectLayer.h"
#include "SelectScene.h"

bool SelectDoorLogos::init()
{
    if(!Node::init())
    {
        return false;
    }
    
    isEnterStage = false;
    
    for(int i = 0; i < StageMAX; i++)
    {
        doorLogos[i] = SelectDoorLogo::create();
        doorLogos[i]->setPosition(Vec2::ZERO);
        this->addChild(doorLogos[i],100);
        
        char file[128];
        sprintf(file, "SelectScene/%d.png",1+i);
        numberLogos[i] = Sprite::create(file);
        this->addChild(numberLogos[i],200);
        
    }
    
    
    int elem = 0;
    Vec2 sukimaOffset = Vec2(30,100);
    Vec2 posOffset = Vec2(20,-30);
    for(int y = 0; y < 3; y++)
    {
        for(int x = 0; x < 4; x++)
        {
            Vec2 pos;
            pos.x = (designResolutionSize.width / 4.5 + posOffset.x) + (doorLogos[elem]->doorSp->getBoundingBox().size.width + sukimaOffset.x) * x ;
            pos.y = (designResolutionSize.height / 1.3f + posOffset.y ) - (doorLogos[elem]->doorSp->getBoundingBox().size.height + sukimaOffset.y) * y;
            
            doorLogos[elem]->doorSp->setPosition(pos);
            numberLogos[elem]->setPosition(pos);
            elem++;
        }
    }
    
    
    
    // タッチイベントとかの登録
    auto touchListener = cocos2d::EventListenerTouchOneByOne::create();
    touchListener->onTouchBegan = CC_CALLBACK_2(SelectDoorLogos::onTouchBegan, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(SelectDoorLogos::onTouchEnded, this);
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);
    
    this->scheduleUpdate();
    return true;
}

void SelectDoorLogos::update(float delta){}


bool SelectDoorLogos::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
    return true;
}

void SelectDoorLogos::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
    if(!isEnterStage)
    {
        for(int i = 0; i < StageMAX; i++)
        {
            if( doorLogos[i]->doorSp->getBoundingBox().containsPoint(touch->getLocation()))
            {
                
                if(doorLogos[i]->isSelect)
                {
                    doorLogos[i]->setZOrder(500);
                    numberLogos[i]->setZOrder(501);
                    sceneChangeAnim(i);
                    isEnterStage = true;
                }
                
                
                log("number =%d ",i);
                doorLogos[i]->isSelect = true;
            }
            else
            {
                doorLogos[i]->isSelect = false;
            }
        }
    }
}

void SelectDoorLogos::sceneChangeAnim(int selectNum)
{
    SelectLayer *parent = ((SelectLayer*)this->getParent());
    
    this->runAction(Sequence::create(
                                     CallFunc::create([=](){((SelectScene*)parent->getParent())->sceneChange(1 + selectNum);}),
                                     CallFunc::create([=](){doorLogos[selectNum]->startOpenAnim();numberLogos[selectNum]->runAction(MoveTo::create(0.5f, designResolutionSize/2));}),
                                     DelayTime::create(0.8f),
                                     CallFunc::create([=](){numberLogos[selectNum]->runAction(Spawn::create(
                                                                                                            ScaleTo::create(0.5f, 0),
                                                                                                            RotateTo::create(0.5f, 180.0f),
                                                                                                            NULL));}),
                                     DelayTime::create(0.2f),
                                     CallFunc::create([=](){parent->back->startBrokenBack();}),
                                     DelayTime::create(0.5f),
                                     CallFunc::create([=](){
                                                                int roop = 0;
                                                                for(auto door : doorLogos)
                                                                {
                                                                    if(roop != selectNum)
                                                                    {
                                                                        door->suikomare();
                                                                        numberLogos[roop]->runAction((Spawn::create(
                                                                                                                     ScaleTo::create(0.5f, 0.0f),
                                                                                                                     MoveTo::create(0.5f, designResolutionSize/2),
                                                                                                                     RotateTo::create(0.5f, 180.0f),
                                                                                                                     FadeOut::create(0.5f),
                                                                                                                     NULL)
                                                                                                       )
                                                                                                     );
                                                                    }
                                                                    roop++;
                                                                }
                                                            }),
                                     DelayTime::create(2.5f),
                                     CallFunc::create([=](){doorLogos[selectNum]->startCloseAnim();}),
                                     DelayTime::create(0.8f),
                                     CallFunc::create([=](){doorLogos[selectNum]->death();}),
                                     DelayTime::create(1.5f),
                                     CallFunc::create([=](){parent->removeAndGameStart();}),
                                     NULL)
                    );
}














