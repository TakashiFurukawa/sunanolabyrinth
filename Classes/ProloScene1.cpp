//
//  ProloScene1.cpp
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/12.
//
//

#include "ProloScene1.h"
#include "MultiResolution.h"

bool ProloScene1::init()
{
    if(!Slideshow::init())
    {
        return false;
    }
    
    
    Vec2 offset = Vec2(0,0);
    rad = 0.0f;
    
    // 背景
    this->appearSprite("PrologueScene/1/back.png", designResolutionSize/2, BACK);
    // たってもの
    this->appearSprite("PrologueScene/1/build.png", designResolutionSize/2, SCROLL_BUILD_1);
    this->appearSprite("PrologueScene/1/build.png", Vec2(designResolutionSize.width *1.5 + offset.x,designResolutionSize.height/2 + offset.y), SCROLL_BUILD_2);
   
    //もぶ
    this->appearSprite("PrologueScene/1/zako.png", Vec2(designResolutionSize.width/2 + offset.x,designResolutionSize.height/2 + offset.y), SCROLL_ZAKO_1);
    this->appearSprite("PrologueScene/1/zako.png", Vec2(designResolutionSize.width *1.5 + offset.x,designResolutionSize.height/2 + offset.y), SCROLL_ZAKO_2);
    
    

    //きゃら
    this->appearSprite("PrologueScene/1/char.png", Vec2(designResolutionSize.width/2 + offset.x,designResolutionSize.height/2 + offset.y), CHAR);
    
    this->scheduleUpdate();
    return true;
}

void ProloScene1::update(float delta)
{
    float moveSpeed = 00.0f;
    Sprite *build[2];
    build[0] = ((Sprite*)this->getChildByTag(SCROLL_BUILD_1));
    build[1] = ((Sprite*)this->getChildByTag(SCROLL_BUILD_2));
    
    Sprite *zako[2];
    zako[0] = ((Sprite*)this->getChildByTag(SCROLL_ZAKO_1));
    zako[1] = ((Sprite*)this->getChildByTag(SCROLL_ZAKO_2));


    for(int i = 0; i < 2; i++)
    {
        if(build[i]->getPositionX() <= -designResolutionSize.width/2 )
        {
            build[i]->setPositionX(designResolutionSize.width * 1.5f);
        }
        moveSpeed = 2.5f;
        build[i]->setPositionX(build[i]->getPositionX() - moveSpeed);
        //----------------------------------------------------------------//
        if(zako[i]->getPositionX() <= -designResolutionSize.width/2 )
        {
            zako[i]->setPositionX(designResolutionSize.width * 1.5f);
        }
        moveSpeed = 5.0f;
        zako[i]->setPositionX(zako[i]->getPositionX() - moveSpeed);
        zako[i]->setPositionY(zako[i]->getPositionY() + sin(rad)* 0.9f);
    }
    
    ((Sprite*)this->getChildByTag(CHAR))->setPositionY(((Sprite*)this->getChildByTag(CHAR))->getPositionY() + cos(rad));
    
    
    rad += 0.3f;
}