//
//  WarpBlock.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/05/13.
//
//

#ifndef __SunanoLabyrinth__WarpBlock__
#define __SunanoLabyrinth__WarpBlock__

#include "cocos2d.h"
#include "Stage.h"

using namespace cocos2d;
using namespace CocosDenshion;

class WarpBlock : public Sprite
{
public:
    static WarpBlock *create();
    virtual bool init();
    void update(float delta);
    
    WarpBlock *_warp;
    Stage *_stage;
    
    bool _isWarp;
    int _warpNo;
    
    void warp();
};

#endif /* defined(__SunanoLabyrinth__WarpBlock__) */
