//
//  ProloScene2.h
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/12.
//
//

#ifndef __SunanoLabyrinth__ProloScene2__
#define __SunanoLabyrinth__ProloScene2__

#include "cocos2d.h"
#include "Slideshow.h"
USING_NS_CC;

class ProloScene2 : public Slideshow
{
public:
    CREATE_FUNC(ProloScene2);
    bool init();
    void update(float delta);
    enum SP_TAG
    {
        BACK,
        CHAR,
        BUILD,
        URANAI
    };
    
private:
    void uranaiAppear(float delta);

};

#endif /* defined(__SunanoLabyrinth__ProloScene2__) */
