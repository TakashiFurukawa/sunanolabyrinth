//
//  SwitchBlock.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/23.
//
//

#include "SwitchBlock.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"

SwitchBlock *SwitchBlock::create()
{
    SwitchBlock *pRet = new SwitchBlock();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool SwitchBlock::init()
{
    if ( !Actor::init() ) return false;
    
    setTexture("gameScene/block/block3.png");
    _actorType = SWITCH_BLOCK;
    
    scheduleUpdate();
    return true;
}

void SwitchBlock::update(float delta)
{
    
}

void SwitchBlock::switchOn()
{
    
}
