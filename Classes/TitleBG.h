//
//  TitleBG.h
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/02.
//
//

#ifndef __SunanoLabyrinth__TitleBG__
#define __SunanoLabyrinth__TitleBG__

#include "cocos2d.h"
USING_NS_CC;

class TitleBG : public Sprite
{
public:
    CREATE_FUNC(TitleBG);
    bool init();
    
};

#endif /* defined(__SunanoLabyrinth__TitleBG__) */
