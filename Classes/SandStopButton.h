//
//  SandStopButton.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/05/22.
//
//

#ifndef __SunanoLabyrinth__SandStopButton__
#define __SunanoLabyrinth__SandStopButton__

#include "cocos2d.h"
#include "UIButtonSprite.h"
#include "Sand.h"

using namespace cocos2d;
using namespace CocosDenshion;

class SandStopButton : public UIButtonSprite
{
public:
    static SandStopButton *create();
    virtual bool init();
    void update(float delta);
    
    Sand *_sand[2];
    Label *_countLabel;
    
    bool _isStop;
    
    int _timeCnt;
    int _time;
};

#endif /* defined(__SunanoLabyrinth__SandStopButton__) */
