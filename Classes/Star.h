//
//  Star.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/05/19.
//
//

#ifndef __SunanoLabyrinth__Star__
#define __SunanoLabyrinth__Star__

#include "cocos2d.h"
#include "MyChar.h"

using namespace cocos2d;
using namespace CocosDenshion;

class Star : public Sprite
{
public:
    static Star *create();
    virtual bool init();
    void update(float delta);
    
    bool _isGet;
    MyChar *_myChar;
    
    void startStar();
};

#endif /* defined(__SunanoLabyrinth__Star__) */
