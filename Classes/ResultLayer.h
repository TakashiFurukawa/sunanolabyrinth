//
//  ResultLayer.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/05/29.
//
//

#ifndef __SunanoLabyrinth__ResultLayer__
#define __SunanoLabyrinth__ResultLayer__

#include "cocos2d.h"

using namespace cocos2d;
using namespace CocosDenshion;

class ResultLayer : public Layer
{
public:
    static ResultLayer *create(int time, int heart, bool isClear);
    virtual bool init(int time, int heart, bool isClear);
    void update(float delta);
    
    Label *_titleLabel, *_percentLabel, *_timeLabel, *_heartLabel, *_guageLabel;
    Label *_kekkaLabel;
    
    Sprite *_wakuSp, *_myCharSp[2], *_guageSp;
    
    ClippingNode *_clipNode;
    bool _changeScene;
    
    float _aidoMax;
    float _aido;
    
    int _time;
    int _clearTime;
    
    int _heartNumMax;
    int _heartNum;
    
    bool _isClear;
    
    
    void hikkomeru();
    void kekka(float dt);
    void appearAction(float dt);
    void buttonAppear(float dt);
    void guageUpdate(float dt);
    void timeUpdate(float dt);
    void heartNumUpdate(float dt);
    
    void happyEffect();
    
    
};

#endif /* defined(__SunanoLabyrinth__ResultLayer__) */
