//
//  CrushSwitch.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/30.
//
//

#ifndef __SunanoLabyrinth__CrushSwitch__
#define __SunanoLabyrinth__CrushSwitch__

#include "cocos2d.h"
#include "GimmickSwitch.h"

using namespace cocos2d;
using namespace CocosDenshion;

class CrushSwitch : public GimmickSwitch
{
public:
    static CrushSwitch *create();
    virtual bool init();
    void update(float delta);
    
    void switchOn() override;
    
    int _crushNo;
};

#endif /* defined(__SunanoLabyrinth__CrushSwitch__) */
