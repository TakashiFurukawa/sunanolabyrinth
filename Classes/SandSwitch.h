//
//  SandSwitch.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/26.
//
//

#ifndef __SunanoLabyrinth__SandSwitch__
#define __SunanoLabyrinth__SandSwitch__

#include "cocos2d.h"
#include "GimmickSwitch.h"

using namespace cocos2d;
using namespace CocosDenshion;

class SandSwitch : public GimmickSwitch
{
public:
    static SandSwitch *create();
    virtual bool init();
    void update(float delta);
    
    void switchOn() override;
};

#endif /* defined(__SunanoLabyrinth__SandSwitch__) */
