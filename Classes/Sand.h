//
//  Sand.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/14.
//
//

#ifndef __SunanoLabyrinth__Sand__
#define __SunanoLabyrinth__Sand__

#include "cocos2d.h"
#include "MapManager.h"
#include "MyChar.h"
#include "ActorManager.h"


using namespace CocosDenshion;

class Sand : public cocos2d::Layer
{
public:
    static Sand *create();
    virtual bool init();
    void update(float delta);
    
    bool _isUmaru;
    
    enum State
    {
        UP,
        DOWN,
    };
    
    float _furyoku;
    bool _isMove;
    
    bool _isWater;
    float _sandSpeed;
    
    State _state;
    MapManager *_mapManager;
    MyChar *_myChar;
    ActorManager *_actorManager;
    
    cocos2d::Sprite *_sand;
    
    void resizeSand();
    
    void changeToWater(bool isChange);
};

#endif /* defined(__SunanoLabyrinth__Sand__) */
