//
//  GoalBlock.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/05/11.
//
//

#include "GoalBlock.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"

GoalBlock *GoalBlock::create()
{
    GoalBlock *pRet = new GoalBlock();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool GoalBlock::init()
{
    if ( !Sprite::init() ) return false;
    
    _isGoal = false;
    
    setTexture("gameScene/gimmick/door.png");
    setAnchorPoint(Vec2(0.5, 0));
    
    scheduleUpdate();
    return true;
}

void GoalBlock::update(float delta)
{
    if(_gameLayer->_activeStage->_goalNode == this)
    {
        if(_gameLayer->_activeStage->_myChar->getBoundingBox().intersectsRect(getBoundingBox()))
        {
            _isGoal = true;
        }
        else
        {
            _isGoal = false;
        }
    }
}













