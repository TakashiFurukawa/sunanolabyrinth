//
//  AnimationManager.cpp
//  TextScript1
//
//  Created by Furukawa on 2014/11/20.
//
//

#include "AnimationManager.h"

// アニメーションキャッシュ追加
void AnimationManager::addAnimationCache(string path, string cacheName, int frameNum, float delay)
{
    // アニメーションの準備
    auto *_animation = cocos2d::Animation::create();
    for (int i = 0; i < frameNum; i++) {
        auto str = cocos2d::__String::createWithFormat("%s%i.png", path.c_str(), i);
        _animation->addSpriteFrameWithFile(str->getCString());
    }
    
    _animation->setDelayPerUnit(delay);
    _animation->setRestoreOriginalFrame(true);
    
    // キャッシュに登録
    cocos2d::AnimationCache::getInstance()->addAnimation(_animation, cacheName);
}


void AnimationManager::addAnimationCacheReverse(string path, string cacheName, int frameNum, float delay)
{
   
        // アニメーションの準備
        auto *_animation = cocos2d::Animation::create();
        for (int i = frameNum - 1; i >= 0; i--) {
            auto str = cocos2d::__String::createWithFormat("%s%i.png", path.c_str(), i);
            _animation->addSpriteFrameWithFile(str->getCString());
        }
        
        _animation->setDelayPerUnit(delay);
        _animation->setRestoreOriginalFrame(true);
        
        // キャッシュに登録
        cocos2d::AnimationCache::getInstance()->addAnimation(_animation, cacheName);
}


// 指定回リピートするアニメートアクションをキャッシュから生成
cocos2d::Repeat* AnimationManager::createRepeat(string cacheName, unsigned int time)
{
    return cocos2d::Repeat::create(cocos2d::Animate::create(cocos2d::AnimationCache::getInstance()->getAnimation(cacheName)), time);
}

// 永遠にリピートするアニメートアクションをキャッシュから生成
cocos2d::RepeatForever* AnimationManager::createRepeatForever(string cacheName)
{
    return cocos2d::RepeatForever::create(cocos2d::Animate::create(cocos2d::AnimationCache::getInstance()->getAnimation(cacheName)));
}