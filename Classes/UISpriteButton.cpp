//
//  UISpriteButton.cpp
//  FPaint
//
//  Created by Furukawa on 2015/05/04.
//
//

#include "UISpriteButton.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"

UISpriteButton *UISpriteButton::create()
{
    UISpriteButton *pRet = new UISpriteButton();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

UISpriteButton *UISpriteButton::create(function<void()> pushed)
{
    auto pRet = create();
    pRet->_pushed = pushed;
    return pRet;
}
UISpriteButton *UISpriteButton::create(function<void()> pushed, function<void()> released)
{
    auto pRet = create();
    pRet->_pushed = pushed;
    pRet->_released = released;
    return pRet;
}
UISpriteButton *UISpriteButton::create(string fileName)
{
    UISpriteButton *pRet = new UISpriteButton();
    if(pRet && pRet->init(fileName))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}
UISpriteButton *UISpriteButton::create(string fileName, function<void()> pushed)
{
    auto pRet = create(fileName);
    pRet->_pushed = pushed;
    return pRet;
}
UISpriteButton *UISpriteButton::create(string fileName, function<void()> pushed, function<void()> released)
{
    auto pRet = create(fileName);
    pRet->_pushed = pushed;
    pRet->_released = released;
    return pRet;
}


bool UISpriteButton::init()
{
    if ( !Sprite::init() ) return false;
    
    // タッチ
    auto touchListener = EventListenerTouchOneByOne::create();
    touchListener->onTouchBegan = CC_CALLBACK_2(UISpriteButton::onTouchBegan, this);
    touchListener->onTouchMoved = CC_CALLBACK_2(UISpriteButton::onTouchMoved, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(UISpriteButton::onTouchEnded, this);
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);
    touchListener->setSwallowTouches(true);
    
    _isPushed = false;
    _isPushOnly = false;
    _baseScale = 1.0f;
    
    scheduleUpdate();
    return true;
}

bool UISpriteButton::init(function<void()> pushed)
{
    init();
    setTextureRect(cocos2d::Rect(0, 0, 100, 100));
    
    _pushed = pushed;
    
    return true;
}

bool UISpriteButton::init(function<void()> pushed, function<void()> released)
{
    init();
    setTextureRect(cocos2d::Rect(0, 0, 100, 100));
    
    _pushed = pushed;
    _released = released;
    
    return true;
}

bool UISpriteButton::init(string fileName)
{
    init();
    setTexture(fileName);
    
    return true;
}

bool UISpriteButton::init(string fileName, function<void()> pushed)
{
    init(fileName);
    _pushed = pushed;
    
    return true;
}

bool UISpriteButton::init(string fileName, function<void()> pushed, function<void()> released)
{
    init(fileName);
    _pushed = pushed;
    _released = released;
    
    return true;
}

void UISpriteButton::update(float delta)
{
    
}

void UISpriteButton::setBaseScale(float scale)
{
    _baseScale = scale;
    setScale(_baseScale);
}

bool UISpriteButton::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    Vec2 touchPos = touch->getLocation();
    auto parent = getParent();
    while(parent)
    {
        touchPos -= parent->getPosition();
        parent = parent->getParent();
    }
    
    if(getBoundingBox().containsPoint(touchPos) && _pushed)
    {
        if(!_isPushOnly)
        {
            runAction(EaseOut::create(ScaleTo::create(0.07, 0.7 * _baseScale), 2));
        }
        else
        {
            auto act = EaseInOut::create(Sequence::create(ScaleTo::create(0.1, 0.7 * _baseScale),
                                                          ScaleTo::create(0.1, 1.0 * _baseScale),
                                                          NULL), 2);
            act->setTag(1);
            this->stopActionByTag(1);
            this->runAction(act);
        }
        
        _isPushed = true;
        _pushed();
    }
    else
    {
        return false;
    }
    
    return true;
}

void UISpriteButton::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
    
}

void UISpriteButton::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
    Vec2 touchPos = touch->getLocation();
    auto parent = (Node*)this;
    while(parent)
    {
        touchPos -= parent->getPosition();
        parent = parent->getParent();
    }
    
    if(_isPushed)
    {
        if(!_isPushOnly)
        {
            runAction(EaseOut::create(ScaleTo::create(0.07, 1.0 * _baseScale), 2));
        }
    }
    if(_released && _isPushed)
    {
        _isPushed = false;
        _released();
    }
}






