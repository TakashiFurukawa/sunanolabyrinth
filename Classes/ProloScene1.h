//
//  ProloScene1.h
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/12.
//
//

#ifndef __SunanoLabyrinth__ProloScene1__
#define __SunanoLabyrinth__ProloScene1__

#include "cocos2d.h"
#include "Slideshow.h"
USING_NS_CC;

class ProloScene1 : public Slideshow
{
public:
    CREATE_FUNC(ProloScene1);
    bool init();
    void update(float delta);
    
    enum SP_TAG
    {
        BACK,
        CHAR,
        SCROLL_BUILD_1,
        SCROLL_BUILD_2,
        SCROLL_ZAKO_1,
        SCROLL_ZAKO_2
    };
private:
    float rad;

};

#endif /* defined(__SunanoLabyrinth__ProloScene1__) */
