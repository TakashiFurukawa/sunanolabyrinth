//
//  InfoBlock.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/06/04.
//
//

#ifndef __SunanoLabyrinth__InfoBlock__
#define __SunanoLabyrinth__InfoBlock__

#include "cocos2d.h"
#include "InfoLabel.h"
#include "MyChar.h"

using namespace cocos2d;
using namespace CocosDenshion;
using namespace std;

class InfoBlock : public Sprite
{
public:
    static InfoBlock *create(string text);
    virtual bool init(string text);
    void update(float delta);
    
    InfoLabel *_infoLabel;
    MyChar *_myChar;
    
    void hitCheck();
};

#endif /* defined(__SunanoLabyrinth__InfoBlock__) */
