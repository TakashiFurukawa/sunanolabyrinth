//
//  BackGrounds.h
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/05/21.
//
//

#ifndef __SunanoLabyrinth__BackGrounds__
#define __SunanoLabyrinth__BackGrounds__

#include <stdio.h>
#include "cocos2d.h"
USING_NS_CC;



class BackGrounds : public Node
{
public:
    CREATE_FUNC(BackGrounds);
    bool init();
    void update(float delta);
    
    Sprite *backSp;
    
    void shake();
    // 空間ブッパ系
    bool isStartBroken;
    Vector<Sprite*> brokenBgSps;
    std::vector<bool> isBrokenFlg;
    Texture2D *backTex;
    void setBrokenBack();
    void brokenBack(float delta);
    void startBrokenBack();
    
private:
};

#endif /* defined(__SunanoLabyrinth__BackGrounds__) */
