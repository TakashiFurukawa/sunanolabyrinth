//
//  GimmickSwitch.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/17.
//
//

#ifndef __SunanoLabyrinth__GimmickSwitch__
#define __SunanoLabyrinth__GimmickSwitch__

#include "cocos2d.h"
#include "Actor.h"
#include "MyChar.h"
#include "Stage.h"

using namespace cocos2d;
using namespace CocosDenshion;

class GimmickSwitch : public Actor
{
public:
    static GimmickSwitch *create();
    virtual bool init();
    void update(float delta);
    
    bool _isOn;
    
    void moveX() override;
    virtual void switchOn();
    
    cocos2d::Sprite *stick;
    
    Color3B _col;
    
    MyChar *_myChar;
private:
    
    int _frameCnt;
};

#endif /* defined(__SunanoLabyrinth__GimmickSwitch__) */
