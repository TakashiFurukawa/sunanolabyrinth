//
//  SlideshowSprite.h
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/05/19.
//
//

#ifndef SunanoLabyrinth_SlideshowSprite_h
#define SunanoLabyrinth_SlideshowSprite_h

#include "cocos2d.h"
USING_NS_CC;

class Slideshow : public Layer
{
public:
    CREATE_FUNC(Slideshow);
    
    
    bool init();
    virtual void update(float delta);
    void appearSprite(const std::string &filename, Vec2 appearPos, int tag); // 上からスプライトを重ねる
    void setSprite(const std::string &filename, Vec2 setPos, int tag);       // スプライトをセットしておく
    void appearToSetSprite();                                                // セットしてあるスプライトを出現させる
    void clearSlide();                                                       // 現在表示している画像全部消す
    void changeSlide(const std::string &filename, Vec2 appearPos, int tag);  // 表示中のスプライトを全て消去後描画
    void playAnimation(cocos2d::FiniteTimeAction *action, int spTag);                        //
    void removeSprite(int tag);
    void death();
    
private:
    int isViewSlideNum;// 0or1
    
};

#endif
