//
//  WaterSwitch.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/05/22.
//
//

#include "WaterSwitch.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "GameLayer.h"

WaterSwitch *WaterSwitch::create()
{
    WaterSwitch *pRet = new WaterSwitch();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool WaterSwitch::init()
{
    if ( !GimmickSwitch::init() ) return false;
    
    setColor(Color3B(255, 50, 255));
    
    scheduleUpdate();
    return true;
}

void WaterSwitch::update(float delta)
{
    GimmickSwitch::update(delta);
}

void WaterSwitch::switchOn()
{
    if(_isOn) return;
    
    GimmickSwitch::switchOn();
    
    
    _isOn = true;
    
    GameLayer *gameLayer;
    
    auto parent = getParent();
    while(parent->getName() != "gameLayer") parent = parent->getParent();
    
    gameLayer = (GameLayer*)parent;
    auto sand = gameLayer->_activeStage->_sand;
    sand->changeToWater(sand->_isWater?false:true);
    
    stick->runAction(cocos2d::Sequence::create(cocos2d::EaseBackIn::create(cocos2d::RotateTo::create(0.25, -stick->getRotation())),
                                               cocos2d::DelayTime::create(0.5),
                                               cocos2d::CallFunc::create([=]()
                                                                         {
                                                                             _isOn = false;
                                                                             _myChar->_inSandPercent = 1;
                                                                         }),
                                               NULL));
}