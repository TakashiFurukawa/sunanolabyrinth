//
//  TitleChar.h
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/03.
//
//

#ifndef __SunanoLabyrinth__TitleChar__
#define __SunanoLabyrinth__TitleChar__

#include "cocos2d.h"
USING_NS_CC;

class TitleChar : public Node
{
public:
    CREATE_FUNC(TitleChar);
    bool init();
    void update(float delata);
    
    Sprite *gentleSp;
    
    void rasterScroll(float delta);
    void fadeOutToUnder();
    
    void changeAct();
    bool isFadeOutFinish;
private:
    Texture2D *ladyTex;
    Vector<Sprite*> ladyTexLines;
    Vec2 ladySpPos;
    
    float pai;
    float waveX;
    bool fadeStart;
    
    
};
#endif /* defined(__SunanoLabyrinth__TitleChar__) */
