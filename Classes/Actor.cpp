//
//  Actor.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/16.
//
//

#include "Actor.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "HiruYoruBlock.h"

int Actor::moveCount = 0;

Actor *Actor::create()
{
    Actor *pRet = new Actor();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool Actor::init()
{
    if ( !cocos2d::Sprite::init() ) return false;
    
    initWithFile("gameScene/myChar/woman0.png");
    setPosition(cocos2d::Vec2(300, 1000));
    setAnchorPoint(cocos2d::Vec2(0.5, 0));
    
    _move = cocos2d::Vec2::ZERO;
    _preMove = _move;
    
    _dir = Direction::RIGHT;
    _state = State::STOP;
    _actorType = NOMAL_BLOCK;
    
    _collisionSize = getBoundingBox().size;
    
    _isClimbOK = true;
    _isLanding = false;
    _isClimbing = false;
    
    _movedX = false;
    _movedY = false;
    
    _kasoku = cocos2d::Vec2::ZERO;
    
    scheduleUpdate();
    return true;
}



void Actor::update(float delta)
{
    
    // 重力
    _move.y -= 10;
    
    move();
    
}

void Actor::move()
{
    if(_actorType == KOTEI || _actorType == SWITCH_BLOCK ||  getOpacity() < 255) return;
    
    _prePos = getPosition();
    
    
    _hitX = false;
    _hitY = false;
    
    moveX();
    moveY();
    
    _preMove = _move;
    
    _move.x = 0;
    _move.y = 0;
}

void Actor::moveX()
{
    if(_actorType == KOTEI || _actorType == NOMAL_BLOCK || _movedX || _actorType == SWITCH || _actorType == SWITCH_BLOCK) return;
    
    if(_move.x == 0) return;
    
    // x
    // 判定の精度
    int seido = abs(_move.x);
//    auto preMoveX = _move.x;
    
    for(int i=0; i<seido; i++)
    {
        // 移動
        setPositionX(getPositionX() + _move.x/seido);
        
        
        // めり込んでいたらもどす
        if(isCollisionActor().size() > 0|| _mapManager->isCollision(getPosition(), getBoundingBox()))
        {
            setPositionX(getPositionX() - _move.x/seido);
            _hitX = true;
        }
        if(isHamidashi())
        {
            setPositionX(getPositionX() - _move.x/seido);
        }
        
    }
}



void Actor::moveY()
{
    if(_actorType == KOTEI || _actorType == HIRU || _actorType == YORU || _movedY || _actorType == SWITCH || _actorType == SWITCH_BLOCK) return;
    
    if(_move.y == 0) return;
    
    // y
    
    int seido = abs(_move.y);
//    int seido = 1;
    
    for(int i=0; i<seido; i++)
    {
        // 移動
        setPositionY(getPositionY() + (int)_move.y/seido);
        
        auto collisionActors = isCollisionActor();
        
        // 他のアクターにあたっていたらそれにも力を加える
        for(Actor* collisionActor : collisionActors)
        {

            if(_move.y > 0 && abs(_move.y) > abs(collisionActor->_move.y))
            {
                collisionActor->_move.y = _move.y;
                collisionActor->_hitY = true;
                collisionActor->moveY();
                break;
            }
        }
        
        // めりこんでいたら戻す
        if(collisionActors.size() > 0)
        {
            setPositionY(getPositionY() - (int)_move.y/seido);
            _hitY = true;
            break;
        }
        else if(_mapManager->isCollision(getPosition(), getBoundingBox()) || isHamidashi())
        {
            setPositionY(getPositionY() - (int)_move.y/seido);
            _hitY = true;
            break;
        }
    }
    
    if(_hitY)
    {
        _isLanding = true;
    }
    else
    {
        _isLanding = false;
    }
}

vector<Actor*> Actor::isCollisionActor()
{
    vector<Actor*> collisionActors;
    
    auto parent = getParent();
    auto children = parent->getChildren();
    
    for(auto child : children)
    {
        if(child == this) continue;
        
        // 透明掛かっているか、遠くにあるものはスルー
        if(child->getOpacity() < 255 ||  getPosition().distance(child->getPosition()) > getBoundingBox().size.height * 5) continue;
        
        Actor *actor = (Actor*)child;
        
        if(actor->getBoundingBox().intersectsRect(getBoundingBox()))
        {
            collisionActors.push_back(actor);
        }
    }
    
//    moveCount++;
//    log("moveCount %d", moveCount);
    
    return collisionActors;
}

cocos2d::Rect Actor::getCollisionRect()
{
    cocos2d::Rect collisionRect = cocos2d::Rect(0, 0, _collisionSize.width, _collisionSize.height);
    return RectApplyAffineTransform(collisionRect, getNodeToParentAffineTransform());
}


bool Actor::isHamidashi()
{
    // はみだししょり
    if((getPositionX() - getBoundingBox().size.width/2 < 0)||
    (getPositionX() + getBoundingBox().size.width/2 > _mapManager->_stageSize.width)||
    (getPositionY() < 0)||
    (getPositionY() + getBoundingBox().size.height > _mapManager->_stageSize.height))
    {
        return true;
    }
    
    return false;
}













