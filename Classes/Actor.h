//
//  Actor.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/16.
//
//

#ifndef __SunanoLabyrinth__Actor__
#define __SunanoLabyrinth__Actor__

#include "cocos2d.h"
#include "MapManager.h"

using namespace CocosDenshion;
using namespace std;

class Actor : public cocos2d::Sprite
{
public:
    enum Direction
    {
        RIGHT,
        LEFT,
        UP,
        DOWN,
    };
    enum State
    {
        STOP,
        WALK,
    };
    enum ActorType
    {
        PLAYER,
        NOMAL_BLOCK,
        KOTEI,
        SWITCH,
        HIRU,
        YORU,
        SWITCH_BLOCK,
    };
    
    static Actor *create();
    virtual bool init();
    void update(float delta);
    
    
    static int moveCount;
    
    Direction _dir;
    State _state;
    ActorType _actorType;
    cocos2d::Vec2 _move;
    cocos2d::Vec2 _preMove;
    cocos2d::Vec2 _prePos;
    MapManager *_mapManager;
    bool _movedX;
    bool _movedY;
    
    bool _hitX, _hitY;
    
    cocos2d::Vec2 _kasoku;
    
    bool _isClimbOK;
    
    virtual void move();
    virtual void moveX();
    virtual void moveY();
    vector<Actor*> isCollisionActor();
    
protected:
    
    cocos2d::Size _collisionSize;
    bool _isLanding;
    bool _isClimbing;
    
    cocos2d::Rect getCollisionRect();
    bool isHamidashi();
};

#endif /* defined(__SunanoLabyrinth__Actor__) */
