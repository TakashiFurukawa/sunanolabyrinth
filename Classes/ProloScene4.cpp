//
//  ProloScene4.cpp
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/12.
//
//

#include "ProloScene4.h"
#include "MultiResolution.h"

bool ProloScene4::init()
{
    if(!Slideshow::init())
    {
        return false;
    }
    
    
    // いっかい普通に表示
    // 背景
    this->appearSprite("PrologueScene/4/back.png", designResolutionSize/2, BACK);
    // 砂時計
    this->appearSprite("PrologueScene/4/sandGlass.png", designResolutionSize/2, SANDGLASS);
    float offsetY =0;
    //おなご
    this->appearSprite("PrologueScene/4/lady.png", Vec2(designResolutionSize.width/2,designResolutionSize.height/2 - offsetY), LADY);
    // おのこ
    this->appearSprite("PrologueScene/4/gentle.png", Vec2(designResolutionSize.width/2,designResolutionSize.height/2 - offsetY), GENTLE);
    
    this->runAction(Sequence::create(
                                     DelayTime::create(0.5f),
                                     CallFunc::create([=](){this->playAnimation(Spawn::create(
                                                                                              MoveBy::create(3.0f, Vec2(-25.0f,10.0f)),
                                                                                              ScaleTo::create(3.0f, 0.8f),
                                                                                              RotateTo::create(3.0f, -15.0f),
                                                                                              NULL),
                                                                                LADY);}),
                                     CallFunc::create([=](){this->playAnimation(MoveBy::create(3.0f, Vec2(-25.0f,-25.0f)), GENTLE);}),
                                     NULL));
    
    this->scheduleUpdate();
    return true;
}

void ProloScene4::update(float delta)
{
    float rotSpeed = 0.025f;
    float rot;
    rot = ((Sprite*)this->getChildByTag(SANDGLASS))->getRotation() + rotSpeed;
    if(rot > 360)
    {
        rot -= 360;
    }
    
    ((Sprite*)this->getChildByTag(SANDGLASS))->setRotation(rot);
    
    

}