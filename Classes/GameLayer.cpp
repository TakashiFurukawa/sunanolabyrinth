//
//  GameLayer.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/14.
//
//

#include "GameLayer.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "CustomFollow.h"
#include "GameScene.h"
#include "GameUI.h"
#include "HeartHomingEffect.h"


GameLayer *GameLayer::create(int stageNo)
{
    GameLayer *pRet = new GameLayer();
    if(pRet && pRet->init(stageNo))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

void GameLayer::onExit()
{
    cocos2d::Layer::onExit();
}


bool GameLayer::init(int stageNo)
{
    if ( !cocos2d::Layer::init() ) return false;
    
    setName("gameLayer");
    
    // タッチイベントとかの登録
    auto touchListener = cocos2d::EventListenerTouchOneByOne::create();
    touchListener->onTouchBegan = CC_CALLBACK_2(GameLayer::onTouchBegan, this);
    touchListener->onTouchMoved = CC_CALLBACK_2(GameLayer::onTouchMoved, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(GameLayer::onTouchEnded, this);
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);
    
    // キーボード
    auto keyListener = cocos2d::EventListenerKeyboard::create();
    keyListener->onKeyPressed = CC_CALLBACK_2(GameLayer::onKeyPressed, this);
    keyListener->onKeyReleased = CC_CALLBACK_2(GameLayer::onKeyReleased, this);
//    getEventDispatcher()->addEventListenerWithFixedPriority(keyListener, 1);
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyListener, this);
    
    // アクセラレータ
    cocos2d::Device::setAccelerometerEnabled(true);
    auto accelaration = cocos2d::EventListenerAcceleration::create(CC_CALLBACK_2(GameLayer::onAcceleration, this));
    
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(accelaration, this);
    
    
    setPosition(cocos2d::Vec2::ZERO);
    setAnchorPoint(cocos2d::Vec2::ZERO);

    _topStage = new Stage();
    addChild(_topStage);
    _topStage->init(MyChar::PlayerType::Gentle, stageNo);
    _topStage->autorelease();
    _topStage->setPosition(Vec2(0, designResolutionSize.height*0.5 / getScale() +_topStage->_mapManager->_stageSize.height * 0.0));

    _bottomStage = new Stage();
    addChild(_bottomStage);
    _bottomStage->init(MyChar::PlayerType::Lady, stageNo);
    _bottomStage->autorelease();
    _bottomStage->setPosition(cocos2d::Vec2(0, _topStage->getPositionY() -_topStage->_mapManager->_stageSize.height * 1.0));
    
    _topStage->_sand->_state = Sand::State::DOWN;
    _bottomStage->_sand->_state = Sand::State::UP;

    _controlMyChar = _topStage->_myChar;

    _isTouchEnable = true;

    _stageState = LANDSCAPE_LEFT;
    _activePlayer = MyChar::PlayerType::Gentle;
    _touchPoint = cocos2d::Vec2::ZERO;

    _turnDuration = 0.5f;
    _isEnablePlayerChange = true;

    _activeStage = _topStage;
    _unActiveStage = _bottomStage;
    
    _isGameOver = false;
    _isTurning = false;

    scheduleUpdate();
    return true;
}

void GameLayer::update(float delta)
{
    
}

void GameLayer::gameOver()
{
    _isGameOver = true;
}

void GameLayer::turn(StageState state)
{
    if(_stageState == state || _isTurning) return;
    
    
    _isTurning = true;
    
    auto rotate = cocos2d::Sequence::create(cocos2d::RotateTo::create(_turnDuration, (state==LANDSCAPE_LEFT?0:180)),
                                            CallFunc::create([=]()
                                                             {
                                                                 _isTurning = false;
                                                             }),
                                            NULL);
    rotate->setTag(1000);
    stopActionByTag(1000);
//    runAction(rotate);
    
    if(_stageState == LANDSCAPE_LEFT)  _stageState = LANDSCAPE_RIGHT;
    else _stageState = LANDSCAPE_LEFT;
    
    if(_topStage->_timeState == Stage::YORU)
    {
        _topStage->_sand->_state = Sand::State::DOWN;
        _topStage->_timeState = Stage::HIRU;
    }
    else
    {
        _topStage->_sand->_state = Sand::State::UP;
        _topStage->_timeState = Stage::YORU;
    }
    
    if(_topStage->_timeState == Stage::YORU)
    {
        _bottomStage->_sand->_state = Sand::State::DOWN;
        _bottomStage->_timeState = Stage::HIRU;
    }
    else
    {
        _bottomStage->_sand->_state = Sand::State::UP;
        _bottomStage->_timeState = Stage::YORU;
    }
    
//    runAction(Sequence::create(EaseOut::create(JumpBy::create(0.15, cocos2d::Vec2::ZERO, 50, 3), 2), NULL));
    
    turnBack(_turnDuration);
    
//    Sprite *sunadokei = Sprite::create("gameScene/ui/sunadokei_icon.png");
//    sunadokei->setPosition(Vec2(visibleCenter));
//    getParent()->addChild(sunadokei);
//    sunadokei->setOpacity(0);
//    
//    auto act = Sequence::create(FadeIn::create(0.1),
//                                EaseInOut::create(RotateBy::create(0.8, 180), 2),
//                                DelayTime::create(0.2),
//                                FadeOut::create(0.1),
//                                CallFunc::create([=]()
//                                                 {
//                                                     sunadokei->removeFromParent();
//                                                 }),
//                                NULL);
//    sunadokei->runAction(act);
    
//    
    GameScene *gameScene = (GameScene*)getParent();
    gameScene->_waku->runAction(rotate);
//    GameUI *gameUI = gameScene->_gameUI;
//
//    gameUI->turn();
//
    
//    stopActionByTag(1002);
//    auto followNode = _activeStage->_myChar;
    
    _topStage->follow(false);
    _bottomStage->follow(false);
    
    float duration = 0.5;
    auto topAct = Spawn::create(MoveTo::create(duration, _bottomStage->getPosition()),
                                Sequence::create(EaseOut::create(MoveBy::create(duration/2, Vec2(_topStage->_mapManager->_stageSize.width/2, 0)), 2),
                                                 EaseIn::create(MoveBy::create(duration/2, Vec2(-_topStage->_mapManager->_stageSize.width/2, 0)), 2),
                                             NULL),
                                NULL);
    auto bottomAct = Spawn::create(MoveTo::create(duration, _topStage->getPosition()),
                                   Sequence::create(EaseOut::create(MoveBy::create(duration/2, Vec2(-_bottomStage->_mapManager->_stageSize.width/2, 0)), 2),
                                                    EaseIn::create(MoveBy::create(duration/2, Vec2(_bottomStage->_mapManager->_stageSize.width/2, 0)), 2),
                                                    NULL),
                                   NULL);
    
    _topStage->runAction(Sequence::create(topAct,
                                          CallFunc::create([=]()
                                                           {
                                                               _topStage->follow(true);
                                                               _bottomStage->follow(true);
                                                           }),
                                          NULL));
    _bottomStage->runAction(bottomAct);
    
    
    auto black = _bottomStage->_black;
    _bottomStage->_black = _topStage->_black;
    _topStage->_black = black;
}



void GameLayer::turnBack(float duration)
{
    auto isHiru = _activeStage->_timeState == Stage::HIRU;
    
    auto rotateBack = cocos2d::Sequence::create(cocos2d::EaseBackInOut::create(cocos2d::RotateTo::create(duration, (isHiru?0:180))), NULL);
    rotateBack->setTag(1000);
    _topStage->_back->stopActionByTag(1000);
    _topStage->_back->runAction(cocos2d::Spawn::create(rotateBack,
                                              cocos2d::Sequence::create(cocos2d::FadeTo::create(duration*0.25, 255 * 0.5),
                                                               cocos2d::DelayTime::create(duration*0.5),
                                                               cocos2d::FadeIn::create(duration*0.25),
                                                               NULL),
                                              NULL));
}



void GameLayer::turn()
{
    if(_stageState == LANDSCAPE_LEFT)
    {
        turn(LANDSCAPE_RIGHT);
    }
    else
    {
        turn(LANDSCAPE_LEFT);
    }
}

void GameLayer::followChange(Node *followNode)
{
//    cocos2d::Vec2 followPos = cocos2d::Vec2(designResolutionSize.width/2,
//                                            designResolutionSize.height /3 + (_activeStage == _topStage?designResolutionSize.height /3 : 0));
    cocos2d::Vec2 followPos = cocos2d::Vec2(designResolutionSize.width/2,
                                            designResolutionSize.height/2);
    
    auto follow = CustomFollow::create(followNode, followPos, cocos2d::Vec2(5, 5));
    follow->_gameLayer = this;
    follow->_mapManager = _topStage->_mapManager;
    follow->setTag(1002);
    stopActionByTag(follow->getTag());
    runAction(follow);
}

void GameLayer::playerChange()
{
    if(!_isEnablePlayerChange) return;
    
    if(_activePlayer == MyChar::Lady)
    {
        _activePlayer = MyChar::Gentle;
        _controlMyChar = _topStage->_myChar;
        _activeStage = _topStage;
        _unActiveStage = _bottomStage;
    }
    else
    {
        _activePlayer = MyChar::Lady;
        _controlMyChar = _bottomStage->_myChar;
        _activeStage = _bottomStage;
        _unActiveStage = _topStage;
    }
    
    _activeStage->_myChar->stop();
    _unActiveStage->_myChar->stop();
//    
//    followChange(_controlMyChar);
//    turnBack(_turnDuration);
    
    Vec2 startPos = Vec2::ZERO;
    Vec2 targetPos = Vec2::ZERO;
    
    Node *parent = _unActiveStage->_myChar;
    while(parent)
    {
        startPos += parent->getPosition();
        parent = parent->getParent();
    }
    
    parent = _activeStage->_myChar;
    while(parent)
    {
        targetPos += parent->getPosition();
        parent = parent->getParent();
    }
    
    startPos *= getScale();
    targetPos *= getScale();
    
    
    
    auto hikaruyo = [=]()
    {
        Sprite *hikaru = Sprite::createWithTexture(_activeStage->_myChar->getTexture());
        hikaru->setPosition(designResolutionSize.width/2, targetPos.y);
        hikaru->setOpacity(255 * 0.8);
        hikaru->setAnchorPoint(Vec2(0.5, 0));
        hikaru->setScale(getScale());
        hikaru->setFlippedX(_activeStage->_myChar->isFlippedX());
        getParent()->addChild(hikaru);
        
        BlendFunc blend;
        blend.src = GL_SRC_ALPHA;
        blend.dst = GL_ONE;
        hikaru->setBlendFunc(blend);
        
        float duration = 0.8;
        hikaru->runAction(Sequence::create(Spawn::create(FadeOut::create(duration),
                                                         EaseOut::create(ScaleTo::create(duration, getScale() * 3), 2),
                                                         NULL),
                                           CallFunc::create([=]()
                                                            {
                                                                hikaru->removeFromParent();
                                                            }),
                                           NULL));
    };
    
    
    auto heartEff = [=]()
    {
        int heartNum = 5;
        for(int i=0; i<heartNum; i++)
        {
            Node *node = Node::create();
            Sprite *sp = Sprite::create("gameScene/ui/heart.png");
            node->addChild(sp);
            getParent()->addChild(node);
            node->setPosition(visibleCenter);
            
            node->setRotation(360/heartNum * i);
            sp->setScale(0);
            
            
            
            node->setPositionY(startPos.y);
            
            float duration = 0.5f;
            node->runAction(Sequence::create(EaseOut::create(RotateBy::create(duration, 180), 2),
                                             EaseBackOut::create(MoveTo::create(duration, Vec2(designResolutionSize.width/2, targetPos.y))),
                                             EaseOut::create(RotateBy::create(duration, -180), 2),
                                             NULL));
            sp->runAction(Sequence::create(Spawn::create(EaseOut::create(MoveBy::create(duration/2, Vec2(0, 300)), 2),
                                                         EaseOut::create(RotateBy::create(duration/2, -90), 2),
                                                         ScaleTo::create(duration/2, 1),
                                                         NULL),
                                           DelayTime::create(duration*2),
                                           Spawn::create(EaseOut::create(MoveBy::create(duration/2, Vec2(0, -300)), 2),
                                                         EaseOut::create(RotateBy::create(duration/2, -90), 2),
                                                         FadeOut::create(duration/2),
                                                         ScaleTo::create(duration/2, 1),
                                                         NULL),
                                           CallFunc::create([=]()
                                                            {
                                                                sp->removeFromParent();
                                                                node->removeFromParent();
                                                            }),
                                           NULL));
        }
    };
   
    runAction(Sequence::create(CallFunc::create(heartEff),
                               DelayTime::create(0.1),
                               CallFunc::create(heartEff),
                               NULL));
    runAction(Sequence::create(DelayTime::create(0.7),
                               CallFunc::create([=]()
                                                {
                                                    followChange(_controlMyChar);
                                                    turnBack(_turnDuration);
                                                }),
                               DelayTime::create(0.7),
                               CallFunc::create(hikaruyo),
                               NULL));
    
}

void GameLayer::onAcceleration(cocos2d::Acceleration *acc, cocos2d::Event *unused_event)
{
//    if(std::abs(acc->y) > 0.32)
//    {
//        if(acc->y > 0)
//        {
//            turn(LANDSCAPE_RIGHT);
//        }
//        else
//        {
//            turn(LANDSCAPE_LEFT);
//        }
//    }
}

bool GameLayer::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
//    cocos2d::log("touch");
    _touchPoint = touch->getLocation();
    return true;
}

void GameLayer::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
//    cocos2d::log("moved");
    if(_touchPoint.distance(touch->getLocation()) > designResolutionSize.width * 0.05)
    {
        if(_touchPoint.x < touch->getLocation().x) _controlMyChar->walk(Actor::RIGHT);
        else _controlMyChar->walk(Actor::LEFT);
    }
    else
    {
        _controlMyChar->stop();
    }
}

void GameLayer::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
    if(!_isTouchEnable)
    {
        _isTouchEnable = true;
        return;
    }
    
//    cocos2d::log("end");
    
    if(_controlMyChar->_state == MyChar::STOP&& touch->getLocation().distance(_touchPoint) < designResolutionSize.width * 0.05)
    {
        _controlMyChar->action();
    }
    
    _controlMyChar->stop();
    
    
}

void GameLayer::onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
    if(keyCode == cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW)
    {
        _controlMyChar->walk(Actor::RIGHT);
    }
    if(keyCode == cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW)
    {
        _controlMyChar->walk(Actor::LEFT);
    }
    if(keyCode == cocos2d::EventKeyboard::KeyCode::KEY_UP_ARROW)
    {
//        _controlMyChar->_move.y = 200;
//        _controlMyChar->_state = MyChar::State::WALK;
//        _controlMyChar->action();
        turn();
    }
    
    if(keyCode == cocos2d::EventKeyboard::KeyCode::KEY_SPACE)
    {
        _controlMyChar->action();
    }
    
    if(keyCode == cocos2d::EventKeyboard::KeyCode::KEY_DOWN_ARROW)
    {
        playerChange();
    }
}

void GameLayer::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
    if(keyCode == cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW)
    {
        _controlMyChar->stop();
        _topStage->_myChar->stop();
        _bottomStage->_myChar->stop();
    }
    if(keyCode == cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW)
    {
        _controlMyChar->stop();
//        _topStage->_myChar->stop();
//        _bottomStage->_myChar->stop();
    }
}












