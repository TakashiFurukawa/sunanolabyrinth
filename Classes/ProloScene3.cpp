//
//  ProloScene3.cpp
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/12.
//
//

#include "ProloScene3.h"
#include "MultiResolution.h"

bool ProloScene3::init()
{
    if(!Slideshow::init())
    {
        return false;
    }
    
    
    Vec2 offSet = Vec2::ZERO;
    
    // 背景
    this->appearSprite("PrologueScene/3/back.png", designResolutionSize/2, BACK);
    // 占い師と男
    this->appearSprite("PrologueScene/3/char.png", designResolutionSize/2, MOB);
    //女の子
    this->appearSprite("PrologueScene/3/lady_1.png", Vec2(designResolutionSize.width/2 + offSet.x,designResolutionSize.height/2 + offSet.y), LADY);
    
    this->runAction(Sequence::create(
                                     DelayTime::create(1.0f),
                                     CallFunc::create([=](){this->removeSprite(LADY);}),
                                     CallFunc::create([=](){this->appearSprite("PrologueScene/3/lady_2.png", Vec2(designResolutionSize.width/2 + offSet.x,designResolutionSize.height/2 + offSet.y), LADY);}),
                                     NULL));
    
    
    return true;
}

void ProloScene3::update(float delta)
{}