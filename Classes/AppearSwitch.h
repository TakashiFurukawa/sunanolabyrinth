//
//  AppearSwitch.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/05/22.
//
//

#ifndef __SunanoLabyrinth__AppearSwitch__
#define __SunanoLabyrinth__AppearSwitch__

#include "cocos2d.h"
#include "GimmickSwitch.h"

using namespace cocos2d;
using namespace CocosDenshion;

class AppearSwitch : public GimmickSwitch
{
public:
    
    int _id;
    
    static AppearSwitch *create();
    virtual bool init();
    void update(float delta);
    
    void switchOn() override;
};

#endif /* defined(__SunanoLabyrinth__AppearSwitch__) */
