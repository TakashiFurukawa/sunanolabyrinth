//
//  CrushBlock.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/23.
//
//

#ifndef __SunanoLabyrinth__CrushBlock__
#define __SunanoLabyrinth__CrushBlock__

#include "cocos2d.h"
#include "SwitchBlock.h"

using namespace cocos2d;
using namespace CocosDenshion;

class CrushBlock : public SwitchBlock
{
public:
    static CrushBlock *create();
    virtual bool init();
    void update(float delta);
    
    void switchOn() override;
    
    int _crushNo;
};

#endif /* defined(__SunanoLabyrinth__CrushBlock__) */
