//
//  ResultLayer.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/05/29.
//
//

#include "ResultLayer.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "UISpriteButton.h"
#include "GameScene.h"
#include "TestScene.h"
#include "SelectScene.h"
#include "SaveData.h"
#include "PrologueScene.h"

ResultLayer *ResultLayer::create(int time, int heart, bool isClear)
{
    ResultLayer *pRet = new ResultLayer();
    if(pRet && pRet->init(time, heart, isClear))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool ResultLayer::init(int time, int heart, bool isClear)
{
    if ( !Layer::init() ) return false;
    
    
    // 現在のマップからハートの数を調べる
    int stageHeartNum = 0;
    
    auto topMap = ((GameScene*)getParent())->_gameLayer->_topStage->_mapManager->_map;
    auto bottomMap = ((GameScene*)getParent())->_gameLayer->_bottomStage->_mapManager->_map;
    
    cocos2d::Size mapSize = topMap->getMapSize();
    cocos2d::Size tileSize = topMap->getTileSize();
    
    auto topHeartLayer = topMap->getLayer("heart");
    auto bottomHeartLayer = bottomMap->getLayer("heart");
    
    for(int y=0; y<(int)mapSize.height; y++)
    {
        for(int x=0; x<(int)mapSize.width; x++)
        {
            if((topHeartLayer && topHeartLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y)) != 0)||
               (bottomHeartLayer && bottomHeartLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y)) != 0))
            {
                stageHeartNum++;
            }
        }
    }
    
    log("heart %d", stageHeartNum);
    
    
    Sprite *black = Sprite::create();
    black->setTextureRect(cocos2d::Rect(0,0,designResolutionSize.width,designResolutionSize.height));
    black->setAnchorPoint(Vec2::ZERO);
    black->setPosition(Vec2::ZERO);
    black->setOpacity(0);
    black->setColor(Color3B::BLACK);
    black->setName("fade");
    addChild(black);
    
    black->runAction(Sequence::create(FadeTo::create(0.5, 255 * 0.7),
                                      NULL));
    
    _isClear = isClear;
    
    _clearTime = time;
    _time = 0;
    
    _heartNumMax = heart;
    _heartNum = 0;;
    
    _aidoMax = (float)_heartNumMax/(float)stageHeartNum;
    _aido = 0;
    
    int resultRank = 2;
    if(_aidoMax > 0.3) resultRank = 1;
    if(_aidoMax > 0.8) resultRank = 0;
    
    log("_aidoMax = %.2f", _aidoMax);
    
    
    _wakuSp = Sprite::create("result/waku.png");
    _wakuSp->setPosition(Vec2(designResolutionSize.width/2, designResolutionSize.height * 0.6));
    _wakuSp->setOpacity(255 * 0.3);
    addChild(_wakuSp);
    
    
    auto wakuSize = _wakuSp->getBoundingBox().size;
    
    _clipNode = ClippingNode::create();
    auto mask = Sprite::create("result/heartGuage.png");
    _clipNode->setPosition(Vec2(wakuSize.width/2, wakuSize.height/2));
    _clipNode->setStencil(mask);
    _clipNode->setInverted(false);
    _clipNode->setAlphaThreshold(0);
    _clipNode->addChild(mask);
    _wakuSp->addChild(_clipNode);
    
    string myCharType[2] = {"lady", "gentle"};
    for(int i=0; i<2; i++)
    {
        char fileName[128];
        sprintf(fileName, "result/%s%d.png", myCharType[i].c_str(), resultRank);
        _myCharSp[i] = Sprite::create(fileName);
        _myCharSp[i]->setPosition(Vec2(wakuSize.width/2 + wakuSize.width * 0.3 * (i==0?1:-1),
                                       wakuSize.height * 0.3));
        _myCharSp[i]->setScale(0.8);
        _wakuSp->addChild(_myCharSp[i]);
    }
    
    _guageSp = Sprite::create();
    _guageSp->setTextureRect(cocos2d::Rect(0, 0, mask->getBoundingBox().size.width, mask->getBoundingBox().size.height * _aido));
    _guageSp->setPosition(Vec2(0, -mask->getBoundingBox().size.height/2));
    _guageSp->setAnchorPoint(Vec2(0.5, 0));
    if(_isClear) _guageSp->setColor(Color3B(255, 200, 200));
    else _guageSp->setColor(Color3B::GRAY);
    _clipNode->addChild(_guageSp);
    
    
    string fontPath = "fonts/font_1_kokumr_1.00_rls.ttf";
    
    
    _titleLabel = Label::createWithTTF("セイセキ", fontPath, 100);
    _titleLabel->setPosition(Vec2(wakuSize.width/2, wakuSize.height * 0.9));
    _wakuSp->addChild(_titleLabel);
    
    char percentage[64];
    sprintf(percentage, "%d %%", (int)(_aido * 100));
    
    _percentLabel = Label::createWithTTF(percentage, fontPath, 80);
    _percentLabel->setPosition(wakuSize/2);
    _percentLabel->setColor(Color3B(108, 90, 85));
    _wakuSp->addChild(_percentLabel);
    
    
    _guageLabel = Label::createWithTTF("アイ％", fontPath, 60);
    _guageLabel->setPosition(Vec2(wakuSize.width/2, wakuSize.height * 0.75));
    _wakuSp->addChild(_guageLabel);
    
    
    char clearTimeStr[128];
    sprintf(clearTimeStr, "クリアタイム：%d", _time);
    _timeLabel = Label::createWithTTF(clearTimeStr, fontPath, 50);
    _timeLabel->setPosition(Vec2(wakuSize.width/2, wakuSize.height * 0.125));
    _wakuSp->addChild(_timeLabel);
    
    
    char heartNumStr[128];
    sprintf(heartNumStr, "アツメタハート：%dコ", _heartNum);
    _heartLabel = Label::createWithTTF(heartNumStr, fontPath, 50);
    _heartLabel->setPosition(_timeLabel->getPosition() + Vec2(0, 70));
    _wakuSp->addChild(_heartLabel);
    
    
    // セーブ
    
    bool nextStageOK = false;
    int stageNo = ((GameScene*)getParent())->_stageNo;
    auto save = SaveData::getInstance();
    
    char isStageClearKey[128];
    sprintf(isStageClearKey, "isStageClear_%d", stageNo);
    if(!save->getValueForKey(isStageClearKey, Value(false)).asBool())
    {
        save->setValueForKey(isStageClearKey, Value(isClear));
    }
    if(save->getValueForKey(isStageClearKey, Value(false)).asBool() || isClear) nextStageOK = true;
    
    
    char clearTimeKey[128];
    sprintf(clearTimeKey, "clearTime_%d", stageNo);
    
    char aidoKey[128];
    sprintf(aidoKey, "aido_%d", stageNo);
    
    
    if(save->getValueForKey(clearTimeKey, Value(1000000)).asInt() > time && isClear)
    {
        save->setValueForKey(clearTimeKey, Value(time));
        save->setValueForKey(aidoKey, Value(_aidoMax*100));
        
        Label *hiScoreLabel = Label::createWithTTF("ハイスコア更新", fontPath, 50);
        hiScoreLabel->setPosition(Vec2(designResolutionSize.width/2,
                                       designResolutionSize.height * 0.275));
        hiScoreLabel->setOpacity(0);
        addChild(hiScoreLabel);
        hiScoreLabel->runAction(Sequence::create(DelayTime::create(4),
                                                 FadeIn::create(1.0f),
                                                 NULL));
    }
    
    _changeScene = false;
    
    
    vector<string> buttonfileName = {
        "result/menubutton.png",
        "result/returnbutton.png",
        "result/nextbutton.png",
    };
    
    function<void()> func[] =
    {
        [=]()
        {
            
            if(_changeScene) return;
            
            _changeScene = true;
            
            Director::getInstance()->replaceScene(TransitionFade::create(0.5, SelectScene::create()));
        },
        
        [=]()
        {
            if(_changeScene) return;
            
            _changeScene = true;
            
            GameScene *gameScene = (GameScene*)getParent();
            
            auto newScene = GameScene::create(gameScene->_stageNo);
            Director::getInstance()->replaceScene(TransitionFade::create(0.5, newScene));
            newScene->start();
        },
        
        [=]()
        {
            
            if(_changeScene) return;
            
            GameScene *gameScene = (GameScene*)getParent();
            
            if(!nextStageOK)
            {
                // クリアしてなかったら次のステージにはいかせん。
                return;
            }
            
            _changeScene = true;
            
            Scene *newScene;
            
            if(gameScene->_stageNo == 12) newScene = PrologueScene::create();
            else
            {
                newScene = GameScene::create(gameScene->_stageNo+1);
                ((GameScene*)newScene)->start();
            }
            
            Director::getInstance()->replaceScene(TransitionFade::create(0.5, newScene));
            
            
        },
       
    };
    
    
    for(int i=0; i<buttonfileName.size(); i++)
    {
        UISpriteButton *button = UISpriteButton::create();
        button->setTexture(buttonfileName[i]);
        button->setPosition(Vec2(designResolutionSize.width/2 + designResolutionSize.width * 0.25 * (i-1),
                                 designResolutionSize.height * 0.2));
        button->_pushed = func[i];
        button->_isPushOnly = true;
        button->setName("button");
        addChild(button);
    }
    
    string kekkaStr = (isClear? "Clear !" : "Dead...");
    
    _kekkaLabel = Label::createWithTTF(kekkaStr, fontPath, 250);
    _kekkaLabel->setPosition(visibleCenter);
    _kekkaLabel->setScale(0);
    _kekkaLabel->setName("kekka");
    addChild(_kekkaLabel);
    
    hikkomeru();
    kekka(0);
    
    
    
    
    
    return true;
}

void ResultLayer::update(float delta)
{
    
}

void ResultLayer::hikkomeru()
{
    
    for(auto child : getChildren())
    {
        if(child->getName() == "kekka" || child->getName() == "fade") continue;
        
        auto pos = child->getPosition();
        child->setPosition(pos + Vec2(0, designResolutionSize.height));
        
    }
    for(auto child : _wakuSp->getChildren())
    {
        auto pos = child->getPosition();
        child->setPosition(pos + Vec2(0, designResolutionSize.height));
    }
    
    
}

void ResultLayer::kekka(float dt)
{
    float duration = 0.5;
    
    SimpleAudioEngine::getInstance()->playEffect("sound/SE/kekka.mp3");
    
    _kekkaLabel->runAction(Sequence::create(EaseOut::create(Spawn::create(RotateBy::create(duration, 360 * 7 + 15),
                                                          ScaleTo::create(duration, 1),
                                                          NULL),2),
                                            DelayTime::create(1.0),
                                            CallFunc::create([=]()
                                                             {
                                                                 appearAction(0);
                                                             }),
                                            NULL));
}


void ResultLayer::appearAction(float dt)
{
    int count = 0;
    float duration = 0.3;
    
    for(auto child : getChildren())
    {
        
        auto pos = child->getPosition() - Vec2(0, designResolutionSize.height);
        
        
        if(child->getName() == "button" || child->getName() == "fade") continue;
        
        child->runAction(Sequence::create(DelayTime::create(count * duration/5),
                                          EaseBackOut::create(MoveTo::create(duration, pos)),
                                          NULL));
        
        count ++;
    }
    
    for(auto child : _wakuSp->getChildren())
    {
        auto pos = child->getPosition() - Vec2(0, designResolutionSize.height);
        child->runAction(Sequence::create(DelayTime::create(count * duration/5),
                                          EaseBackOut::create(MoveTo::create(duration, pos)),
                                          NULL));
        
        count ++;
    }
    
    runAction(Sequence::create(DelayTime::create(count * duration/5 + 0.5),
                               CallFunc::create([=]()
                                                {
                                                    this->schedule(schedule_selector(ResultLayer::guageUpdate));
                                                }),
                               NULL));
    
}



void ResultLayer::guageUpdate(float dt)
{
    if(_aido < _aidoMax)
    {
        _aido+=0.01;
        
        char percentage[64];
        sprintf(percentage, "%d %%", (int)(_aido * 100));
        
        _percentLabel->setString(percentage);
        
        auto size = _clipNode->getStencil()->getBoundingBox().size;
        _guageSp->setTextureRect(cocos2d::Rect(0,0,size.width, size.height * _aido));
        
        if((int)(_aido*100)%2 == 0)
            SimpleAudioEngine::getInstance()->playEffect("sound/SE/piko.mp3", false, _aido + 0.5, 1, 1);
        
        if(_aido > 0.99)
            SimpleAudioEngine::getInstance()->playEffect("sound/SE/chin.mp3");
    }
    else
    {
        unschedule(schedule_selector(ResultLayer::guageUpdate));
        schedule(schedule_selector(ResultLayer::timeUpdate));
        
        if(_isClear) happyEffect();
    }
}


void ResultLayer::timeUpdate(float dt)
{
    if(_time < _clearTime)
    {
        _time+=1;
        
        // 何分何秒に変換
        int fun = _time / 60;
        int byo = _time - fun * 60;
        
        char str[64];
        sprintf(str, "クリアタイム：%02d:%02d", fun, byo);
        
        _timeLabel->setString(str);
        
        if(_time % 2 == 0)
            SimpleAudioEngine::getInstance()->playEffect("sound/SE/pon.mp3", false, _aido + 0.5, 1, 1);
        
    }
    else
    {
        unschedule(schedule_selector(ResultLayer::timeUpdate));
        schedule(schedule_selector(ResultLayer::heartNumUpdate));
    }
}


void ResultLayer::heartNumUpdate(float dt)
{
    if(_heartNum < _heartNumMax)
    {
        _heartNum++;
        
        char str[64];
        sprintf(str, "あつめたハート：%dコ", _heartNum);
        
        _heartLabel->setString(str);
        
        
        if(_heartNum % 2 == 0)
            SimpleAudioEngine::getInstance()->playEffect("sound/SE/pon.mp3", false, _aido + 0.5, 1, 1);
        
    }
    else
    {
        unschedule(schedule_selector(ResultLayer::heartNumUpdate));
        scheduleOnce(schedule_selector(ResultLayer::buttonAppear), 0);
    }
}

void ResultLayer::buttonAppear(float dt)
{
    int count = 0;
    float duration = 0.3;
    
    for(auto child : getChildren())
    {
        if(child->getName() != "button") continue;
        
        child->runAction(Sequence::create(DelayTime::create(count * duration/5),
                                          EaseBackOut::create(MoveBy::create(duration, Vec2(0, -designResolutionSize.height))),
                                          NULL));
        
        count ++;
    }
}

void ResultLayer::happyEffect()
{
    for(int i=0; i<100; i++)
    {
        Sprite *eff = Sprite::create("gameScene/ui/heart.png");
//        eff->setTextureRect(cocos2d::Rect(0,0,20,20));
        eff->setPosition(Vec2(designResolutionSize.width/2 + designResolutionSize.width * 0.4 * (i%2==0?1:-1),
                              -designResolutionSize.height * 0.1));
        eff->setColor(Color3B(255 * 0.8 + 255 * 0.2 * (rand()%100/100.0f),
                              255 * 0.8 + 255 * 0.2 * (rand()%100/100.0f),
                              255 * 0.8 + 255 * 0.2 * (rand()%100/100.0f)));
        eff->setOpacity(255 * 0.3);
        addChild(eff);
        
        auto duration = 1.5;
        eff->runAction(Sequence::create(Spawn::create(EaseOut::create(RotateBy::create(duration, 360 * (rand()%10) + 10 + rand()%360), 2),
                                                      EaseOut::create(JumpBy::create(duration+duration*(rand()%100/100.0f),
                                                                                     Vec2((designResolutionSize.width * 0.5 + designResolutionSize.width*0.7*(rand()%100/100.0f)) * (i%2==0?-1:1), 0),
                                                                                     designResolutionSize.height*0.6 + designResolutionSize.height*0.6 * (rand()%100/100.0f),
                                                                                     1) ,2),
                                                      NULL),
                                        CallFunc::create([=]()
                                                         {
                                                             eff->removeFromParent();
                                                         }),
                                        NULL));
    }
}




