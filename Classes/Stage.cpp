//
//  Stage.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/19.
//
//

#include "Stage.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"

#include "CustomFollow.h"


bool Stage::init(MyChar::PlayerType playerType, int stageNo)
{
    if ( !Node::init() ) return false;
    
    setName("stage");
    
    _mapManager = MapManager::create(stageNo);
    addChild(_mapManager, 1);
    
    _playerType = playerType;

    _myChar = new MyChar();
    
    _actorManager = new ActorManager();
    _actorManager->_mapManager = _mapManager;
    addChild(_actorManager, 2);
    _actorManager->init();
    _actorManager->autorelease();
    
    _sand = new Sand();
    _sand->_mapManager = _mapManager;
    _sand->_actorManager = _actorManager;
    _sand->_myChar = _myChar;
    _sand->init();
//    _sand->setCascadeOpacityEnabled(true);
    _sand->autorelease();
    addChild(_sand, 3);
    
    
    _stageBack = cocos2d::Sprite::create();
    _stageBack->setTextureRect(cocos2d::Rect(0,0,
                                             _mapManager->_stageSize.width,
                                             _mapManager->_stageSize.height));
    _stageBack->setAnchorPoint(cocos2d::Vec2(0,0));
    _stageBack->setColor(cocos2d::Color3B(255, 244, 219));
    _stageBack->setOpacity(255 * 0.2);
    addChild(_stageBack, -1);
    
    if(playerType == MyChar::Gentle)
    {
        _timeState = HIRU;
    }
    else
    {
        _timeState = YORU;
    }
    
    
    // フェード用ブラック
    _black = Sprite::create();
    _black->setTextureRect(cocos2d::Rect(0,0, designResolutionSize.width, designResolutionSize.height/2));
    _black->setAnchorPoint(Vec2(0, 0));
    _black->setPosition(Vec2(0, designResolutionSize.height/2 * MapManager::stageSubNo));
    _black->setColor(Color3B::BLACK);
    _black->setOpacity(0);
    getParent()->getParent()->addChild(_black);
    
    follow(true);
    
    scheduleUpdate();
    return true;
}

void Stage::update(float delta)
{
    
}

void Stage::follow(bool isFollow)
{
    if(isFollow)
    {
        auto follow = CustomFollow::create(_myChar, Vec2(designResolutionSize.width/2 / getParent()->getScale(), 0), cocos2d::Vec2(5, 5));
        follow->_gameLayer = (GameLayer*)getParent();
        follow->_mapManager = _mapManager;
        follow->setTag(1002);
        stopActionByTag(follow->getTag());
        runAction(follow);
    }
    
    else
    {
        stopActionByTag(1002);
    }
}

void Stage::followChange(Node *followNode)
{
    //    cocos2d::Vec2 followPos = cocos2d::Vec2(designResolutionSize.width/2,
    //                                            designResolutionSize.height /3 + (_activeStage == _topStage?designResolutionSize.height /3 : 0));
//    cocos2d::Vec2 followPos = cocos2d::Vec2(designResolutionSize.width/2,
//                                            designResolutionSize.height/2);
//    
//    auto follow = CustomFollow::create(followNode, followPos, cocos2d::Vec2(5, 5));
//    follow->_gameLayer = this;
//    follow->_mapManager = _topStage->_mapManager;
//    follow->setTag(1002);
//    stopActionByTag(follow->getTag());
//    runAction(follow);
    
    auto follow = CustomFollow::create(followNode, Vec2(designResolutionSize.width/2 / getParent()->getScale(), 0), cocos2d::Vec2(5, 5));
    follow->_gameLayer = (GameLayer*)getParent();
    follow->_mapManager = _mapManager;
    follow->setTag(1002);
    stopActionByTag(follow->getTag());
    runAction(follow);
}












