//
//  AppearSwitch.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/05/22.
//
//

#include "AppearSwitch.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "GameLayer.h"
#include "AppearBlock.h"

AppearSwitch *AppearSwitch::create()
{
    AppearSwitch *pRet = new AppearSwitch();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool AppearSwitch::init()
{
    if ( !GimmickSwitch::init() ) return false;
    
    setColor(Color3B(100, 255, 100));
    
    
    scheduleUpdate();
    return true;
}

void AppearSwitch::update(float delta)
{
    GimmickSwitch::update(delta);
}

void AppearSwitch::switchOn()
{
    
    if(_isOn) return;
    
    GimmickSwitch::switchOn();
    
    
    _isOn = true;
    
    GameLayer *gameLayer = (GameLayer*)getParent()->getParent()->getParent();
    gameLayer->_isEnablePlayerChange = false;
    
    gameLayer->_controlMyChar->_isWalkOK = false;
    
    auto func = [=]()
    {
        
        
        // 砂の上下一時停止
        gameLayer->_topStage->_sand->_isMove = false;
        gameLayer->_bottomStage->_sand->_isMove = false;
        
        
        // スイッチの発動とカメラ切り替え
        auto actorManager = gameLayer->_unActiveStage->_actorManager;
        auto children = actorManager->getChildren();
        cocos2d::Node *followNode;
        for(auto child : children)
        {
            AppearBlock *block = (AppearBlock*)child;
            
            if(block->_actorType == SWITCH_BLOCK && block->_id == _id)
            {
                block->switchOn();
                followNode = block;
            }
        }
        if(followNode)
            gameLayer->_unActiveStage->followChange(followNode);
        
        
        stick->runAction(Sequence::create(FadeOut::create(0.5), NULL));
    };
    
    
    stick->runAction(cocos2d::Sequence::create(cocos2d::EaseBackIn::create(cocos2d::RotateTo::create(0.25, -stick->getRotation())),
                                               cocos2d::DelayTime::create(0.5),
                                               cocos2d::CallFunc::create(func),
                                               NULL));
}