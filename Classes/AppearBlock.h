//
//  AppearBlock.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/05/22.
//
//

#ifndef __SunanoLabyrinth__AppearBlock__
#define __SunanoLabyrinth__AppearBlock__

#include "cocos2d.h"
#include "Actor.h"
#include "SwitchBlock.h"

using namespace cocos2d;
using namespace CocosDenshion;

class AppearBlock : public SwitchBlock
{
public:
    int _id;
    
    static AppearBlock *create();
    virtual bool init();
    void update(float delta);
    
    void switchOn()override;
};

#endif /* defined(__SunanoLabyrinth__AppearBlock__) */
