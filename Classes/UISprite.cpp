//
//  UISprite.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/20.
//
//

#include "UISprite.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"

UISprite *UISprite::create(string fileName, cocos2d::Vec2 origin)
{
    UISprite *pRet = new UISprite();
    if(pRet && pRet->init(fileName, origin))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool UISprite::init(string fileName, cocos2d::Vec2 origin)
{
    if ( !cocos2d::Sprite::init() ) return false;
    
    initWithFile(fileName);
    
    _origin = origin;
    
    scheduleUpdate();
    return true;
}

void UISprite::update(float delta)
{
    
}