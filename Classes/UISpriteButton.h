//
//  UISpriteButton.h
//  FPaint
//
//  Created by Furukawa on 2015/05/04.
//
//

#ifndef __FPaint__UISpriteButton__
#define __FPaint__UISpriteButton__

#include "cocos2d.h"

using namespace cocos2d;
using namespace CocosDenshion;
using namespace std;

class UISpriteButton : public Sprite
{
public:
    static UISpriteButton *create();
    static UISpriteButton *create(function<void()> pushed);
    static UISpriteButton *create(function<void()> pushed, function<void()> released);
    static UISpriteButton *create(string fileName);
    static UISpriteButton *create(string fileName, function<void()> pushed);
    static UISpriteButton *create(string fileName, function<void()> pushed, function<void()> released);
    
    virtual bool init();
    virtual bool init(function<void()> pushed);
    virtual bool init(function<void()> pushed, function<void()> released);
    virtual bool init(string fileName);
    virtual bool init(string fileName, function<void()> pushed);
    virtual bool init(string fileName, function<void()> pushed, function<void()> released);
    
    void setBaseScale(float scale);
    
    void update(float delta);
    
    function<void ()> _pushed;
    function<void ()> _released;
    
    float _baseScale;
    
    bool _isPushed;
    bool _isPushOnly;
    
    bool onTouchBegan(Touch *touch, Event *event);
    void onTouchEnded(Touch *touch, Event *event);
    void onTouchMoved(Touch *touch, Event *event);
};

#endif /* defined(__FPaint__UISpriteButton__) */
