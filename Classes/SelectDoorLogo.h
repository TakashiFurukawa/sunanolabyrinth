//
//  SelectDoorLogo.h
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/14.
//
//

#ifndef __SunanoLabyrinth__SelectDoorLogo__
#define __SunanoLabyrinth__SelectDoorLogo__

#include "cocos2d.h"
USING_NS_CC;

class SelectDoorLogo : public Node
{
public:
    CREATE_FUNC(SelectDoorLogo);
    bool init();
    void update(float delta);
    
    bool isSelect;
    
    Sprite *doorSp;
    
    Vector<Sprite*> doorSps;
    std::vector<bool> isBroken;
    Texture2D *doorTex;
    
    void startOpenAnim();
    void startCloseAnim();
    void suikomare();
    void death();
    void shake();
private:
    void fadeOunt(float delta);
    
    bool isDeath;
    
};

#endif /* defined(__SunanoLabyrinth__SelectDoorLogo__) */
