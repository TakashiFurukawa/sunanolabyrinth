//
//  CustomFollow.cpp
//  OreTobe
//
//  Created by Furukawa on 2014/12/27.
//
//

#include "CustomFollow.h"
#include "MultiResolution.h"
#include "MapManager.h"

CustomFollow* CustomFollow::create(cocos2d::Node* followNode)
{
    return create(followNode, visibleCenter, cocos2d::Vec2(10, 10));
}

CustomFollow* CustomFollow::create(cocos2d::Node* followNode, cocos2d::Vec2 screenPos)
{
    return create(followNode, screenPos, cocos2d::Vec2(10, 10));
}

CustomFollow* CustomFollow::create(cocos2d::Node* followNode, cocos2d::Vec2 screenPos, cocos2d::Vec2 weight)
{
    
    CustomFollow *follow = new CustomFollow();
    if (follow && follow->initWithTarget(followNode, cocos2d::Rect::ZERO) && follow->init(screenPos, weight))
    {
        follow->autorelease();
        return follow;
    }
    CC_SAFE_DELETE(follow);
    return nullptr;
    
}

bool CustomFollow::init(cocos2d::Vec2 screenPos, cocos2d::Vec2 weight)
{
    _screenPos = screenPos;
    _weight = weight;
    
    return true;
}

void CustomFollow::step(float dt)
{
    CC_UNUSED_PARAM(dt);
    
    if(_boundarySet)
    {
        if(_boundaryFullyCovered)
            return;
        
        cocos2d::Vec2 tempPos = _screenPos - _followedNode->getPosition();
        
        _target->setPosition(cocos2d::Vec2(cocos2d::clampf(tempPos.x, _leftBoundary, _rightBoundary),
                                  cocos2d::clampf(tempPos.y, _bottomBoundary, _topBoundary)));
    }
    else
    {
        cocos2d::Vec2 followNodePos = _followedNode->getPosition();
        auto parent = _followedNode->getParent();
        while (parent != _target) {
            followNodePos += parent->getPosition();
            parent = parent->getParent();
        }
        followNodePos *= _target->getScale();
        cocos2d::Vec2 targetPos = _target->getPosition();
        cocos2d::Vec2 screenPos = _screenPos;
        
//        if(_gameLayer->_activePlayer == MyChar::Lady)
//        {
//            followNodePos += _gameLayer->_bottomStage->getPosition();
//        }
        
        if(_gameLayer->_stageState == GameLayer::StageState::LANDSCAPE_RIGHT)
        {
//            followNodePos.x *= -1;
//            followNodePos.y *= -1;
        }
        
        
        cocos2d::Vec2 move = (screenPos - followNodePos) - targetPos;
        
        move.x /= _weight.x;
        move.y /= _weight.y;
        
        move.y = 0;
        
        cocos2d::Vec2 pos = _target->getPosition() + move;
        
//        pos = cocos2d::Vec2(floor(pos.x), floor(pos.y));
        
        _target->setPosition(pos);
        
        
        cocos2d::Size stageSize = _mapManager->_stageSize;
        
        
        // ステージからカメラがはみ出ないようにする
//        if(_target->getPositionX() > 0) _target->setPositionX(0);
//        if(_target->getPositionX() < -stageSize.width + designResolutionSize.width)
//            _target->setPositionX(-stageSize.width + designResolutionSize.width);
//        
//        if(_target->getPositionY() > 0) _target->setPositionY(0);
//        if(_target->getPositionY() < -stageSize.height + designResolutionSize.height)
//            _target->setPositionY(-stageSize.height + designResolutionSize.height);
//
        
    }
}