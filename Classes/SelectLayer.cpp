//
//  SelectLayer.cpp
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/05/19.
//
//

#include "SelectLayer.h"
#include "SelectScene.h"

bool SelectLayer::init()
{
    if(!Layer::init())
    {
        return false;
    }
    
    isOkSceneChange = false;
    
    back = BackGrounds::create();
    this->addChild(back,200);
    
      
    doorN = SelectDoorLogos::create();
    this->addChild(doorN,1000);
    
    
    //--
    isFirstPlay = true;
    
    UserDefault *userDef = UserDefault::getInstance();
    isFirstPlay = userDef->getBoolForKey(KEY_FIRST_PLAY,true);
    log("%d",userDef->getBoolForKey(KEY_FIRST_PLAY));
    //--
    return true;
}

void SelectLayer::removeAndGameStart()
{
    ((SelectScene*)this->getParent())->gameStart();
    log("!!!!!!!!!!!!!REMO");
    this->removeFromParent();
}

bool SelectLayer::getSceneChangeFlg()
{
    return isOkSceneChange;
}
void SelectLayer::setSceneChangeFlg(bool isOk)
{
    isOkSceneChange = isOk;
}