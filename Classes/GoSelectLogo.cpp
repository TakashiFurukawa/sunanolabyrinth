//
//  GoSelectLogo.cpp
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/03.
//
//

#include "GoSelectLogo.h"
#include "MultiResolution.h"
bool GoSelectLogo::init()
{
    if(!Node::init())
    {
        return false;
    }
    
    Label *lb;
    
    lb = Label::createWithTTF("Touch Screen", "fonts/bokutachi.otf", 70);
    lb->setPosition(designResolutionSize.width/2, designResolutionSize.height/3);
    lb->setTag(TOUCH_SCREEN_KEY);
    this->addChild(lb);
    return true;
}

void GoSelectLogo::changeAction()
{
    Label *lb = ((Label*)this->getChildByTag(TOUCH_SCREEN_KEY));
    
    auto seq = Sequence::create(
                                TargetedAction::create(lb, FadeOut::create(0.2f)),
                                TargetedAction::create(lb, FadeIn::create(0.2f)),
                                NULL);
    auto rep = Repeat::create(seq, 3);
    
    auto act = Sequence::create(
                                rep,
                                TargetedAction::create(lb, FadeOut::create(1.0f)),
                                NULL);
    
    this->runAction(act);
    
    
    
}