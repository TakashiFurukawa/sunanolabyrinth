//
//  TitleLogo.cpp
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/02.
//
//

#include "TitleLogo.h"
#include "MultiResolution.h"

bool TitleLogo::init()
{
    if(!Node::init())
    {
        return false;
    }
    
    echoEffTiming = 30;// 初期値
    flamCnt = 0;
    
    logoSp = Sprite::create("TitleScene/logo1.png");
    logoSp->setPosition(Vec2(designResolutionSize.width/2,designResolutionSize.height/1.5f));
    
    // 画像が横画面サイズだから少し小さくしておく今だけ
    logoSp->setScale(1.0f);
    this->addChild(logoSp,1000);
    
    //logoSp ->setColor(Color3B::WHITE);
    
    
    srand((unsigned int)time(NULL));
    
    
    this->scheduleUpdate();
    return true;
}

void TitleLogo::update(float delta)
{
    if(flamCnt % echoEffTiming == 0)
    {
        echoEffect();
    }
    flamCnt++;
}

void TitleLogo::echoEffect()
{
    Sprite *effSp = Sprite::createWithTexture(logoSp->getTexture());
    this->addChild(effSp,500);
    effSp->setPosition(logoSp->getPosition());
    effSp->setScale(0.75f);
    effSp->setOpacity(0);
    
    //ブレンド
    BlendFunc blend;
    blend.src = GL_ONE_MINUS_DST_COLOR;
    blend.dst = GL_ONE;
    
    effSp->setBlendFunc(blend);
    
    
    float toScale = logoSp->getScale() + (0.3f + (rand()%3 / 10));
    
    float delay = 0.8f + (float)(rand()%10 /10.0f);
    auto fadein = TargetedAction::create(effSp, FadeIn::create(delay));
    auto scaleAct =TargetedAction::create(effSp,EaseIn::create(ScaleTo::create(delay, toScale),2.0f));
    auto fadeOut = TargetedAction::create(effSp,FadeOut::create(delay));
    
    auto spw = Spawn::create(scaleAct,fadeOut,NULL);
    
    auto seq = Sequence::create(fadein,spw,
                                DelayTime::create(delay),
                                CallFunc::create([=](){effSp->removeFromParent();}),
                                NULL);
    this->runAction(seq);
    
    echoEffTiming = 60 + rand()%100;
    
}







