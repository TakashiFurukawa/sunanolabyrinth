//
//  StageBlock.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/18.
//
//

#ifndef __SunanoLabyrinth__StageBlock__
#define __SunanoLabyrinth__StageBlock__

#include "cocos2d.h"
#include "Actor.h"


using namespace CocosDenshion;

class StageBlock : public Actor
{
public:
    static StageBlock *create();
    virtual bool init();
    void update(float delta);
};

#endif /* defined(__SunanoLabyrinth__StageBlock__) */
