//
//  PauseLayer.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/06/08.
//
//

#ifndef __SunanoLabyrinth__PauseLayer__
#define __SunanoLabyrinth__PauseLayer__

#include "cocos2d.h"
#include "UISpriteButton.h"
#include "GameScene.h"

using namespace cocos2d;
using namespace CocosDenshion;

class PauseLayer : public Layer
{
public:
    static PauseLayer *create();
    virtual bool init();
    void update(float delta);
    
    UISpriteButton *_resume;
    UISpriteButton *_title;
    UISpriteButton *_reset;
    
    GameScene *_gameScene;
    
    void appear(bool isAppear);
};

#endif /* defined(__SunanoLabyrinth__PauseLayer__) */
