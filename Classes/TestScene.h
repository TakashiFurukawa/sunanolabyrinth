//
//  TestScene.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/28.
//
//

#ifndef __SunanoLabyrinth__TestScene__
#define __SunanoLabyrinth__TestScene__

#include "cocos2d.h"

using namespace cocos2d;
using namespace CocosDenshion;

class TestScene : public Scene
{
public:
    static TestScene *create();
    virtual bool init();
    void update(float delta);
    
    Sprite *button1;
    Sprite *button2;
    
    bool changeScene;
    
    bool onTouchBegan(Touch *touch, Event *event);
    void onTouchMoved(Touch *touch, Event *event);
    void onTouchEnded(Touch *touch, Event *event);
};

#endif /* defined(__SunanoLabyrinth__TestScene__) */
