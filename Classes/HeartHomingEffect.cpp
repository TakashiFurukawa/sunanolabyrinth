//
//  HeartHomingEffect.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/21.
//
//

#include "HeartHomingEffect.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"

cocos2d::Vec2 getWorldPos(cocos2d::Node *node)
{
    auto pos = node->getPosition();
    auto parent = node->getParent();
    
    while (parent)
    {
        pos += parent->getPosition();
        parent = parent->getParent();
    }
    
    return pos;
}

HeartHomingEffect *HeartHomingEffect::create(MyChar *start, MyChar *goal)
{
    auto pRet = new HeartHomingEffect();
    if(pRet && pRet->init(start, goal))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool HeartHomingEffect::init(MyChar *start, MyChar *goal)
{
    if ( !cocos2d::Sprite::init() ) return false;
    
    
    setTexture("gameScene/ui/heart.png");
    
    setRotation(0);
    setPosition(getWorldPos(start));
    
    setOpacity(255 * 0.7);
    
    _moveSpeed = 100;
    _rotaSpeed = 20;
    
    _start = start;
    _goal = goal;
    
    _isCollision = false;
    
    scheduleUpdate();
    return true;
}

void HeartHomingEffect::update(float delta)
{
    
}













