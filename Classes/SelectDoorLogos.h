//
//  SelectDoorLogos.h
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/15.
//
//

#ifndef __SunanoLabyrinth__SelectDoorLogos__
#define __SunanoLabyrinth__SelectDoorLogos__

#include "cocos2d.h"
USING_NS_CC;

#include "SelectDoorLogo.h"

#define StageMAX 12

class SelectDoorLogos : public Node
{
public:
    CREATE_FUNC(SelectDoorLogos);
    bool init();
    void update(float delta);
    
    SelectDoorLogo *doorLogos[StageMAX];
    Sprite *numberLogos[StageMAX];
    
    
    void sceneChangeAnim(int selectNum);
    
    // タッチ
    bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    
private:
    bool isEnterStage;
    
    
};

#endif /* defined(__SunanoLabyrinth__SelectDoorLogos__) */
