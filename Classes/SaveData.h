//
//  SaveData.h
//  OreTobe
//
//  Created by Furukawa on 2015/01/26.
//
//

#ifndef __OreTobe__SaveData__
#define __OreTobe__SaveData__

#include <stdio.h>
#include "cocos2d.h"
#include <string>
#include <vector>

using namespace std;
using namespace cocos2d;

class SaveData {
    
    ValueMap loadedValueMap;
    
public:
    
    SaveData();
    ~SaveData();
    
    static const string SAVE_FILE_NAME;
    
    static SaveData *getInstance();
    static SaveData *instance;
    
    void setValueForKey(string key, Value value);
    Value getValueForKey(string key, Value defaultValue);
    
    void flush();
    
};

#endif /* defined(__OreTobe__SaveData__) */
