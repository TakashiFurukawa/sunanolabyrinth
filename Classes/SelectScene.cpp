//
//  SelectScene.cpp
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/05/19.
//
//

#include "SelectScene.h"
#include "GameScene.h"

bool SelectScene::init()
{
    if(!Scene::init())
    {
        return false;
    }
    
    selectLayer = SelectLayer::create();
    this->addChild(selectLayer);
    
    
    
    isChange = false;
    this->scheduleUpdate();
    
    return true;
}

void SelectScene::gameStart()
{
    gameScene->start();
}

void SelectScene::update(float delta)
{
}

void SelectScene::sceneChange(int selectNum)
{
    
    if(!isChange)
    {
        gameScene = GameScene::create(selectNum);
        this->addChild(gameScene,-1);
        
        isChange = true;
    }
}





