//
//  LoveHeart.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/26.
//
//

#ifndef __SunanoLabyrinth__LoveHeart__
#define __SunanoLabyrinth__LoveHeart__

#include "cocos2d.h"
#include "Stage.h"

using namespace cocos2d;
using namespace CocosDenshion;

class LoveHeart : public cocos2d::Sprite
{
public:
    static LoveHeart *create();
    virtual bool init();
    void update(float delta);
    
    bool _isGet;
    void catchHeart();
    
    Stage *_stage;
};

#endif /* defined(__SunanoLabyrinth__LoveHeart__) */
