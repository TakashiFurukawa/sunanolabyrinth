//
//  CustomFollow.h
//  OreTobe
//
//  Created by Furukawa on 2014/12/27.
//
//

#ifndef __OreTobe__CustomFollow__
#define __OreTobe__CustomFollow__

#include <stdio.h>
#include "cocos2d.h"
#include "MapManager.h"
#include "GameLayer.h"



class CustomFollow : public cocos2d::Follow {
    
    
public:
    
    static CustomFollow* create(cocos2d::Node *followedNode);
    static CustomFollow* create(cocos2d::Node *followedNode, cocos2d::Vec2 screenPos);
    static CustomFollow* create(cocos2d::Node *followedNode, cocos2d::Vec2 screenPos, cocos2d::Vec2 weight);
    
    bool init(cocos2d::Vec2 screenPos, cocos2d::Vec2 weight);
    void step(float dt);
    
    
    MapManager *_mapManager;
    GameLayer *_gameLayer;
    
private:
    
    cocos2d::Vec2 _screenPos;
    cocos2d::Vec2 _weight;
    
};

#endif /* defined(__OreTobe__CustomFollow__) */
