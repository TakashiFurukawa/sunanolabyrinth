//
//  TitleBG.cpp
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/02.
//
//

#include "TitleBG.h"
#include "MultiResolution.h"

bool TitleBG::init()
{
    if(!Sprite::init())
    {
        return false;
    }
    this->initWithFile("TitleScene/back.png");// かり
    this->setPosition(designResolutionSize/2);
    return true;
}