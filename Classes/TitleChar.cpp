//
//  TitleChar.cpp
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/03.
//
//

#include "TitleChar.h"
#include "MultiResolution.h"
bool TitleChar::init()
{
    if(!Node::init())
    {
        return false;
    }
    
    
    pai = 0.0f;
    isFadeOutFinish = false;
    waveX = 0.0f;
    fadeStart = false;
    
    
    
    gentleSp = Sprite::create("TitleScene/boy.png");
    //gentleSp->setOpacity(0);
    gentleSp->setPosition(Vec2(designResolutionSize.width/6,designResolutionSize.height/4));
    this->addChild(gentleSp);
    
    
    
    //ラスタスクロールもどきしてみるよ！！-------------------------------------------------------------
    // ラスタする基テクスチャ
    ladyTex = TextureCache::sharedTextureCache()->addImage("TitleScene/girl.png");
    ladyTex->retain();
    
    ladySpPos.x = gentleSp->getPositionX() + ladyTex->getPixelsWide();
    ladySpPos.y = gentleSp->getPositionY() + ladyTex->getPixelsHigh()/2;
    
    // テクスチャを千切りにするんだってさ
    for(int line = 0; line < ladyTex->getPixelsHigh(); line++)
    {
        //千切りでベクターに格納
        ladyTexLines.pushBack(Sprite::createWithTexture(ladyTex, cocos2d::Rect(0, line, ladyTex->getPixelsWide(), 1)));
    }
    // addchild
    for(int i = 0; i < ladyTexLines.size(); i++)
    {
        ladyTexLines.at(i)->setPosition(Vec2(ladySpPos.x,ladySpPos.y - i));
        this->addChild(ladyTexLines.at(i),100);
    }
    //-------------------------------------------------------------------------------------------
    this->scheduleUpdate();
    return true;
}

void TitleChar::update(float delata)
{
    
//    static int a= 0;
//    if(a==120)
//    {
//        changeAct();
//    }
//    a++;
//    
//    
    if(!isFadeOutFinish)
    {
        if(fadeStart == true)
        {
            for(auto sp : ladyTexLines)
            {
                if(sp->getOpacity() > 0)
                {
                    return;
                }
            }
            isFadeOutFinish = true;
            log("okokokokokokok");
        }
    }
}

void TitleChar::changeAct()
{
    fadeOutToUnder();
    
    this->schedule(schedule_selector(TitleChar::rasterScroll));
    
}

void TitleChar::rasterScroll(float delta)
{
    pai += 5;   //角度加算
    //角度を360に収める
    if(pai > 360)
    {
        pai -= 360;
    }
    
    float posX = ladySpPos.x;
    float rad = 0.0f;
    float addX = 0.0f;
    float shuhasu = 5;
    
    // 急に始まるとッパってなるから徐々に揺れ幅増やす
    if(waveX<20)
    {
        waveX+=0.05f;
    }
    for(int y = 0; y < ladyTexLines.size(); y++)
    {
        // 1Lineごとの角度
        rad = (M_PI/180) * (pai + y *shuhasu);
        // 1LineごとのXかさんち
        addX = waveX *cos(rad);
        
        // 位置ずらす
        ladyTexLines.at(y)->setPositionX(posX + addX);
    }
}

void TitleChar::fadeOutToUnder()
{
    float delay = 0.0f;
    for(int y = ladyTexLines.size()-1; y >= 0; y--)
    {
        auto fadeOut = FadeOut::create(delay);
        ladyTexLines.at(y)->runAction(fadeOut);
        
        delay += 0.03f;
    }
    fadeStart = true;
}










