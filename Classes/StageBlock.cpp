//
//  StageBlock.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/18.
//
//

#include "StageBlock.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"

StageBlock *StageBlock::create()
{
    StageBlock *pRet = new StageBlock();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool StageBlock::init()
{
    if ( !Actor::init() ) return false;
    
    setTexture("gameScene/block/0.png");
    
    _actorType = KOTEI;
    
    scheduleUpdate();
    return true;
}

void StageBlock::update(float delta)
{
    
}



