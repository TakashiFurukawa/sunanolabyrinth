//
//  InfoLabel.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/06/04.
//
//

#ifndef __SunanoLabyrinth__InfoLabel__
#define __SunanoLabyrinth__InfoLabel__

#include "cocos2d.h"
#include "Label.h"

using namespace cocos2d;
using namespace CocosDenshion;
using namespace std;

class InfoLabel : public Sprite
{
public:
    static InfoLabel *create(string text);
    virtual bool init(string text);
    void update(float delta);
    
    nicos2d::Label *label;
    
    string _text;
    Vec2 _origin;
    
    bool _isAppeared;
    
    void appear(bool isAppear);
};

#endif /* defined(__SunanoLabyrinth__InfoLabel__) */
