//
//  MyChar.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/14.
//
//

#include "MyChar.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "AnimationManager.h"
#include "GameLayer.h"
#include "ActorManager.h"
#include "GimmickSwitch.h"

GameLayer *getGameLayer(cocos2d::Node *child)
{
    GameLayer *gameLayer;
    auto parent = child->getParent();
    while (parent->getName() != "gameLayer") parent = parent->getParent();
    gameLayer = (GameLayer *) parent;
    
    return gameLayer;
}

bool MyChar::init(PlayerType playerType)
{
    if ( !Actor::init() ) return false;
    
    char name[128];
    
    if(playerType == Gentle)
    {
        setTexture("gameScene/myChar/man0.png");
        sprintf(name, "gentle");
    }
    else
    {
        setTexture("gameScene/myChar/woman0.png");
        sprintf(name, "lady");
    }
    _actorType = PLAYER;
    _playerType = playerType;
    
    _moveSpeed = 10;
    _isAction = false;
    _isActorClimb = false;
    _inSandPercent = 0;
    _isWalkOK = true;
    _preLanding = false;
    _isStar = false;
    _isWarping = false;
    
    _frameCnt = 0;
    
//    BlendFunc blend;
//    blend.src = GL_SRC_ALPHA;
//    blend.dst = GL_ONE;
//    setBlendFunc(blend);
    
    animationNames[ANIM_STAND] = name;
    animationNames[ANIM_WALK] = name;
    
    animationNames[ANIM_STAND] += "_stand";
    animationNames[ANIM_WALK] += "_walk";
    
    AnimationManager::addAnimationCache("gameScene/myChar/" + animationNames[ANIM_STAND] + "/", animationNames[ANIM_STAND], 1, 100);
    AnimationManager::addAnimationCache("gameScene/myChar/" + animationNames[ANIM_WALK] + "/", animationNames[ANIM_WALK], 8, 0.07);
    
    runAction(AnimationManager::createRepeat(animationNames[ANIM_STAND], -1));
    
    scheduleUpdate();
    
    
    // se preload
    auto se = SimpleAudioEngine::getInstance();
    se->preloadEffect("sound/SE/walk_gentle0.mp3");
    se->preloadEffect("sound/SE/walk_gentle1.mp3");
    se->preloadEffect("sound/SE/walk_lady0.mp3");
    se->preloadEffect("sound/SE/walk_lady1.mp3");
    
    return true;
}

void MyChar::update(float delta)
{
    if(_isAction) _isAction = false;
    
    if(_move.x > 0 && !isFlippedX())
    {
        setFlippedX(true);
    }
    if(_move.x < 0 && isFlippedX())
    {
        setFlippedX(false);
    }
    
    if(_state == WALK)
    {
        _move.x = _moveSpeed * (_dir == RIGHT ? 1 : -1);
    }
    
//    log("x %.2f, y %.2f", _move.x, _move.y);
    
    // 登っているときは重力でさがらない
    if(!_isClimbing) _move.y -= 10;
    
    // 砂で下がる移動スピード
    if(_inSandPercent && !_isStar)
    _move *= (1-_inSandPercent * 0.9);
    
    
    // スター状態
    if(_isStar)
    {
//        _move *= 2;
        
        if(_frameCnt % 10 == 0)
        {
            Sprite *eff = Sprite::create("gameScene/gimmick/star.png");
            eff->setPosition(getPosition());
            getParent()->getParent()->addChild(eff);
            
            eff->runAction(Sequence::create(Spawn::create(ScaleBy::create(1.0, 2),
                                                          FadeOut::create(1.0),
                                                          NULL),
                                            CallFunc::create([=]()
                                                             {
                                                                 eff->removeFromParent();
                                                             }),
                                            NULL));
        }
    }
    
    _preMove = _move;
    move();
    
//    if(_playerType == Gentle)
//    {
//        if(_isLanding) printf("地上\t");
//        else printf("＿\t");
//        
//        if(_isClimbing) printf("登っている\n");
//        else printf("＿\n");
//    }
    
    
    
    
    // SE
    if(_frameCnt % 18 == 0 && _state == WALK)
    {
        auto audio = SimpleAudioEngine::getInstance();
        
        char fileName[128];
        if(_playerType == Gentle)
        {
            sprintf(fileName, "sound/SE/walk_gentle%d.mp3", rand()%2);
        }
        else
        {
            sprintf(fileName, "sound/SE/walk_lady%d.mp3", rand()%2);
        }
        
        audio->playEffect(fileName, false, 1, 1, 100);
    }
    
    
    
    _frameCnt++;
    if(_frameCnt > 1000000) _frameCnt = 0;
}

void MyChar::move()
{
    if(_actorType == KOTEI || _isWarping) return;
    
    
    _hitX = false;
    _hitY = false;
    
    moveX();
    
    // 登っていたら
    if(_isClimbOK && _isClimbing)
    {
        // アクターに登っている状態であれば、そのアクターの上下方向の力を継ぐ
        if(_isActorClimb && _collisionActor->getPositionY() - _collisionActor->_prePos.y != 0)
        {
            Sand *sand = ((Stage*)(getParent()->getParent()))->_sand;
            
            bool inSand = _collisionActor->getPositionY() < sand->getPositionY();
            
            int offset = inSand ? 0 : 10;
            
            auto my =  abs(_collisionActor->getPositionY() - _collisionActor->_prePos.y);
            _move.y += (_collisionActor->_preMove.y+offset > 0) ? my:-my;
            cocos2d::log("%.2f,  %.2f", _collisionActor->_preMove.y, my);
        }
    }
    moveY();
    
    
    _preLanding = _isClimbing;
    
    _move.x = 0;
    _move.y = 0;
}

void MyChar::walk(Actor::Direction dir)
{
    if(_state == WALK && _dir == dir) return;
    
    if(!_isWalkOK) return;
    
    auto anim = AnimationManager::createRepeat(animationNames[ANIM_WALK], -1);
    anim->setTag(1);
    stopActionByTag(1);
    runAction(anim);
    
    _state = WALK;
    _dir = dir;
    
    if(dir == RIGHT)
    {
        _move.x = _moveSpeed;
    }
    
    if(dir == LEFT)
    {
        _move.x = -_moveSpeed;
    }
    
    GameLayer *gameLayer = (GameLayer *)getParent()->getParent()->getParent();
    if(gameLayer->_stageState == GameLayer::StageState::LANDSCAPE_RIGHT)
    {
//        _move.x *= -1;
    }
}

void MyChar::stop()
{
    auto anim = AnimationManager::createRepeat(animationNames[ANIM_STAND], -1);
    anim->setTag(1);
    stopActionByTag(1);
    runAction(anim);
    
    _move.x = 0;
    _state = MyChar::State::STOP;
}

void MyChar::moveX()
{
    if(_move.x == 0) return;
    
    // x
    
    if(_isClimbOK && _isClimbing)
    {
        _move.y += abs(_move.x);
    }
    
//    int seido = abs(_move.x);
    
    // 移動
    setPositionX(getPositionX() + _move.x);
    
    
    for(auto collisionActor : isCollisionActor())
    {
        // 動くブロックにあたっていたら一緒に動くものとして登録
        if(collisionActor->_actorType == NOMAL_BLOCK)
        {
            _isActorClimb = true;
            _collisionActor = collisionActor;
            
            log("うぇい");
        }
    }
    
    // めり込んでいたらもどす
    if(isCollisionActor().size() > 0|| _mapManager->isCollision(getPosition(), getBoundingBox()))
    {
        setPositionX(getPositionX() - _move.x);
        _hitX = true;
    }
    
    if(isHamidashi())
    {
        setPositionX(getPositionX() - _move.x);
    }
    
    
    // 戻された場合（なにかにあたっていた場合）
    if(_isClimbOK)
    {
        if(_hitX)
        {
            _isClimbing = true;
        }
        else
        {
            _isClimbing = false;
            _isActorClimb = false;
        }
    }
    
    
    if(!_isClimbing && !_isLanding && !_preLanding)
    {
        Stage *stage = (Stage*)getParent()->getParent();
        if(stage->_sand->_isWater)
        {
            if(_inSandPercent < 0.3)
                setPositionX(getPositionX() - _move.x); // 空中でうごけない
        }
        else
        {
            setPositionX(getPositionX() - _move.x); // 空中でうごけない？
        }
    }
    
    
    
}

void MyChar::action()
{
//    if(_isClimbing) return;
    
    _isAction = true;
    
    ActorManager *actorManager = (ActorManager *)getParent();
    auto children = actorManager->getChildren();
    
    for(auto child : children)
    {
        Actor *actor = (Actor*)child;
        if(actor->_actorType == SWITCH && getPosition().distance(actor->getPosition()) < getBoundingBox().size.width*2)
        {
            GimmickSwitch *sw = (GimmickSwitch*)actor;
            sw->switchOn();
        }
    }
}




void MyChar::star()
{
    _isStar = true;
    
    auto act = Sequence::create(DelayTime::create(10),
                                CallFunc::create([=]()
                                                 {
                                                     _isStar = false;
                                                 }),
                                NULL);
    act->setTag(1023);
    stopActionByTag(act->getTag());
    runAction(act);
}




