//
//  Sand.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/14.
//
//

#include "Sand.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "Stage.h"
#include "GameUI.h"
#include "GameScene.h"

Sand *Sand::create()
{
    Sand *pRet = new Sand();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool Sand::init()
{
    if ( !Layer::init() ) return false;
    
    _state = UP;
    _isMove = true;
    
    setPosition(cocos2d::Vec2(0, _mapManager->_stageSize.height * 0.5));
    
    
    _sand = cocos2d::Sprite::create();
    _sand->setAnchorPoint(cocos2d::Vec2(0, 1));
    _sand->setPosition(cocos2d::Vec2(0,
                            0));
    
    _sand->setOpacity(255 * 0.6);
    _sand->setColor(cocos2d::Color3B(178, 149, 119));
    resizeSand();
    addChild(_sand);
    
//    cocos2d::BlendFunc blend;
//    blend.src = GL_ONE_MINUS_DST_COLOR;
//    blend.dst = GL_ONE;
//    _sand->setBlendFunc(blend);
    
    if(_mapManager->_mapInfo->getProperties().size() > 0)
    {
        _sandSpeed = _mapManager->_mapInfo->getProperties().at("sandSpeed").asFloat();
    }
    else
    {
        _sandSpeed = 0.7;
    }

    _isUmaru = true;
    
    _isWater = false;
    _furyoku = 0;
    
    scheduleUpdate();
    return true;
}

void Sand::update(float delta)
{
    GameScene *gameScene;
    auto parent = getParent();
    while (parent->getName()!="gameScene") parent = parent->getParent();
    gameScene = (GameScene*)parent;
    
    GameUI *gameUI = gameScene->_gameUI;
    auto alert = gameUI->_alertSp;
    
    float sandRate = getPositionY() / _mapManager->_stageSize.height;
    if((sandRate < 0.2 && _state == DOWN) || (sandRate > 0.8 && _state == UP)) alert->setVisible(true);
    else alert->setVisible(false);
    
    GameLayer *gameLayer = gameScene->_gameLayer;
    Stage *stage = (Stage*)getParent();
    
    auto posx = 0;
    parent = getParent();
    while (parent) {
        posx += parent->getPositionX();
        parent = parent->getParent();
    }
    
    if(!gameLayer->_isTurning)
        setPositionX(-posx * gameLayer->getScale() - _sand->getBoundingBox().size.width/2 * gameLayer->getScale());
    
    if(_isMove)
    {
        // 昼か夜か見る
        
        if(stage->_timeState == Stage::HIRU) _state = DOWN;
        else _state = UP;
        
        // 砂の上下
        if(_state == UP && getPositionY() < _mapManager->_stageSize.height)
        {
            setPositionY(getPositionY() + _sandSpeed);
        }
        if(_state == DOWN && getPositionY() > 0)
        {
            setPositionY(getPositionY() - _sandSpeed);
        }
    }
    
    resizeSand();
    
    
    // アクターの上下
    auto children = _actorManager->getChildren();
    for(auto node : children)
    {
        Actor *actor = (Actor*)node;
        
        // 砂の位置よりキャラが下にいる場合、キャラを浮上
        if(getPositionY() > node->getPositionY())
        {
            
            if(actor->_actorType == Actor::KOTEI) continue;
            
            if((actor->_actorType != Actor::PLAYER) || (actor->_actorType == Actor::PLAYER && ((MyChar*)actor)->_inSandPercent > 0.3 && _isWater))
            {
            
                float moveY = getPositionY() - actor->getPositionY();
                moveY /= 6;
                actor->_move.y += moveY;
            
            }
             
        }
        
        if(actor->_actorType == Actor::PLAYER)
        {
            MyChar *myChar = (MyChar*)actor;
            
            float def = getPositionY() - actor->getPositionY();
            
            float movePercent = def / actor->getBoundingBox().size.height;
            if(movePercent > 1) movePercent = 1;
            if(movePercent < 0) movePercent = 0;
            
            
            myChar->_inSandPercent = movePercent;
        }
    }
    
    // エフェクト（上から下に）
    if(_state == DOWN)
    {
        Sprite *sandEff = Sprite::create("gameScene/sand.png");
//        sandEff->setPosition(Vec2(_sand->getBoundingBox().size.width/2 + (rand()%100)/100.0f * 50 * (rand()%2==0?1:-1), 0));
        sandEff->setPosition(Vec2((-gameLayer->getPositionX() + designResolutionSize.width/2)/gameLayer->getScale(), designResolutionSize.height/2 / gameLayer->getScale()));
        sandEff->setColor(_sand->getColor());
        sandEff->setOpacity(_sand->getOpacity()/5);
        sandEff->setScale(1.5);
        gameLayer->addChild(sandEff);
        float duration = 2.0f;
        auto act = Sequence::create(Spawn::create(EaseIn::create(MoveBy::create(duration, Vec2(0,
                                                                                                -1000 / gameLayer->getScale())), 2),
                                                  EaseIn::create(FadeOut::create(duration/2),2),
                                                  RotateBy::create(duration, 180 * (rand()%4 -2)),
                                                  NULL),
                                    CallFunc::create([=]()
                                                     {
                                                         sandEff->removeFromParent();
                                                     }),
                                    NULL);
        sandEff->runAction(act);
    }
}

void Sand::resizeSand()
{
    _sand->setTextureRect(cocos2d::Rect(0, 0, _mapManager->_stageSize.width*2,
                               getPositionY()));
    
    
    if(_sand->getTextureRect().size.height > _mapManager->_stageSize.height)
    {
        _sand->setTextureRect(cocos2d::Rect(0, 0, _mapManager->_stageSize.width*2,
                                   _mapManager->_stageSize.height));
    }
    
    if(_sand->getTextureRect().size.height <= 0)
    {
        _sand->setTextureRect(cocos2d::Rect(0,0,_mapManager->_stageSize.width*2,0));
        _sand->setPositionY(0);
    }
}


void Sand::changeToWater(bool isChange)
{
    if(isChange && !_isWater)
    {
        auto duration = 1.0f;
        _sand->runAction(Sequence::create(TintTo::create(duration, 10, 120, 150),
                                   CallFunc::create([=]()
                                                    {
                                                        _isWater = true;
                                                    }),
                                   NULL));
    }
    else if(!isChange && _isWater)
    {
        auto duration = 1.0f;
        _sand->runAction(Sequence::create(TintTo::create(duration, 178, 149, 119),
                                   CallFunc::create([=]()
                                                    {
                                                        _isWater = false;
                                                    }),
                                   NULL));
    }
}










