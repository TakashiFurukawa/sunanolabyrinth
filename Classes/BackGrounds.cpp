//
//  BackGrounds.cpp
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/05/21.
//
//

#include "BackGrounds.h"
#include "MultiResolution.h"

bool BackGrounds::init()
{
    if(!Node::init())
    {
        return false;
    }
    
    backSp = Sprite::create("TitleScene/back.png");
    backSp->setPosition(designResolutionSize/2);
    
    setBrokenBack();
    
    isStartBroken = false;
    
    
    this->scheduleUpdate();
    return true;
}

void BackGrounds::update(float delta)
{
}

void BackGrounds::startBrokenBack()
{
    if(!isStartBroken)
    {
        float callTiming = 3/60;
        
        this->schedule(schedule_selector(BackGrounds::brokenBack), callTiming);
        
        isStartBroken = true;
    }
}



void BackGrounds::setBrokenBack()
{
    
    backTex = TextureCache::sharedTextureCache()->addImage("TitleScene/back.png");
    backTex->retain();
    
    int cutWidth = 64;
    int cutHight = 64;

    int yMax = (backTex->getPixelsHigh())/cutWidth;
    int xMax = (backTex->getPixelsWide()/cutHight);
    
    for(int y = 0; y < yMax;y++)
    {
        for(int x = 0;x < xMax; x++)
        {
            brokenBgSps.pushBack(Sprite::createWithTexture(backTex, cocos2d::Rect(x*cutWidth, y*cutHight, cutWidth, cutHight)));
        }
    }
    
    int value = 0;
    for(int y = 0; y < yMax; y++)
    {
        for(int x = 0; x < xMax; x++)
        {
            brokenBgSps.at(value)->setAnchorPoint(Vec2(0,1));
            brokenBgSps.at(value)->setPosition(x*cutWidth, designResolutionSize.height - y*cutHight);
            this->addChild(brokenBgSps.at(value),1000000-value);
            isBrokenFlg.push_back(false);
            value++;
        }
    }
}

void BackGrounds::brokenBack(float delta)
{
    
    
    for(int i = 0; i < 10; i++)
    {
        int roopCnt = 0;

        int randMax = backTex->getPixelsWide() / 64;
        int centerNum = brokenBgSps.size()/2;
    
    //真ん中から上下
    
    while (true)// 真ん中から↓
    {
        if(roopCnt >= brokenBgSps.size())
        {
            break;
        }
        if(randMax >= brokenBgSps.size())
        {
            randMax = brokenBgSps.size();
        }
        
        int vecNum =centerNum - rand()%randMax;
        if(vecNum < 0)
        {
            vecNum = 0;
        }
        if(isBrokenFlg[vecNum] == false)
        {
            
            auto moveScOut =EaseIn::create(MoveTo::create(0.6f, Vec2(designResolutionSize.width/2,designResolutionSize.height/2)),2);
            auto fadeOut = FadeOut::create(0.5f);
            auto moveAndFade = Spawn::create(moveScOut,fadeOut, NULL);
            
            this->runAction(TargetedAction::create(brokenBgSps.at(vecNum),moveAndFade));
            isBrokenFlg[vecNum] = true;
            break;
        }
        
        randMax++;
        roopCnt++;
    }
    randMax = backTex->getPixelsWide() / 64;
    roopCnt = 0;
    
    while (true)
    {
        if(roopCnt >= brokenBgSps.size())
        {
            break;
        }
        if(randMax >= brokenBgSps.size())
        {
            randMax = brokenBgSps.size();
        }
        
        int vecNum =centerNum + rand()%randMax;
        if(vecNum >= isBrokenFlg.size())
        {
            vecNum = isBrokenFlg.size()-1;
        }
        
        if(isBrokenFlg[vecNum] == false)
        {
            
            auto moveScOut =EaseIn::create(MoveTo::create(0.6f, Vec2(designResolutionSize.width/2,designResolutionSize.height/2)),2);
            auto fadeOut = FadeOut::create(0.5f);
            auto moveAndFade = Spawn::create(moveScOut,fadeOut, NULL);
            
            this->runAction(TargetedAction::create(brokenBgSps.at(vecNum),moveAndFade));
            isBrokenFlg[vecNum] = true;
            break;
        }
        
        randMax++;
        roopCnt++;
    }
}
    //全部終わったかチェック
    for(auto isBro : isBrokenFlg)
    {
        if(isBro == false)
        {
            return;
        }
    }
    brokenBgSps.clear();
    
    this->unschedule(schedule_selector(BackGrounds::brokenBack));
    
}


void BackGrounds::shake()
{
    Vec2 pos = this->getPosition();
    
    auto seq = Sequence::create(MoveTo::create(0.05f,Vec2(pos.x,pos.y+10.0f)),
                                DelayTime::create(0.05f),
                                MoveTo::create(0.05f,Vec2(pos.x,pos.y-10.0f)),
                                NULL);
    
    auto rep = Repeat::create(seq, 2);
    
    this->runAction(Sequence::createWithTwoActions(rep, MoveTo::create(0.2f, pos)));
}