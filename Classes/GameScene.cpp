//
//  GameScene.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/14.
//
//

#include "GameScene.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "CustomFollow.h"
#include "TestScene.h"
#include "GoalBlock.h"
#include "ResultLayer.h"
#include "PauseLayer.h"
#include "StartLayer.h"

GameScene *GameScene::create(int stageNo)
{
    GameScene *pRet = new GameScene();
    if(pRet && pRet->init(stageNo))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool GameScene::init(int stageNo)
{
    if ( !Scene::init() ) return false;
    
    auto touchListener = cocos2d::EventListenerTouchOneByOne::create();
    touchListener->onTouchBegan = CC_CALLBACK_2(GameScene::onTouchBegan, this);
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);
    
    
    
    _stageNo = stageNo;
    
    setName("gameScene");
    
    cocos2d::Sprite *back = cocos2d::Sprite::create("gameScene/back/back.jpg");
    back->setAnchorPoint(cocos2d::Vec2(0.5, 0.5));
    back->setPosition(cocos2d::Vec2(designResolutionSize.width/2, designResolutionSize.height*-0.2));
    back->setScale(2);
    back->setOpacity(0);
    addChild(back);
    
    cocos2d::Sprite *whiteBack = cocos2d::Sprite::create();
    whiteBack->setTextureRect(cocos2d::Rect(0, 0, designResolutionSize.width, designResolutionSize.height));
    whiteBack->setColor(cocos2d::Color3B::WHITE);
    whiteBack->setAnchorPoint(cocos2d::Vec2::ZERO);
    addChild(whiteBack);
    
    cocos2d::Sprite *backGround = cocos2d::Sprite::create("gameScene/back/back.jpg");
    backGround->setTextureRect(cocos2d::Rect(0, 0, designResolutionSize.width, designResolutionSize.height));
    backGround->setColor(cocos2d::Color3B::WHITE);
    backGround->setAnchorPoint(cocos2d::Vec2::ZERO);
    backGround->setOpacity(255 * 0.3);
    addChild(backGround);
    
    
    _gameLayer = new GameLayer();
    _gameLayer->setScale(0.4);
    addChild(_gameLayer);
    _gameLayer->init(stageNo);
    _gameLayer->autorelease();

    
    _waku = Sprite::create("gameScene/back/waku.png");
    _waku->setAnchorPoint(Vec2(0.5,0.5));
    _waku->setPosition(visibleCenter);
    addChild(_waku);
    
    
    _gameUI = new GameUI();
    _gameUI->_gameLayer = _gameLayer;
    _gameUI->init();
    _gameUI->autorelease();
    addChild(_gameUI);
    
    _isGameOver = false;
    
    _gameLayer->_topStage->_back = back;
    
    
    auto start = StartLayer::create();
    start->_gameScene = this;
    start->setName("start");
    addChild(start);
    
    
    
    
    _time = 0;
    _heartNum = 0;
    
    _isStart = false;
    _isPaused = false;
    
    _infoPause = false;
    
    scheduleUpdate();
    return true;
}

bool GameScene::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    if(_infoPause)
    {
        _infoPause = false;
        resumeAllChild(_gameLayer);
        resumeAllChild(_gameUI);
    }
    
    return true;
}

void GameScene::update(float delta)
{
    if(!_isStart && !_isPaused)
    {
        _isPaused = true;
        
        GameScene::pauseAllChild(_gameLayer->_topStage->_sand);
        GameScene::pauseAllChild(_gameLayer->_bottomStage->_sand);
        GameScene::pauseAllChild(_gameLayer);
        GameScene::pauseAllChild(_gameUI);
    }
    
    // ゲームオーバー監視
    if(!_isGameOver &&
       ( _gameLayer->_topStage->_sand->getPositionY() <= 0 ||
        _gameLayer->_bottomStage->_sand->getPositionY() <= 0))
    {
        gameOver(false);
    }
    
    
    // ゲームクリア監視
    if(!_isGameOver &&
       ( ((GoalBlock*)_gameLayer->_topStage->_goalNode)->_isGoal&&
        ((GoalBlock*)_gameLayer->_bottomStage->_goalNode)->_isGoal))
    {
        gameOver(true);
        SimpleAudioEngine::getInstance()->playEffect("sound/SE/lock.mp3");
    }
    
    
    if(!_isGameOver) _time ++;
    
}

void GameScene::gameOver(bool isClear)
{
    log("gameover");
    
    _gameUI->gameOver();
    _gameLayer->gameOver();
    
    pauseAllChild(_gameLayer);
    pauseAllChild(_gameUI);
    
    _isGameOver = true;
    
//    Director::getInstance()->replaceScene(TransitionFade::create(0.1, TestScene::create()));
    
//    
//    auto result = ResultLayer::create(_time/60, _heartNum, isClear);
    auto result = new ResultLayer();
    addChild(result);
    result->init(_time/60, _heartNum, isClear);
    result->autorelease();
    
}

void GameScene::start()
{
    StartLayer *start = (StartLayer*)getChildByName("start");
    start->start();
    
    SimpleAudioEngine::getInstance()->playEffect("sound/SE/shakin.mp3");
}

void GameScene::pauseAllChild(cocos2d::Node *node)
{
    node->pause();
    for(auto child : node->getChildren())
    {
        if(child)
        {
            pauseAllChild(child);
        }
    }
}

void GameScene::resumeAllChild(cocos2d::Node *node)
{
    node->resume();
    for(auto child : node->getChildren())
    {
        if(child)
        {
            resumeAllChild(child);
        }
    }
}











