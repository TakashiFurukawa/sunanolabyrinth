//
//  SwitchBlock.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/23.
//
//

#ifndef __SunanoLabyrinth__SwitchBlock__
#define __SunanoLabyrinth__SwitchBlock__

#include "cocos2d.h"
#include "Actor.h"

using namespace cocos2d;
using namespace CocosDenshion;

class SwitchBlock : public Actor
{
public:
    static SwitchBlock *create();
    virtual bool init();
    void update(float delta);
    
    virtual void switchOn();
};

#endif /* defined(__SunanoLabyrinth__SwitchBlock__) */
