//
//  GameUI.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/20.
//
//

#ifndef __SunanoLabyrinth__GameUI__
#define __SunanoLabyrinth__GameUI__

#include "cocos2d.h"
#include "GameLayer.h"
#include "UISprite.h"
#include "UISpriteButton.h"
#include "SandStopButton.h"

using namespace cocos2d;
using namespace CocosDenshion;

class GameUI : public cocos2d::Layer
{
public:
    
    static GameUI *create();
    virtual bool init();
    void update(float delta);
    
    GameLayer::StageState _state;
    
    GameLayer *_gameLayer;
    bool _isGameOver;
    
    UISpriteButton *_playerChangeButton;
    UISpriteButton *_rightButton;
    UISpriteButton *_leftButton;
    UISpriteButton *_sunadokeiHanten;
    SandStopButton *_sandStopButton;
    
    Sprite *_alertSp;
    
    void turn();
    void gameOver();
    
    void turnObject(UISprite *button);
};

#endif /* defined(__SunanoLabyrinth__GameUI__) */
