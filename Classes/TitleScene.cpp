//
//  TitleScene.cpp
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/05/26.
//
//

#include "TitleScene.h"
#include "SelectScene.h"

bool TitleScene::init()
{
    if(!Scene::init())
    {
        return false;
    }
    sceneChange = false;
    touchSc =false;
    
    layer = TitleLayer::create();
    this->addChild(layer);
    
    
    
    
    // タッチイベントとかの登録
    auto touchListener = cocos2d::EventListenerTouchOneByOne::create();
    touchListener->onTouchBegan = CC_CALLBACK_2(TitleScene::onTouchBegan, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(TitleScene::onTouchEnded, this);
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);
    
    this->scheduleUpdate();
    return true;
}

void TitleScene::update(float delta)
{
    if(!sceneChange)
    {
        if(layer->isSceneChangeOK)
        {
            auto scene = SelectScene::create();
            Director::getInstance()->replaceScene(TransitionFade::create(1.0f, scene, Color3B::WHITE));
            
            sceneChange = true;
        }
    }
}

bool TitleScene::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
    
    return true;
}

void TitleScene::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
    if(!touchSc)
    {
//        auto scene = SelectScene::create();
//        Director::getInstance()->replaceScene(TransitionFade::create(0.5f, scene));
        
        layer->tChar->changeAct();
        layer->changeLogo->changeAction();
        touchSc = true;
    }
    
}








