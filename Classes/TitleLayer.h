//
//  TitleLayer.h
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/02.
//
//

#ifndef __SunanoLabyrinth__TitleLayer__
#define __SunanoLabyrinth__TitleLayer__

#include "cocos2d.h"
USING_NS_CC;
#include "TitleBG.h"
#include "TitleLogo.h"
#include "GoSelectLogo.h"
#include "TitleChar.h"

class TitleLayer : public Layer
{
public:
    CREATE_FUNC(TitleLayer);
    bool init();
    void update(float delta);
    
    TitleBG *bg;
    TitleLogo *titleLogo;
    GoSelectLogo *changeLogo;
    TitleChar *tChar;
    
    bool isSceneChangeOK;

};


#endif /* defined(__SunanoLabyrinth__TitleLayer__) */
