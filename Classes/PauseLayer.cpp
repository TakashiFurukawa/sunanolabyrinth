//
//  PauseLayer.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/06/08.
//
//

#include "PauseLayer.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "TestScene.h"
#include "SelectScene.h"

PauseLayer *PauseLayer::create()
{
    PauseLayer *pRet = new PauseLayer();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool PauseLayer::init()
{
    if ( !Layer::init() ) return false;
    
    
    
    Sprite *fade = Sprite::create();
    fade->setTextureRect(cocos2d::Rect(0,0,designResolutionSize.width,designResolutionSize.height));
    fade->setAnchorPoint(Vec2::ZERO);
    fade->setColor(Color3B::BLACK);
    fade->setOpacity(255 * 0);
    fade->setName("fade");
    addChild(fade);
    
    
    auto resumePushed = [=]()
    {
        GameScene::resumeAllChild(_gameScene);
        this->appear(false);
    };
    
    _resume = UISpriteButton::create("gameScene/ui/startbutton.png", resumePushed);
    addChild(_resume);
    
    auto resetPushed = [=]()
    {
        auto gameScene = GameScene::create(_gameScene->_stageNo);
        Director::getInstance()->replaceScene(TransitionFade::create(0.5, gameScene));
        gameScene->start();
    };
    
    _reset = UISpriteButton::create("gameScene/ui/returnbutton.png", resetPushed);
    addChild(_reset);
    
    auto titlePushed = [=]()
    {
        Director::getInstance()->replaceScene(TransitionFade::create(0.5, SelectScene::create()));
    };
    
    _title = UISpriteButton::create("gameScene/ui/menubutton.png", titlePushed);
    addChild(_title);
    
    int size = getChildren().size() -1;
    int pos = -size/2;
    int count = 0;
    for(auto child : getChildren())
    {
        if(child->getName()=="fade") continue;
        
        ((UISpriteButton*)child)->_isPushOnly = true;
        child->setPositionX(designResolutionSize.width/2 + child->getBoundingBox().size.width * 5 * ((float)pos/size));
        child->setPositionY(designResolutionSize.height/2 + designResolutionSize.height);
        count ++;
        pos++;
    }
    
    
    scheduleUpdate();
    return true;
}

void PauseLayer::update(float delta)
{
    
}


void PauseLayer::appear(bool isAppear)
{
    
    int size = getChildren().size() -1;
    int pos = -size/2;
    int count = 0;
    for(auto child : getChildren())
    {
        if(child->getName()=="fade")
        {
            float fadeRate = (isAppear?0.7:0);
            child->runAction(Sequence::create(FadeTo::create(0.2, 255*fadeRate), NULL));
            continue;
        }
        child->runAction(Sequence::create(DelayTime::create(0.1 * count),
                                          EaseBackInOut::create(MoveBy::create(0.2, Vec2(0, -designResolutionSize.height * (isAppear?1:-1)))),
                                          CallFunc::create([=]()
                                                           {
                                                               if(!isAppear && count+1 == size)
                                                               {
                                                                   this->removeFromParent();
                                                               }
                                                           }),
                                          NULL));
        
        count ++;
        pos++;
    }
    
    
}


