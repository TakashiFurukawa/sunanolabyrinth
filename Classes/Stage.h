//
//  Stage.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/19.
//
//

#ifndef __SunanoLabyrinth__Stage__
#define __SunanoLabyrinth__Stage__

#include "cocos2d.h"
#include "MapManager.h"
#include "ActorManager.h"
#include "MyChar.h"
#include "Sand.h"


using namespace CocosDenshion;

class Stage : public cocos2d::Node
{
public:
    enum TimeState
    {
        HIRU,
        YORU,
    };
    
    virtual bool init(MyChar::PlayerType playerType, int stageNo);
    void update(float delta);
    
    TimeState _timeState;
    
    MapManager *_mapManager;
    ActorManager *_actorManager;
    MyChar *_myChar;
    Sand *_sand;
    cocos2d::Sprite *_back;
    
    MyChar::PlayerType _playerType;
    
    cocos2d::Sprite *_stageBack;
    
    Sprite *_black;
    Node *_goalNode;
    
    
    void follow(bool isFollow);
    void followChange(Node *followNode);
};

#endif /* defined(__SunanoLabyrinth__Stage__) */
