//
//  PrologueScene.h
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/05/19.
//
//

#ifndef SunanoLabyrinth_PrologueScene_h
#define SunanoLabyrinth_PrologueScene_h

#define scene_tag_1 10001
#define scene_tag_2 10002
#define scene_tag_3 10003
#define scene_tag_4 10004

#include "cocos2d.h"
USING_NS_CC;

#include "Slideshow.h"

class PrologueScene : public Scene
{
public:
    CREATE_FUNC(PrologueScene);
    bool init();
    
    Slideshow *slide;
    void changeScreen();
    
    void changeScene();
    
private:
    //シーンチェンジ用
    bool isSceneChanged;
    int nowScene;
    
    // タッチ
    bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    
};

#endif
