//
//  UISprite.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/20.
//
//

#ifndef __SunanoLabyrinth__UISprite__
#define __SunanoLabyrinth__UISprite__

#include "cocos2d.h"

using namespace CocosDenshion;
using namespace std;

class UISprite : public cocos2d::Sprite
{
public:
    static UISprite *create(string fileName, cocos2d::Vec2 origin);
    virtual bool init(string fileName, cocos2d::Vec2 origin);
    void update(float delta);
    
    cocos2d::Vec2 _origin;
};

#endif /* defined(__SunanoLabyrinth__UISprite__) */
