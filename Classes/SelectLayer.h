//
//  SelectLayer.h
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/05/19.
//
//

#ifndef SunanoLabyrinth_SelectLayer_h
#define SunanoLabyrinth_SelectLayer_h

#define KEY_FIRST_PLAY "ISFIRSTPLAY"

#include "cocos2d.h"
#include "BackGrounds.h"

#include "SelectDoorLogos.h"

USING_NS_CC;

class SelectLayer : public Layer
{
public:
    CREATE_FUNC(SelectLayer);
    bool init();
    BackGrounds *back;
   
    SelectDoorLogos *doorN;
    
    bool getSceneChangeFlg();
    void setSceneChangeFlg(bool isOk);
    bool getAllFinish();
    void removeAndGameStart();
private:
    bool isOkSceneChange;
    bool isFirstPlay;
    
};
#endif
