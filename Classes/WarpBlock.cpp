//
//  WarpBlock.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/05/13.
//
//

#include "WarpBlock.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "GameLayer.h"

WarpBlock *WarpBlock::create()
{
    WarpBlock *pRet = new WarpBlock();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool WarpBlock::init()
{
    if ( !Sprite::init() ) return false;
    
    setName("warp");
    
    setTexture("gameScene/block/warp.png");
    setAnchorPoint(Vec2(0.5, 0));
    
    _isWarp = false;
    
    scheduleUpdate();
    return true;
}

void WarpBlock::update(float delta)
{
    GameLayer *gameLayer;
    auto parent = getParent();
    while (parent->getName() != "gameLayer") parent = parent->getParent();
    gameLayer = (GameLayer*)parent;
    
    
    if(gameLayer->_activeStage == _stage)
    {
        if(gameLayer->_activeStage->_myChar->getBoundingBox().intersectsRect(getBoundingBox()))
        {
            warp();
            _warp->_isWarp = true;
            _isWarp = true;
        }
        else
        {
            _isWarp = false;
        }
    }
}

void WarpBlock::warp()
{
    auto myChar = _stage->_myChar;
    
    
    if(_warp->_isWarp || _isWarp || myChar->_isWarping) return;
    
    
    
    
    myChar->_isWarping = true;
    SimpleAudioEngine::getInstance()->playEffect("sound/SE/warp.mp3");
    
    myChar->runAction(Sequence::create(ScaleTo::create(0.1, 0, 2),
                                       CallFunc::create([=]()
                                                        {
                                                            myChar->setPosition(_warp->getPosition());
                                                            myChar->_inSandPercent = 0;
                                                        }),
                                       ScaleTo::create(0.1, 1, 1),
                                       CallFunc::create([=]()
                                                        {
                                                            myChar->_isWarping = false;
                                                        }),
                                       nil));
    
}

























