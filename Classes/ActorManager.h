//
//  ActorManager.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/16.
//
//

#ifndef __SunanoLabyrinth__ActorManager__
#define __SunanoLabyrinth__ActorManager__

#include "cocos2d.h"
#include "Actor.h"
#include "MapManager.h"

using namespace cocos2d;
using namespace CocosDenshion;
using namespace std;

class ActorManager : public cocos2d::Node
{
public:
    static ActorManager *create();
    virtual bool init();
    void update(float delta);
    
    MapManager *_mapManager;
    
    void createActors();
    void initActor(Actor *actor, cocos2d::Vec2 index);
    
    Color3B _gimmickColor[9];
    int _colorIndex;
};

#endif /* defined(__SunanoLabyrinth__ActorManager__) */
