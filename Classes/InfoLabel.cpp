//
//  InfoLabel.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/06/04.
//
//

#include "InfoLabel.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"

InfoLabel *InfoLabel::create(string text)
{
    InfoLabel *pRet = new InfoLabel();
    if(pRet && pRet->init(text))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool InfoLabel::init(string text)
{
    if ( !Sprite::init() ) return false;
    
    setTextureRect(cocos2d::Rect(0, 0, 950, 500));
    setColor(Color3B(246, 243, 237));
    
    auto size = getBoundingBox().size;
    
    label = nicos2d::Label::create("", "fonts/bokutachi.otf", 80);
    label->setColor(Color3B(108, 90, 85));
    label->setAnchorPoint(Vec2(0, 1));
    label->setHorizontalAlignment(cocos2d::TextHAlignment::LEFT);
    label->setPosition(Vec2(size.width * 0.05, size.height * 0.9));
    addChild(label);
    
    setOpacity(255 * 0.7);
    
    setScale(0);
    
    string kaigyouari = "";
    
    int count = 0;
    for(char c : text)
    {
        kaigyouari.push_back(c);
        
        if(c == '\n') count = 0;
        
        count ++;
        if(count == 10*3)
        {
            kaigyouari.push_back('\n');
            count = 0;
        }
    }
    
    _text = kaigyouari;
    scheduleUpdate();
    return true;
}

void InfoLabel::update(float delta)
{
    
}

void InfoLabel::appear(bool isAppear)
{
    if(_isAppeared == isAppear) return;
    
    
    
    
    
    
    _isAppeared = isAppear;
    
    stopAllActions();
    
    auto size = getParent()->getBoundingBox().size;
    setPositionX(size.width/2);
    
    float duration = 0.3;
    
    if(isAppear)
    {
        // SE
        SimpleAudioEngine::getInstance()->playEffect("sound/SE/info.mp3");
        
        label->setString("");
        runAction(Sequence::create(Spawn::create(EaseBackOut::create(ScaleTo::create(duration, 1)),
                                                 EaseOut::create(MoveTo::create(duration, Vec2(size.width/2, 300)), 2),
                                                 NULL),
                                   CallFunc::create([=]()
                                                    {
                                                        label->startRunString(_text, 0.06);
                                                    }),
                                   NULL));
    }
    else
    {
        runAction(Sequence::create(Spawn::create(EaseBackIn::create(ScaleTo::create(duration, 0)),
                                                 EaseOut::create(MoveTo::create(duration, Vec2(size.width/2, 0)), 2),
                                                 NULL),
                                   NULL));
    }
}

















