//
//  InfoBlock.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/06/04.
//
//

#include "InfoBlock.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"

InfoBlock *InfoBlock::create(string text)
{
    InfoBlock *pRet = new InfoBlock();
    if(pRet && pRet->init(text))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool InfoBlock::init(string text)
{
    if ( !Sprite::init() ) return false;
    
    initWithFile("gameScene/block/info.png");
    
    _infoLabel = InfoLabel::create(text);
    addChild(_infoLabel);
    
    scheduleUpdate();
    return true;
}

void InfoBlock::update(float delta)
{
    hitCheck();
}

void InfoBlock::hitCheck()
{
    if(_myChar->getBoundingBox().intersectsRect(getBoundingBox()))
    {
        _infoLabel->appear(true);
    }
    else
    {
        _infoLabel->appear(false);
    }
}