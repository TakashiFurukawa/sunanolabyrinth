//
//  TitleLayer.cpp
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/02.
//
//

#include "TitleLayer.h"

bool TitleLayer::init()
{
    if(!Layer::init())
    {
        return false;
    }
    
    bg = TitleBG::create();
    this->addChild(bg,10);
    
    titleLogo = TitleLogo::create();
    this->addChild(titleLogo,1000);
    
    changeLogo = GoSelectLogo::create();
    this->addChild(changeLogo,50);
    
    tChar = TitleChar::create();
    this->addChild(tChar,60);
    
    
    isSceneChangeOK = false;
    
    this->scheduleUpdate();
    return true;
}

void TitleLayer::update(float delta)
{
    if(!isSceneChangeOK)
    {
        if(tChar->isFadeOutFinish)
        {
            isSceneChangeOK = true;
        }
    }
    
}
