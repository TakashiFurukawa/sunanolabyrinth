//
//  ProloScene4.h
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/12.
//
//

#ifndef __SunanoLabyrinth__ProloScene4__
#define __SunanoLabyrinth__ProloScene4__

#include "cocos2d.h"
#include "Slideshow.h"
USING_NS_CC;

class ProloScene4 : public Slideshow
{
public:
    CREATE_FUNC(ProloScene4);
    bool init();
    void update(float delta);
  
    enum SP_TAG
    {
        BACK,
        LADY,
        GENTLE,
        SANDGLASS
        
    };
};


#endif /* defined(__SunanoLabyrinth__ProloScene4__) */
