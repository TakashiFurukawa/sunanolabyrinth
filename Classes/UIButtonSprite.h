//
//  UIButtonSprite.h
//  Beans
//
//  Created by Furukawa on 2015/02/17.
//
//

#ifndef __Beans__UIButtonSprite__
#define __Beans__UIButtonSprite__

#include "cocos2d.h"
#include "GameLayer.h"
#include "UISprite.h"

using namespace CocosDenshion;
using namespace std;

class UIButtonSprite : public UISprite
{
public:
    static UIButtonSprite *create();
    static UIButtonSprite *create(string fileName, cocos2d::Vec2 origin, const function<void ()> &func);
    static UIButtonSprite *create(string fileName, cocos2d::Vec2 origin, const function<void ()> &push, const function<void ()> &release);
    bool init();
    bool init(string fileName, cocos2d::Vec2 origin, const function<void ()> &func);
    bool init(string fileName, cocos2d::Vec2 origin, const function<void ()> &push, const function<void ()> &release);
    void update(float delta);
    
    bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);
    void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event);
    void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event);
    
    bool isEnable;
    float pushedScale;
    function<void ()> pushFunc;
    function<void ()> releaseFunc;
    
    bool isPushed;
    
    virtual void pushed();
    virtual void released();
    
    static bool isEnableAllButtons;
    static bool isPushedFrame;
    
    GameLayer *_gameLayer;
};

#endif /* defined(__Beans__UIButtonSprite__) */
