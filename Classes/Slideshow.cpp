//
//  SlideshowSprite.cpp
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/05/19.
//
//

#include "Slideshow.h"

bool Slideshow::init()
{
    if(!Layer::create())
    {
        return false;
    }
    
    isViewSlideNum = 0;
    
    this->scheduleUpdate();
    return true;
}

void Slideshow::update(float delta){}
void Slideshow::death()
{
    this->runAction(Sequence::create(
                                     CallFunc::create([=](){this->clearSlide();}),
                                     DelayTime::create(4.0),
                                     CallFunc::create([=](){this->removeFromParent();}),
                                     NULL));

    
}

void Slideshow::appearSprite(const std::string &filename, cocos2d::Vec2 appearPos, int tag)
{
    Sprite *sp = Sprite::create(filename);
    sp->setOpacity(0);
    sp->setPosition(appearPos);
    sp->setTag(tag);
    this->addChild(sp,0);
    
    sp->runAction(FadeIn::create(1.0f));
}

void Slideshow::setSprite(const std::string &filename, cocos2d::Vec2 setPos, int tag)
{
    Sprite *sp = Sprite::create(filename);
    sp->setOpacity(0);
    sp->setPosition(setPos);
    sp->setTag(tag);
    this->addChild(sp,0);
}

void Slideshow::appearToSetSprite()
{
    auto children = this->getChildren();
    
    for(auto child : children)
    {
        if(child->getOpacity() == 0)
        {
            child->runAction(FadeIn::create(1.0f));
    
        }

    }
}

void Slideshow::removeSprite(int tag)
{
    ((Sprite*)this->getChildByTag(tag))->runAction(Sequence::create(
                                                                    FadeOut::create(1.0f),
                                                                    DelayTime::create(1.0f),
                                                                    CallFunc::create([=](){((Sprite*)this->getChildByTag(tag))->removeFromParent();}),
                                                                    NULL)
                                                   );
}

void Slideshow::clearSlide()
{
    auto children = this->getChildren();
    
    for(auto child : children)
    {
        child->runAction(Sequence::create(
                                          FadeOut::create(1.0f),
                                          DelayTime::create(2.0f),
                                          CallFunc::create([=](){this->unscheduleAllSelectors(); child->removeFromParent();}),
                                          NULL)
                         );
    }
}

void Slideshow::changeSlide(const std::string &filename, cocos2d::Vec2 appearPos, int tag)
{
    this->runAction(Sequence::create(
                                     CallFunc::create([=](){clearSlide();}),
                                     CallFunc::create([=](){appearSprite(filename,appearPos,tag);})
                                     , NULL));
}

void Slideshow::playAnimation(cocos2d::FiniteTimeAction *action, int spTag)
{
    this->runAction(TargetedAction::create(((Sprite*)this->getChildByTag(spTag)), action));
}





