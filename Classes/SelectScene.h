//
//  SelectScene.h
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/05/19.
//
//

#ifndef SunanoLabyrinth_SelectScene_h
#define SunanoLabyrinth_SelectScene_h

#include "cocos2d.h"
#include "SelectLayer.h"
USING_NS_CC;

#include "GameScene.h"

class SelectScene : public Scene
{
public:
    CREATE_FUNC(SelectScene);
    bool init();
    void update(float delta);
    SelectLayer *selectLayer;
    
    void gameStart();
    GameScene *gameScene;
    void sceneChange(int selectNum);
private:
    bool isChange;
    
    
};

#endif
