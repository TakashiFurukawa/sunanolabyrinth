//
//  ProloScene2.cpp
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/12.
//
//

#include "ProloScene2.h"
#include "MultiResolution.h"

bool ProloScene2::init()
{
    if(!Slideshow::init())
    {
        return false;
    }
    
    Vec2 offset = Vec2::ZERO;
    
    // 背景
    this->appearSprite("PrologueScene/2/back.png", designResolutionSize/2, BACK);
    // たってもの
    this->appearSprite("PrologueScene/2/build.png", designResolutionSize/2, BUILD);
    //占い師
    this->appearSprite("PrologueScene/2/uranai.png", Vec2(designResolutionSize.width/2 + offset.x,designResolutionSize.height/2 + offset.y), URANAI);
    //きゃら
    offset.y = -designResolutionSize.height/2;
    this->appearSprite("PrologueScene/2/char.png", Vec2(designResolutionSize.width/2 + offset.x,designResolutionSize.height/2 + offset.y), CHAR);
    
    ((Sprite*)this->getChildByTag(URANAI))->setColor(Color3B(0,0,0));
    
    this->runAction(Sequence::create(
                                     DelayTime::create(0.5f),
                                     CallFunc::create([=](){this->schedule(schedule_selector(ProloScene2::uranaiAppear));}),
                                     DelayTime::create(2.0f),
                                     CallFunc::create([=](){this->playAnimation(MoveTo::create(1.5f, designResolutionSize/2), CHAR);}),
                                     NULL));
    
    this->scheduleUpdate();
    return true;
}

void ProloScene2::update(float delta)
{
}

void ProloScene2::uranaiAppear(float delta)
{
    Color3B color = ((Sprite*)this->getChildByTag(URANAI))->getColor();

    color.r += 1;
    color.g += 1;
    color.b += 1;
    ((Sprite*)this->getChildByTag(URANAI))->setColor(color);
    
    if(color.r == 255 &&color.g == 255 && color.b == 255 )
    {
        this->unschedule(schedule_selector(ProloScene2::uranaiAppear));
    }
    
    
}