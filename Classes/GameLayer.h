//
//  GameLayer.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/14.
//
//

#ifndef __SunanoLabyrinth__GameLayer__
#define __SunanoLabyrinth__GameLayer__

#include "cocos2d.h"
#include "MapManager.h"
#include "ActorManager.h"
#include "MyChar.h"
#include "Sand.h"
#include "Stage.h"

using namespace cocos2d;
using namespace CocosDenshion;

class GameLayer : public cocos2d::Layer
{
public:
    
    enum StageState
    {
        LANDSCAPE_LEFT,
        LANDSCAPE_RIGHT,
    };
    
    
    static GameLayer *create(int stageNo);
    virtual bool init(int stageNo);
    void onExit();
    void update(float delta);
    
    Stage *_topStage;
    Stage *_bottomStage;
    bool _isGameOver;
    
    StageState _stageState;
    
    cocos2d::Vec2 _touchPoint;
    
    cocos2d::Vec2 _followScreenPos[2];
    
    MyChar::PlayerType _activePlayer;
    MyChar *_controlMyChar;
    Stage *_activeStage;
    Stage *_unActiveStage;
    
    
    bool _isTouchEnable;
    bool _isEnablePlayerChange;
    
    float _turnDuration;
    bool _isTurning;
    
    void turn(StageState state);
    void turn();
    void turnBack(float duration);
    void followChange(cocos2d::Node *followNode);
    void playerChange();
    void gameOver();
    
    void onAcceleration(cocos2d::Acceleration *acc, cocos2d::Event *unused_event);
    bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
};

#endif /* defined(__SunanoLabyrinth__GameLayer__) */
