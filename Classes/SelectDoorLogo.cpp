//
//  StageDoorLogo.cpp
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/14.
//
//

#include "SelectDoorLogo.h"
#include "AnimationManager.h"

#include "MultiResolution.h"

bool SelectDoorLogo::init()
{
    if(!Node::init())
    {
        return false;
    }
    
    
    isSelect = false;
    isDeath = false;
    
    doorTex = TextureCache::sharedTextureCache()->addImage("SelectScene/door_00.png");
    
    
    doorSp = Sprite::create("SelectScene/door_00.png");
    doorSp->setScale(0.9f);
    this->addChild(doorSp);
    
    AnimationManager::addAnimationCache("SelectScene/door_0", "doorOpen", 7, 0.1f);
    AnimationManager::addAnimationCacheReverse("SelectScene/door_0", "doorClose", 7, 0.1f);
    
    
    
    this->setPosition(designResolutionSize/2);
    
    
    
    this->scheduleUpdate();
    return true;
}

void SelectDoorLogo::update(float delta)
{
    if(!isDeath)
    {
        if(!isSelect)
        {
            doorSp->setScale(0.9f);
        }
        else
        {
            doorSp->setScale(1.0f);
        }
    }
}

void SelectDoorLogo::startOpenAnim()
{
    isDeath = true;
    
    doorSp->runAction(Sequence::create(
                                       MoveTo::create(0.5f, designResolutionSize/2),
                                       AnimationManager::createRepeat("doorOpen", 1),
                                       CallFunc::create([=](){doorSp->setTexture("SelectScene/door_06.png");}),
                                       NULL
                                       )
                      );
}

void SelectDoorLogo::startCloseAnim()
{
    doorSp->runAction(Sequence::create(
                                       AnimationManager::createRepeat("doorClose", 1),
                                       CallFunc::create([=](){doorSp->setTexture("SelectScene/door_00.png");}),
                                       NULL
                                       )
                      );
}

void SelectDoorLogo::suikomare()
{
    
    isDeath = true;
    
    this->runAction(Sequence::create(
                                     CallFunc::create([=](){shake();}),
                                     DelayTime::create(1.0f),
                                     CallFunc::create([=](){doorSp->runAction(Spawn::create(
                                                                                            ScaleTo::create(0.5f, 0.0f),
                                                                                            MoveTo::create(0.5f, designResolutionSize/2),
                                                                                            RotateTo::create(0.5f, 180.0f),
                                                                                            FadeOut::create(0.5f),
                                                                                            NULL)
                                                                              );}),
                                     NULL)
                    );
    
}

void SelectDoorLogo::shake()
{
    Vec2 pos = this->getPosition();
    
    auto seq = Sequence::create(MoveTo::create(0.025f,Vec2(pos.x+2.5f,pos.y+2.5f)),
                                DelayTime::create(0.025f),
                                MoveTo::create(0.025f,Vec2(pos.x-2.5f,pos.y-2.5f)),
                                NULL);
    
    auto rep = Repeat::create(seq, 20);
    this->runAction(rep);
}

void SelectDoorLogo::death()
{
    float
    cutH = 33,
    cutW = 12.15f;
    
    for(int x = 0; x < doorTex->getPixelsWide() / cutW; x++)
    {
        for(int y = 0; y < doorTex->getPixelsHigh() / cutH; y++)
        {
            Sprite *sp = Sprite::createWithTexture(doorTex, cocos2d::Rect(x*cutW, y*cutH, cutW, cutH));
            
            Vec2 pos;
            pos.x = -doorTex->getPixelsWide()/2 + x * cutW;
            pos.y = doorTex->getPixelsHigh()/2 - y * cutH;
            
            pos += doorSp->getPosition();
            Vec2 offset = Vec2(5.5,-16);
            pos += offset;
            
            sp->setPosition(pos);
            this->addChild(sp);
            doorSps.pushBack(sp);
            isBroken.push_back(false);
        }
    }
   
    isDeath = true;
    doorSp->removeFromParent();
    
    this->schedule(schedule_selector(SelectDoorLogo::fadeOunt));
    
}

void SelectDoorLogo::fadeOunt(float delta)
{
    // 左から右に消える
    
    int roopCnt = 0;
    float cutW = 33;
    
    
    int lineMax = doorTex->getPixelsWide() / cutW;
    int brokenCnt = 0;
    
    while (true)
    {
        if(roopCnt >= doorSps.size())
        {
            break;
        }
        if(brokenCnt == lineMax)
        {
            break;
        }
        if(isBroken[roopCnt] == false)
        {
            this->runAction(TargetedAction::create(doorSps.at(roopCnt),ScaleTo::create(1.0f, 0.0f)));
            isBroken[roopCnt] = true;
            brokenCnt++;
        }
        roopCnt++;
    }
    
    //全部終わったかチェック
    for(auto isBro : isBroken)
    {
        if(isBro == false)
        {
            return;
        }
    }
    doorSps.clear();
    this->unschedule(schedule_selector(SelectDoorLogo::fadeOunt));
    
}