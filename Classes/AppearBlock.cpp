//
//  AppearBlock.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/05/22.
//
//

#include "AppearBlock.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "GameLayer.h"

AppearBlock *AppearBlock::create()
{
    AppearBlock *pRet = new AppearBlock();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool AppearBlock::init()
{
    if ( !SwitchBlock::init() ) return false;
    
    setTexture("gameScene/block/block.png");
    setOpacity(255 * 0.4);
    
    
    scheduleUpdate();
    return true;
}

void AppearBlock::update(float delta)
{
    
}

void AppearBlock::switchOn()
{
    GameLayer *gameLayer = (GameLayer*)getParent()->getParent()->getParent();
    
    float duration = 0.5;
    runAction(cocos2d::Sequence::create(DelayTime::create(1),
                                        Spawn::create(EaseInOut::create(Repeat::create(Sequence::create(ScaleTo::create(duration/2, 0.7),
                                                                                         ScaleTo::create(duration/2, 1.0),
                                                                                                        NULL),3),2),
                                                      FadeIn::create(duration),
                                                      NULL),
                                        DelayTime::create(1),
                                        CallFunc::create([=]()
                                                         {
                                                             ((Stage*)(getParent()->getParent()))->followChange(gameLayer->_unActiveStage->_myChar);
                                                             
                                                             gameLayer->_topStage->_sand->_isMove = true;
                                                             gameLayer->_bottomStage->_sand->_isMove = true;
                                                             gameLayer->_isEnablePlayerChange = true;
                                                             
                                                             gameLayer->_controlMyChar->_isWalkOK = true;
                                                         }),
                                        NULL));
}










