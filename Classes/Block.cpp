//
//  Block.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/16.
//
//

#include "Block.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "ActorManager.h"

Block *Block::create()
{
    Block *pRet = new Block();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool Block::init()
{
    if ( !cocos2d::Sprite::init() ) return false;
    
    setTexture("gameScene/block/block2.png");
    
    setAnchorPoint(cocos2d::Vec2(0.5, 0));
    
    _actorType = NOMAL_BLOCK;
    
    scheduleUpdate();
    return true;
}

void Block::update(float delta)
{
    Actor::update(delta);
    
}






