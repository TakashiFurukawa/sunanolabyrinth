//
//  TitleScene.h
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/05/26.
//
//

#ifndef __SunanoLabyrinth__TitleScene__
#define __SunanoLabyrinth__TitleScene__

#include "cocos2d.h"
USING_NS_CC;
#include "TitleLayer.h"



class TitleScene : public Scene
{
public:
    CREATE_FUNC(TitleScene);
    bool init();
    void update(float delta);
    
    TitleLayer *layer;
    
    // タッチ
    bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    
private:
    bool sceneChange;
    bool touchSc;
    
    
};

#endif /* defined(__SunanoLabyrinth__TitleScene__) */
