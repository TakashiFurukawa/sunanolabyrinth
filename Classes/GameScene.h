//
//  GameScene.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/14.
//
//

#ifndef __SunanoLabyrinth__GameScene__
#define __SunanoLabyrinth__GameScene__

#include "cocos2d.h"
#include "GameLayer.h"
#include "GameUI.h"

using namespace CocosDenshion;

class GameScene : public cocos2d::Scene
{
public:
    static GameScene *create(int stageNo);
    virtual bool init(int stageNo);
    void update(float delta);
    
    GameLayer *_gameLayer;
    GameUI *_gameUI;
    
    Sprite *_waku;
    int _stageNo;
    
    bool _infoPause;
    
    
    int _heartNum;
    int _time;
    
    bool _isPaused;
    bool _isStart;
    
    bool _isGameOver;
    
    void gameOver(bool isClear);
    
    static void pauseAllChild(Node *node);
    static void resumeAllChild(Node *node);
    
    bool onTouchBegan(Touch *touch, Event *event);
    
    void start();
};

#endif /* defined(__SunanoLabyrinth__GameScene__) */
