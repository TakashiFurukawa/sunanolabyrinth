//
//  HiruYoruBlock.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/19.
//
//

#ifndef __SunanoLabyrinth__HiruYoruBlock__
#define __SunanoLabyrinth__HiruYoruBlock__

#include "cocos2d.h"
#include "Actor.h"
#include "GameLayer.h"


using namespace CocosDenshion;

class HiruYoruBlock : public Actor
{
public:
    static HiruYoruBlock *create(ActorType actorType);
    virtual bool init(ActorType actorType);
    void update(float delta);
    
    GameLayer *_gameLayer;
    
    bool isCollide();
    
    void move() override;
};

#endif /* defined(__SunanoLabyrinth__HiruYoruBlock__) */
