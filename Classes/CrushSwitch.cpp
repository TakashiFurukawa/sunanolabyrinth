//
//  CrushSwitch.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/30.
//
//

#include "CrushSwitch.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "GameLayer.h"
#include "SwitchBlock.h"
#include "CrushBlock.h"

CrushSwitch *CrushSwitch::create()
{
    CrushSwitch *pRet = new CrushSwitch();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool CrushSwitch::init()
{
    if ( !GimmickSwitch::init() ) return false;
    
    setColor(Color3B(100, 100, 200));
    
    scheduleUpdate();
    return true;
}

void CrushSwitch::update(float delta)
{
    GimmickSwitch::update(delta);
}

void CrushSwitch::switchOn()
{
    if(_isOn) return;
    
    GimmickSwitch::switchOn();
    
    
    _isOn = true;
    
    GameLayer *gameLayer = (GameLayer*)getParent()->getParent()->getParent();
    gameLayer->_isEnablePlayerChange = false;
    
    gameLayer->_controlMyChar->_isWalkOK = false;
    
    
    // 片側を暗くする
    ((Stage*)_myChar->getParent()->getParent())->_black->runAction(FadeTo::create(0.2, 255 * 0.5));
    
    auto func = [=]()
    {
        
        // 砂の上下一時停止
        gameLayer->_topStage->_sand->_isMove = false;
        gameLayer->_bottomStage->_sand->_isMove = false;
        
        
        // スイッチの発動とカメラ切り替え
        auto actorManager = gameLayer->_unActiveStage->_actorManager;
        auto children = actorManager->getChildren();
        cocos2d::Node *followNode;
        for(auto child : children)
        {
            CrushBlock *block = (CrushBlock*)child;
            
            if(block->_actorType == SWITCH_BLOCK && block->_crushNo == _crushNo)
            {
                block->switchOn();
                followNode = block;
            }
        }
        if(followNode)
            gameLayer->_unActiveStage->followChange(followNode);
        
        
        stick->runAction(Sequence::create(FadeOut::create(0.5), NULL));
    };
    
    
    stick->runAction(cocos2d::Sequence::create(cocos2d::EaseBackIn::create(cocos2d::RotateTo::create(0.25, -stick->getRotation())),
                                               cocos2d::DelayTime::create(0.5),
                                               cocos2d::CallFunc::create(func),
                                               cocos2d::DelayTime::create(2.0),
                                               cocos2d::CallFunc::create([=]()
                                                                         {
                                                                             // 片側を暗くする
                                                                             ((Stage*)_myChar->getParent()->getParent())->_black->runAction(FadeTo::create(0.2, 0));
                                                                         }),
                                               NULL));
    
}