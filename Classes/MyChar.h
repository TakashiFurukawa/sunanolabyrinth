//
//  MyChar.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/14.
//
//

#ifndef __SunanoLabyrinth__MyChar__
#define __SunanoLabyrinth__MyChar__

#include "cocos2d.h"
#include "MapManager.h"
#include "Actor.h"

using namespace cocos2d;
using namespace CocosDenshion;
using namespace std;

class MyChar : public Actor
{
public:
    
    enum PlayerType
    {
        Gentle,
        Lady,
    };
    
    enum Animation
    {
        ANIM_STAND,
        ANIM_WALK,
        ANIMATION_MAX,
    };
    
    virtual bool init(PlayerType playerType);
    void update(float delta);
    
    string animationNames[ANIMATION_MAX];
    
    PlayerType _playerType;
    float _moveSpeed;
    float _inSandPercent;
    bool _isAction;
    bool _isWalkOK;
    bool _preLanding;
    bool _isStar;
    
    Actor *_collisionActor;
    
    void walk(Direction dir);
    void stop();
    void action();
    
    void move() override;
    void moveX() override;
//    void moveY() override;
    
    void star();
    
    bool _isWarping;
    
private:
    
    bool _isActorClimb;
    
    unsigned int _frameCnt;
    
};

#endif /* defined(__SunanoLabyrinth__MyChar__) */
