//
//  AnimationManager.h
//  TextScript1
//
//  Created by Furukawa on 2014/11/20.
//
//

#ifndef __TextScript1__AnimationManager__
#define __TextScript1__AnimationManager__

#include <stdio.h>
#include "cocos2d.h"

using namespace std;

class AnimationManager
{
public:
    
    static void addAnimationCache(string path, string cacheName, int frameNum, float delay);
    static void addAnimationCacheReverse(string path, string cacheName, int frameNum, float delay);
    static cocos2d::Repeat* createRepeat(string cacheName, unsigned int time);
    static cocos2d::RepeatForever* createRepeatForever(string cacheName);
    
    
private:
    
};


#endif /* defined(__TextScript1__AnimationManager__) */
