//
//  GoalBlock.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/05/11.
//
//

#ifndef __SunanoLabyrinth__GoalBlock__
#define __SunanoLabyrinth__GoalBlock__

#include "cocos2d.h"
#include "GameLayer.h"
#include "Stage.h"

using namespace cocos2d;
using namespace CocosDenshion;

class GoalBlock : public Sprite
{
public:
    static GoalBlock *create();
    virtual bool init();
    void update(float delta);
    
    GameLayer *_gameLayer;
    Stage *_stage;
    
    bool _isGoal;
};

#endif /* defined(__SunanoLabyrinth__GoalBlock__) */
