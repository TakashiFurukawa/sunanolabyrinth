//
//  SandStopButton.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/05/22.
//
//

#include "SandStopButton.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"

SandStopButton *SandStopButton::create()
{
    SandStopButton *pRet = new SandStopButton();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool SandStopButton::init()
{
    if ( !UIButtonSprite::init() ) return false;
    
    setTexture("gameScene/ui/sunadokei.png");
    
    pushFunc = [=]()
    {
        _sand[0]->_isMove = false;
        _sand[1]->_isMove = false;
        
        _isStop = true;
        runAction(Sequence::create(DelayTime::create(10),
                                   CallFunc::create([=]()
                                                    {
                                                        _sand[0]->_isMove = true;
                                                        _sand[1]->_isMove = true;
                                                    }),
                                   NULL));
        
    };
    
    _countLabel = Label::createWithTTF("", "fonts/bokutachi.otf", 100);
    _countLabel->setPosition(Vec2(getBoundingBox().size.width/2, -getBoundingBox().size.height * 1.2));
    _countLabel->setColor(Color3B(108, 90, 85));
    addChild(_countLabel);
    
    _timeCnt = 0;
    _time = 10;
    
    _isStop = false;
    
    scheduleUpdate();
    return true;
}

void SandStopButton::update(float delta)
{
    if(_isStop)
    {
        _timeCnt++;
        
        if(_timeCnt % 60 == 0)
        {
            _time--;
            char timeLabelStr[128];
            sprintf(timeLabelStr, "%d", _time);
            _countLabel->setString(timeLabelStr);
            
            if(_time == -1)
            {
                _countLabel->setString("");
                _isStop = false;
                _timeCnt = 0;
                _time = 10;
            }
        }
    }
}










