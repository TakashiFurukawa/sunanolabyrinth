//
//  TestScene.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/28.
//
//

#include "TestScene.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "UIButtonSprite.h"
#include "GameScene.h"
#include "UIButtonSprite.h"

TestScene *TestScene::create()
{
    TestScene *pRet = new TestScene();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool TestScene::init()
{
    if ( !Scene::init() ) return false;
    
    // タッチイベントとかの登録
    auto touchListener = cocos2d::EventListenerTouchOneByOne::create();
    touchListener->onTouchBegan = CC_CALLBACK_2(TestScene::onTouchBegan, this);
    touchListener->onTouchMoved = CC_CALLBACK_2(TestScene::onTouchMoved, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(TestScene::onTouchEnded, this);
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);
    
    
    Layer *layer = Layer::create();
    addChild(layer);
    
    Label *label1 = Label::createWithTTF("ステージをえらんでね", "fonts/bokutachi.otf", 70);
    label1->setPosition(cocos2d::Vec2(designResolutionSize.width/2,
                                      designResolutionSize.height * 0.7));
    label1->setColor(Color3B::WHITE);
    addChild(label1);
    
//    button1 = Sprite::create("gameScene/ui/sunadokei.png");
//    button1->setPosition(cocos2d::Vec2(designResolutionSize.width/3,
//                                       designResolutionSize.height * 0.35));
//    addChild(button1);
//    
//    
//    button2 = Sprite::create("gameScene/ui/sunadokei.png");
//    button2->setPosition(cocos2d::Vec2(designResolutionSize.width/3*2,
//                                       designResolutionSize.height * 0.35));
//    addChild(button2);
    
    
    int line = 0;
    int posX = 0;
    int width = 4;
    Vec2 origin = Vec2(designResolutionSize.width * 0.3, designResolutionSize.height * 0.5);
    for(int i=0; i<=12; i++)
    {
        UIButtonSprite *button = UIButtonSprite::create("gameScene/block/0.png",
                                                        Vec2(origin.x + 200 * posX ,
                                                             origin.y - 200 * line),
                                                        [=]()
                                                        {
                                                            if(changeScene) return;
                                                            
                                                            changeScene = true;
                                                            
                                                            int count = i;
                                                            
                                                            Director::getInstance()->replaceScene(GameScene::create(i));
                                                        });
        addChild(button);
        
        char stageNoStr [64];
        sprintf(stageNoStr, "%d", i);
        Label *stageNo = Label::createWithTTF(stageNoStr, "fonts/bokutachi.otf", 50);
        stageNo->setColor(Color3B(108, 90, 85));
        stageNo->setPosition(Vec2(button->getBoundingBox().size.width/2,
                                  button->getBoundingBox().size.height/2));
        button->addChild(stageNo);
        
        posX++;
        if(posX == width)
        {
            posX = 0;
            line++;
        }
    }
    
    changeScene = false;
    
    scheduleUpdate();
    return true;
}

void TestScene::update(float delta)
{
    
}

bool TestScene::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
//    if(button1->getBoundingBox().containsPoint(touch->getLocation()))
//    {
//        if(changeScene) return true;
//        
//        changeScene = true;
//        
//        Director::getInstance()->replaceScene(GameScene::create(-2));
//    }
//    
//    if(button2->getBoundingBox().containsPoint(touch->getLocation()))
//    {
//        if(changeScene) return true;
//        
//        changeScene = true;
//        
//        Director::getInstance()->replaceScene(GameScene::create(2));
//    }
    
    return true;
}

void TestScene::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
    
}

void TestScene::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
    
}