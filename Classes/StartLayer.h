//
//  StartLayer.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/06/09.
//
//

#ifndef __SunanoLabyrinth__StartLayer__
#define __SunanoLabyrinth__StartLayer__

#include "cocos2d.h"
#include "GameScene.h"

using namespace cocos2d;
using namespace CocosDenshion;
using namespace std;

class StartLayer : public Layer
{
public:
    static StartLayer *create();
    virtual bool init();
    void update(float delta);
    
    GameScene *_gameScene;
    
    Label *startLabel[5];
    Sprite *fade;
    
    void start();
    
    bool onTouchBegan(Touch *touch, Event *event);
    void onTouchEnded(Touch *touch, Event *event);
    void onTouchMoved(Touch *touch, Event *event);
};

#endif /* defined(__SunanoLabyrinth__StartLayer__) */
