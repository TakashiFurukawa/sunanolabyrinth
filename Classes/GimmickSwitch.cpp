//
//  GimmickSwitch.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/17.
//
//

#include "GimmickSwitch.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "ActorManager.h"
#include "SwitchBlock.h"
#include "GameLayer.h"

GimmickSwitch *GimmickSwitch::create()
{
    GimmickSwitch *pRet = new GimmickSwitch();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool GimmickSwitch::init()
{
    if ( !Actor::init() ) return false;
    
    
    setTexture("gameScene/gimmick/switch.png");
    
    
    stick = Sprite::create("gameScene/gimmick/bou.png");
    stick->setAnchorPoint(Vec2(0.5, 0));
    stick->setPosition(Vec2(getBoundingBox().size.width/2, getBoundingBox().size.height * 0.65));
    addChild(stick, -1);
    stick->setRotation(45);
    
    setCascadeColorEnabled(true);
    
    
    _actorType = SWITCH;
    
    _isOn = false;
    _frameCnt = 0;
    
    scheduleUpdate();
    return true;
}

void GimmickSwitch::update(float delta)
{
    if(_isOn) return;
    
    if(_frameCnt % 15 == 0)
    {
        if(getPosition().distance(_myChar->getPosition()) < _myChar->getBoundingBox().size.width*2)
        {
            Sprite *eff = Sprite::createWithTexture(getTexture());
            eff->setAnchorPoint(Vec2(0.5, 0));
            eff->setPosition(Vec2(getBoundingBox().size.width/2, 0));
            eff->setColor(_col);
            addChild(eff);
            
            float duration = 0.5;
            eff->runAction(Sequence::create(Spawn::create(FadeOut::create(duration),
                                                          ScaleTo::create(duration, 2.0),
                                                          NULL),
                                            CallFunc::create([=]()
                                                             {
                                                                 eff->removeFromParent();
                                                             }),
                                            NULL));
        }
    }
    
    _frameCnt ++;
    if(_frameCnt > 1000000) _frameCnt = 0;
}

void GimmickSwitch::moveX()
{
    switchOn();
    
    _move.x = 0;
}

void GimmickSwitch::switchOn()
{
    if(_isOn) return;
    
    // SE
    SimpleAudioEngine::getInstance()->playEffect("sound/SE/switch01.mp3");
    
    runAction(Sequence::create(DelayTime::create(0.5),
                               CallFunc::create([=]()
                                                {
                                                    SimpleAudioEngine::getInstance()->playEffect("sound/SE/chin.mp3");
                                                }),
                               nil));
}








