//
//  ProloScene3.h
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/12.
//
//

#ifndef __SunanoLabyrinth__ProloScene3__
#define __SunanoLabyrinth__ProloScene3__

#include "cocos2d.h"
#include "Slideshow.h"
USING_NS_CC;

class ProloScene3 : public Slideshow
{
public:
    CREATE_FUNC(ProloScene3);
    bool init();
    void update(float delta);
    enum SP_TAG
    {
        BACK,
        MOB,
        LADY
    };

};


#endif /* defined(__SunanoLabyrinth__ProloScene3__) */
