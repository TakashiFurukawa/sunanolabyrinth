//
//  MapManager.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/14.
//
//

#ifndef __SunanoLabyrinth__MapManager__
#define __SunanoLabyrinth__MapManager__

#include "cocos2d.h"

using namespace CocosDenshion;
using namespace std;

class MapManager : public cocos2d::Layer
{
public:
    static MapManager *create(int stageNo);
    virtual bool init(int stageNo);
    void update(float delta);
    
    cocos2d::TMXTiledMap *_map;
    cocos2d::TMXMapInfo *_mapInfo;
    
    cocos2d::Size _stageSize;
    
    cocos2d::Vec2 getTileIndex(cocos2d::Vec2 pos);
    cocos2d::Rect getTileBoundingBox(cocos2d::Vec2 index);
    
    bool isCollision(cocos2d::Vec2 pos, cocos2d::Rect rect);
    
    static int stageSubNo;
    
    int _stageNo;
};

#endif /* defined(__SunanoLabyrinth__MapManager__) */
