//
//  MapManager.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/14.
//
//

#include "MapManager.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"

int MapManager::stageSubNo = 0;

MapManager *MapManager::create(int stageNo)
{
    MapManager *pRet = new MapManager();
    if(pRet && pRet->init(stageNo))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool MapManager::init(int stageNo)
{
    if ( !Layer::init() ) return false;
    
    _stageNo = stageNo;
    
    char fileName[128];
    sprintf(fileName, "map/%d-%d.tmx", stageNo, stageSubNo);
    
    _map = cocos2d::TMXTiledMap::create(fileName);
    addChild(_map);
    
    _mapInfo = cocos2d::TMXMapInfo::create(fileName);
    
    cocos2d::Size mapSize = _map->getMapSize();
    cocos2d::Size tileSize = _map->getTileSize() * _map->getScale();
    
    _stageSize = cocos2d::Size(mapSize.width * tileSize.width,
                     mapSize.height * tileSize.height);
    
    
    stageSubNo ++;
    
    if(stageSubNo == 2) stageSubNo = 0;
    
    auto children = _map->getChildren();
    for(auto child : children)
    {
        if(child == _map->getLayer("terrain")) continue;
        child->setVisible(false);
    }
    
    scheduleUpdate();
    return true;
}

void MapManager::update(float delta)
{
    
}




// 指定の場所にあるタイルのマップ座標を返す
cocos2d::Vec2 MapManager::getTileIndex(cocos2d::Vec2 pos)
{
    cocos2d::Vec2 localPos = pos - _map->getPosition();		// マップ上のローカル座標に変換
    cocos2d::Vec2 index;
    
    // 座標を算出(Y軸がTiledとcocos2dでは反対向きなので補正する)
    index.x =								 floor(localPos.x / (_map->getScaleX() * _map->getTileSize().width));
    index.y = _map->getMapSize().height - 1 - floor(localPos.y / (_map->getScaleY() * _map->getTileSize().height));
    
    return index;
}

// 指定のタイルの(ワールド座標における)矩形を返す
cocos2d::Rect MapManager::getTileBoundingBox(cocos2d::Vec2 index)
{
    cocos2d::TMXLayer *terrain = _map->getLayer("terrain");
    cocos2d::Sprite *tile = NULL;
    if(index.x>=0 && index.x < _map->getMapSize().width &&
       index.y>=0 && index.y < _map->getMapSize().height)
    {
        tile = terrain->getTileAt(index);
    }
    cocos2d::Rect rect(0,0,0,0);
    
    if(tile)
    {
        rect.setRect(_map->getPositionX() + tile->getPositionX() * _map->getScaleX(),
                     _map->getPositionY() + tile->getPositionY() * _map->getScaleY(),
                     terrain->getMapTileSize().width * _map->getScaleX() * 0.9,
                     terrain->getMapTileSize().height * _map->getScaleY() * 0.9);
    }
    return rect;
}

bool MapManager::isCollision(cocos2d::Vec2 pos, cocos2d::Rect rect)
{
    cocos2d::Vec2 index = getTileIndex(pos);
    int search = 2;
    
    for(int y=-search; y<search; y++)
    {
        for(int x=-search; x<search; x++)
        {
            if(getTileBoundingBox(getTileIndex(pos) + cocos2d::Vec2(x, y)).intersectsRect(rect))
            {
                return true;
            }
        }
    }
    
    return false;
}














