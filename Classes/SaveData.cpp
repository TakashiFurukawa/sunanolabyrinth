//
//  SaveData.cpp
//  OreTobe
//
//  Created by Furukawa on 2015/01/26.
//
//

#include "SaveData.h"

const string SaveData::SAVE_FILE_NAME = "save.plist";
SaveData *SaveData::instance = NULL;

SaveData::SaveData()
{
    loadedValueMap = FileUtils::getInstance()->getValueMapFromFile(FileUtils::getInstance()->getWritablePath()+SAVE_FILE_NAME);
    log("%s", FileUtils::getInstance()->getWritablePath().c_str());
}

SaveData::~SaveData()
{
    flush();
    delete instance;
}

SaveData *SaveData::getInstance()
{
    if(instance == NULL)
    {
        instance = new SaveData();
    }
    
    return instance;
}

void SaveData::setValueForKey(string key, Value value)
{
    loadedValueMap[key] = value;
    flush();
}

Value SaveData::getValueForKey(string key, Value defaultValue)
{
    auto iterator = loadedValueMap.find(key);
    if(iterator != loadedValueMap.end())
    {
        return loadedValueMap[key];
    }
    else
    {
        setValueForKey(key, defaultValue);
        
        return defaultValue;
    }
}

void SaveData::flush()
{
    FileUtils::getInstance()->writeToFile(loadedValueMap, FileUtils::getInstance()->getWritablePath()+SAVE_FILE_NAME);
}


