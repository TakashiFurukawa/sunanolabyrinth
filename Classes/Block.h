//
//  Block.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/16.
//
//

#ifndef __SunanoLabyrinth__Block__
#define __SunanoLabyrinth__Block__

#include "cocos2d.h"
#include "MapManager.h"
#include "Actor.h"


using namespace CocosDenshion;

class Block : public Actor
{
public:
    
    static Block *create();
    virtual bool init();
    void update(float delta);
};

#endif /* defined(__SunanoLabyrinth__Block__) */
