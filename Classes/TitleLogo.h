//
//  TitleLogo.h
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/06/02.
//
//

#ifndef __SunanoLabyrinth__TitleLogo__
#define __SunanoLabyrinth__TitleLogo__

#include "cocos2d.h"
USING_NS_CC;

class TitleLogo : public Node
{
public:
    CREATE_FUNC(TitleLogo);
    bool init();
    void update(float delta);
    
    Sprite *logoSp;
    
    void changeAction();
    void echoEffect();
private:
    int echoEffTiming;
    int flamCnt;
    
};

#endif /* defined(__SunanoLabyrinth__TitleLogo__) */
