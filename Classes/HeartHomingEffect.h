//
//  HeartHomingEffect.h
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/21.
//
//

#ifndef __SunanoLabyrinth__HeartHomingEffect__
#define __SunanoLabyrinth__HeartHomingEffect__

#include "cocos2d.h"
#include "MyChar.h"


using namespace CocosDenshion;

class HeartHomingEffect : public cocos2d::Sprite
{
public:
    static HeartHomingEffect *create(MyChar *start, MyChar *goal);
    virtual bool init(MyChar *start, MyChar *goal);
    void update(float delta);
    
    MyChar *_start;
    MyChar *_goal;
    
    float _moveSpeed, _rotaSpeed;
    cocos2d::Vec2 _move;
    
    bool _isCollision;
};

#endif /* defined(__SunanoLabyrinth__HeartHomingEffect__) */
