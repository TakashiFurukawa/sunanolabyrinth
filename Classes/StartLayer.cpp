//
//  StartLayer.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/06/09.
//
//

#include "StartLayer.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"

StartLayer *StartLayer::create()
{
    StartLayer *pRet = new StartLayer();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool StartLayer::init()
{
    if ( !Layer::init() ) return false;
    
    // タッチ
    auto touchListener = EventListenerTouchOneByOne::create();
    touchListener->onTouchBegan = CC_CALLBACK_2(StartLayer::onTouchBegan, this);
    touchListener->onTouchMoved = CC_CALLBACK_2(StartLayer::onTouchMoved, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(StartLayer::onTouchEnded, this);
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);
    touchListener->setSwallowTouches(true);
    
    
    fade = Sprite::create();
    fade->setTextureRect(cocos2d::Rect(0,0,designResolutionSize.width,designResolutionSize.height));
    fade->setAnchorPoint(Vec2::ZERO);
    fade->setOpacity(255 * 0.7);
    fade->setColor(Color3B::BLACK);
    addChild(fade);
    
    string labelStr[] = {"S","T","A","R","T"};
    
    
    for(int i=0; i<5; i++)
    {
        float width = designResolutionSize.width * 0.07;
        
        startLabel[i] = Label::createWithTTF(labelStr[i], "fonts/font_1_kokumr_1.00_rls.ttf", 70);
        startLabel[i]->setPosition(Vec2(designResolutionSize.width * 1.5 + width * (i-2),
                                        designResolutionSize.height/2));
        addChild(startLabel[i]);
    }
    
    
    scheduleUpdate();
    return true;
}

void StartLayer::update(float delta)
{
    
}

bool StartLayer::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
    return false;
}

void StartLayer::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
    
}

void StartLayer::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
    
}

void StartLayer::start()
{
    for(int i=0; i<5; i++)
    {
        startLabel[i]->runAction(Sequence::create(DelayTime::create(0.10 * i),
                                                  EaseSineOut::create(MoveBy::create(0.2, Vec2(-designResolutionSize.width, 0))),
                                                  DelayTime::create(1.5),
                                                  EaseSineIn::create(MoveBy::create(0.2, Vec2(-designResolutionSize.width, 0))),
                                                  NULL));
    }
    
    
    fade->runAction(Sequence::create(DelayTime::create(2.0f),
                                     FadeOut::create(1.0f),
                                     CallFunc::create([=]()
                                                      {
                                                          GameScene::resumeAllChild(_gameScene->_gameLayer);
                                                          GameScene::resumeAllChild(_gameScene->_gameUI);
                                                          
                                                          // BGM
                                                          SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0.5);
                                                          SimpleAudioEngine::getInstance()->playBackgroundMusic("sound/BGM/game.mp3", true);
                                                          
                                                          this->removeFromParent();
                                                      }),
                                     NULL));
}











