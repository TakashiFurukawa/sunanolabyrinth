//
//  HiruYoruBlock.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/19.
//
//

#include "HiruYoruBlock.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"

HiruYoruBlock *HiruYoruBlock::create(ActorType actorType)
{
    HiruYoruBlock *pRet = new HiruYoruBlock();
    if(pRet && pRet->init(actorType))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool HiruYoruBlock::init(ActorType actorType)
{
    if ( !Actor::init() ) return false;
    
    _actorType = actorType;
    
    setTexture("gameScene/block/0.png");
    
    if(actorType == HIRU)
    {
        setTexture("gameScene/block/hiru.png");
    }
    if(actorType == YORU)
    {
        setTexture("gameScene/block/yoru.png");
    }
    
    scheduleUpdate();
    return true;
}

void HiruYoruBlock::update(float delta)
{
    if(isCollide())
    {
        setOpacity(255);
    }
    else
    {
        setOpacity(255 * 0.2);
    }
    
    if(!isCollide()) return;
    Actor::update(delta);
}

void HiruYoruBlock::move()
{
    
//    Actor::move();
}

bool HiruYoruBlock::isCollide()
{
//    bool isHiru = (_gameLayer->_stageState == GameLayer::StageState::LANDSCAPE_LEFT? true : false);
    
    bool isHiru = (_gameLayer->_activeStage->_timeState == Stage::HIRU ? true : false);
    
    if(isHiru && _actorType == ActorType::HIRU)
    {
        return true;
    }
    else if(!isHiru && _actorType == ActorType::YORU)
    {
        return true;
    }
    
    return false;
}








