//
//  SandSwitch.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/26.
//
//

#include "SandSwitch.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "GameLayer.h"

SandSwitch *SandSwitch::create()
{
    SandSwitch *pRet = new SandSwitch();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool SandSwitch::init()
{
    if ( !GimmickSwitch::init() ) return false;
    

    setColor(Color3B(255, 100, 100));
    
    
//    cocos2d::BlendFunc blend;
//    blend.src = GL_SRC_ALPHA;
//    blend.dst = GL_ONE;
//    setBlendFunc(blend);
//    stick->setBlendFunc(blend);
    
    scheduleUpdate();
    return true;
}

void SandSwitch::update(float delta)
{
    GimmickSwitch::update(delta);
}


void SandSwitch::switchOn()
{
    GimmickSwitch::switchOn();
    
    if(_isOn) return;
    
    _isOn = true;
    
    GameLayer *gameLayer;
    
    auto parent = getParent();
    while(parent->getName() != "gameLayer") parent = parent->getParent();
    
    gameLayer = (GameLayer*)parent;
    
    stick->runAction(cocos2d::Sequence::create(cocos2d::EaseBackIn::create(cocos2d::RotateTo::create(0.25, -stick->getRotation())),
                                               cocos2d::DelayTime::create(0.5),
                                               cocos2d::CallFunc::create([=]()
                                                                         {
                                                                             gameLayer->turn();
                                                                             _isOn = false;
                                                                         }),
                                               NULL));
}