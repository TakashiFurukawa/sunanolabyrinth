//
//  UIButtonSprite.cpp
//  Beans
//
//  Created by Furukawa on 2015/02/17.
//
//

#include "UIButtonSprite.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"

bool UIButtonSprite::isEnableAllButtons = true;
bool UIButtonSprite::isPushedFrame = false;

UIButtonSprite *UIButtonSprite::create()
{
    UIButtonSprite *pRet = new UIButtonSprite();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

UIButtonSprite *UIButtonSprite::create(string fileName, cocos2d::Vec2 origin, const function<void ()> &func)
{
    UIButtonSprite *pRet = new UIButtonSprite();
    if(pRet && pRet->init(fileName, origin, func))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

UIButtonSprite *UIButtonSprite::create(string fileName, cocos2d::Vec2 origin, const function<void ()> &push, const function<void ()> &release)
{
    UIButtonSprite *pRet = new UIButtonSprite();
    if(pRet && pRet->init(fileName, origin, push, release))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool UIButtonSprite::init()
{
    if ( !UISprite::init("gameScene/ui/gentle.png", cocos2d::Vec2(0, 0)) ) return false;
    
    auto touchListener = cocos2d::EventListenerTouchOneByOne::create();
    touchListener->onTouchBegan = CC_CALLBACK_2(UIButtonSprite::onTouchBegan, this);
    touchListener->onTouchMoved = CC_CALLBACK_2(UIButtonSprite::onTouchMoved, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(UIButtonSprite::onTouchEnded, this);
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);
    
    getTexture()->setAliasTexParameters();
    
    isEnable = true;
    isPushed = false;
    
    pushFunc = NULL;
    pushedScale = 0.75f;
    
    scheduleUpdate();
    return true;
}

bool UIButtonSprite::init(string fileName, cocos2d::Vec2 origin, const function<void ()> &func)
{
    if(!UIButtonSprite::init()) return false;
    
    setTexture(fileName);
    getTexture()->setAliasTexParameters();
    pushFunc = func;
    
    _origin = origin;
    
    setPosition(origin);
    
    return true;
}

bool UIButtonSprite::init(string fileName, cocos2d::Vec2 origin, const function<void ()> &push, const function<void ()> &releaseF)
{
    if(!UIButtonSprite::init()) return false;
    
    setTexture(fileName);
    getTexture()->setAliasTexParameters();
    pushFunc = push;
    releaseFunc = releaseF;
    
    _origin = origin;
    
    setPosition(origin);
    
    return true;
}

void UIButtonSprite::update(float delta)
{
    
}

bool UIButtonSprite::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    cocos2d::Vec2 touchPos = touch->getLocation();
    
    if(getBoundingBox().containsPoint(touchPos))
    {
        if(!isEnable) return true;
        
        if(!isEnableAllButtons) return true;
            
        isEnableAllButtons = false;
        
        if(_gameLayer) _gameLayer->_isTouchEnable = false;
        
        float duration = 0.05;
        runAction(cocos2d::Sequence::create(cocos2d::EaseOut::create(cocos2d::ScaleTo::create(duration, pushedScale), 2),
                                   cocos2d::CallFunc::create([=]()
                                                    {
                                                        pushed();
                                                    }),
                                   cocos2d::EaseOut::create(cocos2d::ScaleTo::create(duration, 1.0), 2),
                                   NULL));
    }
    
    return true;
}

void UIButtonSprite::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
//    if(getBoundingBox().containsPoint(touch->getLocation()))
//    {
//        if(!isPushed) pushed();
//    }
//    else
//    {
//        if(isPushed) released();
//    }
}

void UIButtonSprite::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
    isEnableAllButtons = true;
    isPushedFrame = false;
    
    if(getBoundingBox().containsPoint(touch->getLocation()) && isPushed)
    {
        if(!isEnable) return;
        
        if(!isEnableAllButtons) return;
        
        isEnableAllButtons = false;
        
        float duration = 0.1;
        runAction(cocos2d::Sequence::create(cocos2d::EaseOut::create(cocos2d::ScaleTo::create(duration, pushedScale), 2),
                                   cocos2d::CallFunc::create([=]()
                                                    {
                                                        released();
                                                    }),
                                   cocos2d::EaseOut::create(cocos2d::ScaleTo::create(duration, 1.0), 2),
                                   NULL));
    }
}

void UIButtonSprite::pushed()
{
    if(pushFunc) pushFunc();
    isPushed = true;
}

void UIButtonSprite::released()
{
    if(releaseFunc) releaseFunc();
    isPushed = false;
}




