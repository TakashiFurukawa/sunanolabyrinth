//
//  ActorManager.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/16.
//
//

#include "ActorManager.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "Block.h"
#include "GimmickSwitch.h"
#include "SandSwitch.h"
#include "StageBlock.h"
#include "HiruYoruBlock.h"
#include "GameLayer.h"
#include "MyChar.h"
#include "Stage.h"
#include "CrushBlock.h"
#include "LoveHeart.h"
#include "CrushSwitch.h"
#include "GoalBlock.h"
#include "WarpBlock.h"
#include "Star.h"
#include "AppearSwitch.h"
#include "AppearBlock.h"
#include "WaterSwitch.h"
#include "InfoBlock.h"

ActorManager *ActorManager::create()
{
    ActorManager *pRet = new ActorManager();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool ActorManager::init()
{
    if ( !Node::init() ) return false;
    
    createActors();
    
    Color3B color[] =
    {
        Color3B(150, 150, 100),
        Color3B(150, 100, 150),
        Color3B(100, 150, 150),
        Color3B(150, 150, 255),
        Color3B(150, 255, 150),
        Color3B(255, 150, 150),
        Color3B(150, 255, 255),
        Color3B(255, 150, 255),
        Color3B(255, 255, 150),
    };
    
    for(int i=0; i<9; i++)
    {
        _gimmickColor[i] = color[i];
    }
    
    _colorIndex = 0;
    
    scheduleUpdate();
    return true;
}

void ActorManager::update(float delta)
{
    
}


void ActorManager::createActors()
{
    auto map = _mapManager->_map;
    auto mapInfo = _mapManager->_mapInfo;
    
//    auto gameLayer = (GameLayer *)getParent()->getParent();
    
    
    auto props = mapInfo->getTileProperties();
    
    // マップから読み取り、ブロックを配置。
    auto oseruLayer = map->getLayer("oseru");
    auto crushSwitchLayer = map->getLayer("crushSwitch");
    auto sandSwitchLayer = map->getLayer("sandSwitch");
    auto appearSwitchLayer = map->getLayer("appearSwitch");
//    auto terrainLayer = map->getLayer("terrain");
//    auto hiruLayer = map->getLayer("hiru");
//    auto yoruLayer = map->getLayer("yoru");
    auto myCharLayer = map->getLayer("myChar");
    auto crushLayer = map->getLayer("crush");
    auto appearLayer = map->getLayer("appear");
    auto heartLayer = map->getLayer("heart");
    auto goalLayer = map->getLayer("goal");
    auto warpLayer = map->getLayer("warp");
    auto starLayer = map->getLayer("star");
    auto waterSwitchLayer = map->getLayer("waterSwitch");
    
    auto infoLayer = map->getLayer("info");
    
    cocos2d::Size mapSize = map->getMapSize();
    cocos2d::Size tileSize = map->getTileSize();
    
    Stage *stage = (Stage*)getParent();
    
    for(int y=0; y<(int)mapSize.height; y++)
    {
        for(int x=0; x<(int)mapSize.width; x++)
        {
            Actor *actor;
            
            if(crushSwitchLayer && crushSwitchLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y)) != 0)
            {
                int gid = crushSwitchLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y));
//                printf("%d\n", gid);
                
                actor = CrushSwitch::create();
                ((CrushSwitch *)actor)->_crushNo = gid;
                
                ((GimmickSwitch*)actor)->_col = actor->getColor();
                ((GimmickSwitch*)actor)->_myChar = stage->_myChar;
                
                initActor(actor, cocos2d::Vec2(x, y));
            }
            if(appearSwitchLayer && appearSwitchLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y)) != 0)
            {
                int gid = appearSwitchLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y));
                                printf("%d\n", gid);
                
                actor = AppearSwitch::create();
                ((AppearSwitch *)actor)->_id = gid;
                
                ((GimmickSwitch*)actor)->_col = actor->getColor();
                ((GimmickSwitch*)actor)->_myChar = stage->_myChar;
                
                initActor(actor, cocos2d::Vec2(x, y));
            }
            if(sandSwitchLayer && sandSwitchLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y)) != 0)
            {
                actor = SandSwitch::create();
                
                ((GimmickSwitch*)actor)->_col = actor->getColor();
                ((GimmickSwitch*)actor)->_myChar = stage->_myChar;
                
                initActor(actor, cocos2d::Vec2(x, y));
            }
            if(waterSwitchLayer && waterSwitchLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y)) != 0)
            {
                actor = WaterSwitch::create();
                
                ((GimmickSwitch*)actor)->_col = actor->getColor();
                ((GimmickSwitch*)actor)->_myChar = stage->_myChar;
                
                initActor(actor, cocos2d::Vec2(x, y));
            }
//            if(hiruLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y)) != 0)
//            {
//                actor = new HiruYoruBlock();
//                auto hiruyoru = (HiruYoruBlock*)actor;
//                hiruyoru->_gameLayer = gameLayer;
//                hiruyoru->init(Actor::ActorType::HIRU);
//                initActor(actor, cocos2d::Vec2(x, y));
//            }
//            if(yoruLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y)) != 0)
//            {
//                actor = new HiruYoruBlock();
//                auto hiruyoru = (HiruYoruBlock*)actor;
//                hiruyoru->_gameLayer = gameLayer;
//                hiruyoru->init(Actor::ActorType::YORU);
//                initActor(actor, cocos2d::Vec2(x, y));
//            }
            if(oseruLayer && oseruLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y)) != 0)
            {
                actor = Block::create();
                initActor(actor, cocos2d::Vec2(x, y));
            }
            if(crushLayer && crushLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y)) != 0)
            {
                actor = CrushBlock::create();
                int gid = crushLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y));
                ((CrushBlock*)actor)->_crushNo = gid;
                initActor(actor, cocos2d::Vec2(x, y));
                
//                actor->setColor(_gimmickColor[gid-8]);
            }
            if(appearLayer && appearLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y)) != 0)
            {
                actor = AppearBlock::create();
                int gid = appearLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y));
                ((AppearBlock*)actor)->_id = gid;
                initActor(actor, cocos2d::Vec2(x, y));
                
                log("%d", gid);
            }
            if(myCharLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y)) != 0)
            {
                
//                stage->_myChar = new MyChar();
                stage->_myChar->init(stage->_playerType);
                stage->_myChar->autorelease();
                initActor(stage->_myChar, cocos2d::Vec2(x, y));
            }
            if(heartLayer && heartLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y)) != 0)
            {
                LoveHeart *love = LoveHeart::create();
                love->setPosition(cocos2d::Vec2(x * tileSize.width * map->getScale(),
                                                y * tileSize.height * map->getScale()));
                cocos2d::Vec2 ajust = cocos2d::Vec2(love->getBoundingBox().size.width / 2,
                                                    love->getBoundingBox().size.height/2);
                
                love->setPosition(love->getPosition() + ajust + map->getPosition());
                love->_stage = (Stage*)getParent();
                getParent()->addChild(love);
            }
            if(starLayer && starLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y)) != 0)
            {
                Star *star = Star::create();
                star->setPosition(cocos2d::Vec2(x * tileSize.width * map->getScale(),
                                                y * tileSize.height * map->getScale()));
                cocos2d::Vec2 ajust = cocos2d::Vec2(star->getBoundingBox().size.width / 2,
                                                    star->getBoundingBox().size.height/2);
                
                star->setPosition(star->getPosition() + ajust + map->getPosition());
                star->_myChar = stage->_myChar;
                getParent()->addChild(star);
            }
            if(goalLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y)) != 0)
            {
                GoalBlock *goal = GoalBlock::create();
                goal->setPosition(cocos2d::Vec2(x * tileSize.width * map->getScale(),
                                                y * tileSize.height * map->getScale()));
                cocos2d::Vec2 ajust = cocos2d::Vec2(goal->getBoundingBox().size.width / 2,
                                                    0);
                
                goal->setPosition(goal->getPosition() + ajust + map->getPosition());
                goal->_stage = (Stage*)getParent();
                goal->_stage->_goalNode = (Node*)goal;
                goal->_gameLayer = (GameLayer*)getParent()->getParent();;
                getParent()->addChild(goal);
            }
            
            if(warpLayer && warpLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y)) != 0)
            {
                WarpBlock *warp = WarpBlock::create();
                warp->setPosition(cocos2d::Vec2(x * tileSize.width * map->getScale(),
                                                y * tileSize.height * map->getScale()));
                cocos2d::Vec2 ajust = cocos2d::Vec2(warp->getBoundingBox().size.width / 2,
                                                    0);
                
                warp->setPosition(warp->getPosition() + ajust + map->getPosition());
                warp->_stage = (Stage*)getParent();
                warp->_warpNo = warpLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y));
                getParent()->addChild(warp);
            }
            
            if(infoLayer && infoLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y)) != 0)
            {
                auto props = mapInfo->getTileProperties();
                auto prop = props.at(infoLayer->getTileGIDAt(cocos2d::Vec2(x, (int)mapSize.height-1 -y))).asValueMap();
                
                InfoBlock *info = InfoBlock::create(prop.at("text").asString());
                info->setPosition(cocos2d::Vec2(x * tileSize.width * map->getScale() + info->getBoundingBox().size.width/2,
                                                y * tileSize.height * map->getScale()));
                info->setAnchorPoint(Vec2(0.5, 0));
                info->_myChar = ((Stage*)getParent())->_myChar;
                getParent()->addChild(info);
            }
            
        }
    }
    
    
    // ワープブロックの参照先を検索
    for(auto childA : getParent()->getChildren())
    {
        if(childA->getName() != "warp") continue;
        
        for(auto childB : getParent()->getChildren())
        {
            if(childB->getName() != "warp") continue;
            
            if(childA == childB) continue;
            
            auto warpA = (WarpBlock*)childA;
            auto warpB = (WarpBlock*)childB;
            
            if(warpA->_warpNo == warpB->_warpNo)
            {
                warpB->_warp = warpA;
            }
        }
    }
    
}

void ActorManager::initActor(Actor *actor, cocos2d::Vec2 index)
{
    auto map = _mapManager->_map;
//    auto mapInfo = _mapManager->_mapInfo;
    cocos2d::Size mapSize = map->getMapSize();
    cocos2d::Size tileSize = map->getTileSize();
    
    actor->_mapManager = _mapManager;
    actor->setScale(0.99);
    
    actor->setPosition(cocos2d::Vec2(index.x * tileSize.width * map->getScale(),
                            index.y * tileSize.height * map->getScale()));
    cocos2d::Vec2 ajust = cocos2d::Vec2(actor->getBoundingBox().size.width/2, tileSize.height * 0.1);
    
    actor->setPosition(actor->getPosition() + ajust + map->getPosition());
    
    addChild(actor);
}


