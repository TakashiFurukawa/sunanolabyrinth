//
//  CrushBlock.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/23.
//
//

#include "CrushBlock.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "GameLayer.h"
#include "Sand.h"

CrushBlock *CrushBlock::create()
{
    CrushBlock *pRet = new CrushBlock();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool CrushBlock::init()
{
    if ( !SwitchBlock::init() ) return false;
    
    
    
    scheduleUpdate();
    return true;
}

void CrushBlock::update(float delta)
{
    
}

void CrushBlock::switchOn()
{
    GameLayer *gameLayer = (GameLayer*)getParent()->getParent()->getParent();
    
    float duration = 0.5;
    runAction(cocos2d::Sequence::create(cocos2d::DelayTime::create(1),
                                        cocos2d::Spawn::create(cocos2d::RotateBy::create(duration, 360),
                                                               cocos2d::ScaleTo::create(duration, 0),
                                                               NULL),
                                        cocos2d::DelayTime::create(1),
                                        cocos2d::CallFunc::create([=]()
                                                                  {
                                                                     ((Stage*)(getParent()->getParent()))->followChange(gameLayer->_unActiveStage->_myChar);
                                                                      this->removeFromParent();
                                                                      
                                                                      gameLayer->_topStage->_sand->_isMove = true;
                                                                      gameLayer->_bottomStage->_sand->_isMove = true;
                                                                      gameLayer->_isEnablePlayerChange = true;
                                                                      
                                                                      gameLayer->_controlMyChar->_isWalkOK = true;
                                                                  }),
                                        NULL));
}



