//
//  Star.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/05/19.
//
//

#include "Star.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"

Star *Star::create()
{
    Star *pRet = new Star();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool Star::init()
{
    if ( !Sprite::init() ) return false;
    
    setTexture("gameScene/gimmick/star.png");
    
    _isGet = false;
    
    runAction(RepeatForever::create(Sequence::create(EaseBackOut::create(RotateTo::create(0.5, 45)),
                                                     EaseBackOut::create(RotateTo::create(0.5, -45)),
                                                     NULL)));
    
    scheduleUpdate();
    return true;
}

void Star::update(float delta)
{
    if(!_isGet && _myChar->getBoundingBox().intersectsRect(getBoundingBox()))
    {
        startStar();
    }
}

void Star::startStar()
{
    _isGet = true;
    
    float duration = 0.5;
    auto act = Sequence::create(Spawn::create(RotateBy::create(duration, 360 * 2),
                                              JumpBy::create(duration, cocos2d::Vec2::ZERO, getBoundingBox().size.height * 3, 1),
                                              FadeOut::create(duration),
                                              NULL),
                                CallFunc::create([=]()
                                                 {
                                                     this->removeFromParent();
                                                 }),
                                NULL);
    runAction(act);
    
    _myChar->star();
}