//
//  GameUI.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/20.
//
//

#include "GameUI.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "UIButtonSprite.h"
#include "GameScene.h"
#include "TestScene.h"
#include "PauseLayer.h"

GameUI *GameUI::create()
{
    GameUI *pRet = new GameUI();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool GameUI::init()
{
    if ( !Layer::init() ) return false;
    
    _playerChangeButton = UISpriteButton::create("gameScene/ui/lady.png",
                                                 [=]()
                                                 {
                                                     _gameLayer->playerChange();
                                                     
                                                     if(_gameLayer->_controlMyChar->_playerType == MyChar::Gentle)
                                                     {
                                                         _playerChangeButton->setTexture("gameScene/ui/lady.png");
                                                     }
                                                     else
                                                     {
                                                         _playerChangeButton->setTexture("gameScene/ui/gentle.png");
                                                     }
                                                 },
                                                 [=]()
                                                 {
                                                 });
    
    _playerChangeButton->setPosition(cocos2d::Vec2(designResolutionSize.width * 0.1, designResolutionSize.height * 0.2));
    addChild(_playerChangeButton);
//
//    _rightButton = UIButtonSprite::create("gameScene/ui/right.png",
//                                          cocos2d::Vec2(designResolutionSize.width * 0.9, designResolutionSize.height * 0.9),
//                                          [=]()
//                                          {
//                                              _gameLayer->runAction(cocos2d::EaseInOut::create(cocos2d::ScaleBy::create(0.5, 1.5), 2));
//                                          },
//                                          [=]()
//                                          {
//                                              
//                                          });
//    _rightButton->_gameLayer = _gameLayer;
//    addChild(_rightButton);
//    
//    _leftButton = UIButtonSprite::create("gameScene/ui/left.png",
//                                         cocos2d::Vec2(designResolutionSize.width * 0.7, designResolutionSize.height * 0.9),
//                                         [=]()
//                                         {
//                                             _gameLayer->runAction(cocos2d::EaseInOut::create(cocos2d::ScaleBy::create(0.5, 0.5), 2));
//                                         },
//                                         [=]()
//                                         {
//                                             
//                                         });
//    _leftButton->_gameLayer = _gameLayer;
//    addChild(_leftButton);
    
//    _sunadokeiHanten = UIButtonSprite::create("gameScene/ui/sunadokei.png",
//                                        cocos2d::Vec2(designResolutionSize.width * 0.1, designResolutionSize.height * 0.9),
//                                        [=]()
//                                        {
//                                            bool *climbOK = &_gameLayer->_controlMyChar->_isClimbOK;
//                                            if(*climbOK) *climbOK = false;
//                                            else *climbOK = true;
//                                        });
//    _sunadokeiHanten->_gameLayer = _gameLayer;
//    addChild(_sunadokeiHanten);
    
    UISpriteButton *_restartButton = UISpriteButton::create("gameScene/ui/stopbutton.png",
                                                            [=]()
                                                            {
                                                                GameScene::pauseAllChild(_gameLayer);
                                                                GameScene::pauseAllChild(this);
                                                                
                                                                PauseLayer *pause = new PauseLayer();
                                                                pause->_gameScene = (GameScene*)_gameLayer->getParent();
                                                                pause->init();
                                                                pause->autorelease();
                                                                pause->_gameScene->addChild(pause);
                                                                pause->appear(true);
                                                            });
    
    _restartButton->setPosition(cocos2d::Vec2(designResolutionSize.width * 0.1,
                                              designResolutionSize.height * 0.9));
    addChild(_restartButton);
    
    
    
//    _sandStopButton = new SandStopButton();
//    _sandStopButton->_sand[0] = _gameLayer->_topStage->_sand;
//    _sandStopButton->_sand[1] = _gameLayer->_bottomStage->_sand;
//    _sandStopButton->init();
//    _sandStopButton->autorelease();
//    _sandStopButton->setPosition(Vec2(designResolutionSize.width * 0.85,
//                                      designResolutionSize.height * 0.9));
//    addChild(_sandStopButton);
//    
//    
    _alertSp = Sprite::create("gameScene/ui/alert.png");
    _alertSp->setPosition(Vec2(designResolutionSize.width/2,
                               designResolutionSize.height * 0.8));
    _alertSp->setOpacity(0);
    addChild(_alertSp);
    
    _alertSp->runAction(RepeatForever::create(Sequence::create(FadeIn::create(1.0),
                                                               FadeOut::create(1.0),
                                                               NULL)));
    
    
    scheduleUpdate();
    return true;
}

void GameUI::update(float delta)
{
    
}

void GameUI::gameOver()
{
    _isGameOver = true;
}


void GameUI::turn()
{
    auto children = getChildren();
    for(auto child : children)
    {
        UISprite *uiSp = (UISprite*)child;
        
        turnObject(uiSp);
    }
}

void GameUI::turnObject(UISprite *button)
{
    cocos2d::ActionInterval *anim;
    
    
    if(_gameLayer->_stageState == GameLayer::LANDSCAPE_LEFT)
    {
        anim = cocos2d::EaseInOut::create(cocos2d::Sequence::create(cocos2d::MoveTo::create(0.5, button->_origin),
                                                                cocos2d::RotateTo::create(0.5, 0),
                                                                NULL),2);
    }
    
    else
    {
        anim = cocos2d::EaseInOut::create(cocos2d::Sequence::create(cocos2d::MoveTo::create(0.5, (cocos2d::Vec2)designResolutionSize - button->_origin),
                                                                cocos2d::RotateTo::create(0.5, 180),
                                                                NULL),2);
    }
    anim->setTag(10);
    button->stopActionByTag(anim->getTag());
    button->runAction(anim);
}







