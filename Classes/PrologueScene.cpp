//
//  PrologueScene.cpp
//  SunanoLabyrinth
//
//  Created by 蔵元隼人 on 2015/05/19.
//
//

#include "PrologueScene.h"
#include "TestScene.h"
#include "TitleScene.h"
#include "MultiResolution.h"

#include "ProloScene1.h"
#include "ProloScene2.h"
#include "ProloScene3.h"
#include "ProloScene4.h"

bool PrologueScene::init()
{
    if(!Scene::init())
    {
        return false;
    }
    isSceneChanged = false;
    
    Slideshow *scene1 = ProloScene1::create();
    scene1->setTag(scene_tag_1);
    this->addChild(scene1);
    
    nowScene = scene_tag_1;
    
    Sprite *waku = Sprite::create("PrologueScene/waku.png");
    waku->setPosition(designResolutionSize/2);
    this->addChild(waku,1111111);
    
    this->runAction(Sequence::create(
                                     DelayTime::create(5.0f),
                                     CallFunc::create([=](){this->changeScreen();}),
                                     DelayTime::create(4.5f),
                                     CallFunc::create([=](){this->changeScreen();}),
                                     DelayTime::create(3.0f),
                                     CallFunc::create([=](){this->changeScreen();}),
                                     DelayTime::create(2.0f),
                                     CallFunc::create([=](){this->changeScene();}),
                                     NULL));
   
    // タッチイベントとかの登録
    auto touchListener = cocos2d::EventListenerTouchOneByOne::create();
    touchListener->onTouchBegan = CC_CALLBACK_2(PrologueScene::onTouchBegan, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(PrologueScene::onTouchEnded, this);
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);
    
    return true;
}

bool PrologueScene::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
    return true;
}

void PrologueScene::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
    changeScene();
}

void PrologueScene::changeScene()
{
    if(!isSceneChanged)
    {
        //this->stopAllActions();
        TransitionFade *fade = TransitionFade::create(2.0f, TitleScene::create(), Color3B::WHITE);
        Director::getInstance()->replaceScene(fade);
    }
}
void PrologueScene::changeScreen()
{
    if(nowScene != scene_tag_4)
    {
        ((Slideshow*)this->getChildByTag(nowScene))->death();
    }
    switch (nowScene)
    {
        case scene_tag_1:
        {
            Slideshow *slide = ProloScene2::create();
            slide->setTag(scene_tag_2);
            nowScene = scene_tag_2;
            this->addChild(slide);
            break;
        }
        case scene_tag_2:
        {
            
            Slideshow *slide = ProloScene3::create();
            slide->setTag(scene_tag_3);
            nowScene = scene_tag_3;
            this->addChild(slide);
            break;
            
        }
        case scene_tag_3:
        {
            Slideshow *slide = ProloScene4::create();
            slide->setTag(scene_tag_4);
            nowScene = scene_tag_4;
            this->addChild(slide);
            break;
        }
    }
}