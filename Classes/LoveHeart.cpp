//
//  LoveHeart.cpp
//  SunanoLabyrinth
//
//  Created by Furukawa on 2015/04/26.
//
//

#include "LoveHeart.h"
#include "SimpleAudioEngine.h"
#include "MultiResolution.h"
#include "GameScene.h"


//GameLayer *getGameLayer(cocos2d::Node *child)
//{
//    GameLayer *gameLayer;
//    auto parent = child->getParent();
//    while (parent->getName() != "gameLayer") parent = parent->getParent();
//    gameLayer = (GameLayer *) parent;
//    
//    return gameLayer;
//}

MyChar *getMyChar(cocos2d::Node *child)
{
    GameLayer *gameLayer;
    auto parent = child->getParent();
    while (parent->getName() != "gameLayer") parent = parent->getParent();
    gameLayer = (GameLayer *) parent;
    
    return gameLayer->_activeStage->_myChar;
}

LoveHeart *LoveHeart::create()
{
    LoveHeart *pRet = new LoveHeart();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool LoveHeart::init()
{
    if ( !cocos2d::Sprite::init() ) return false;
    
    setTexture("gameScene/ui/heart.png");
    
    _isGet = false;
    
    scheduleUpdate();
    return true;
}

void LoveHeart::update(float delta)
{
    if(getMyChar(this) == _stage->_myChar)
    {
        if(getMyChar(this)->getBoundingBox().intersectsRect(getBoundingBox()))
        {
            catchHeart();
        }
    }
}

void LoveHeart::catchHeart()
{
    if(_isGet) return;
    
    SimpleAudioEngine::getInstance()->playEffect("sound/SE/piko.mp3");
    
    _isGet = true;
    
    GameScene *gameScene;
    auto parent = getParent();
    while (parent->getName() != "gameScene")
    {
        parent = parent->getParent();
    }
    gameScene = (GameScene*)parent;
    
    gameScene->_heartNum ++;
    
    float duration = 0.5;
    auto act = Sequence::create(Spawn::create(RotateBy::create(duration, 360 * 2),
                                              JumpBy::create(duration, cocos2d::Vec2::ZERO, getBoundingBox().size.height * 3, 1),
                                              FadeOut::create(duration),
                                              NULL),
                                CallFunc::create([=]()
                                                 {
                                                     this->removeFromParent();
                                                 }),
                                NULL);
    runAction(act);
}




